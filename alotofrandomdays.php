<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Calendario</title>
  <style>
    #calendar
    {
      font-family:Arial;
      font-size:12px;
    }
    #calendar caption
    {
      text-align:left;
      padding:5px 10px;
      background-color:#003366;
      color:#fff;
      font-weight:bold;
    }
    #calendar .fechasolimop
    {
      background-color:#8c3c26;
    }
    #calendar .fechaentregaserver
    {
      background-color:#b8db67;
    }
    #calendar .entregauser
    {
      background-color:#708a6b;
    }
    #calendar .inicioPreATP
    {
      background-color:#256c70;
    }
    #calendar .finPreATP
    {
      background-color:#253e70;
    }
    #calendar .entregaOYM
    {
      background-color:#6eb9eb;
    }
    body
    {
      background-image: url(all-of-those-images/interf/logoazul.png);
      background-repeat: no-repeat;
      background-size: 120px 35px;
      background-position: 20px 25px;
    }
    #calendar caption div:nth-child(1) {float:left;}
    #calendar caption div:nth-child(2) {float:right;}
    #calendar caption div:nth-child(2) a {cursor:pointer;}
    table
    {
      border: 1px solid black;
    }
    th
    {
      background-color:#006699;
      color:#fff;
      width:40px;
    }
    td
    {
      text-align:right;
      padding:2px 5px;
      background-color:silver;
    }
    .shad
    {
      font-size: 20px;
      font-weight: bold;
      border: 1px flat #000033;
    }
    .containerOfRod
    {
      padding: 4px 4px;
      font-size: 14px;
      border:10px groove #616161;
      border-radius: 10px;
      position:absolute;
    }
    button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
  </style>
  <?php
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    $fecha_actual= date("Y-m-d");
    $sql="select entregaUser,fechaSoliMOP,fechaEntregaServer,inicioPreATP,finPreATP,entregaOYM from maquinas where interId='".$_POST['folio']."'";
    $r = mysqli_query($conn,$sql);
    if(!$r)
      echo "No se lograron recuperar fechas";
    else
    {
      $thosedays = mysqli_fetch_array($r);
      $c=array('entregaUser','fechaSoliMOP','fechaEntregaServer','inicioPreATP','finPreATP','entregaOYM');
      $h=0;
      echo "<script type='text/javascript'>";
      for($j=0;$j<6;$j++)
        if($thosedays[$j]!="")
        {
          $names[$h]=$c[$j];
          $day[$h]=new DateTime($thosedays[$j]);
          $some=explode("-",$thosedays[$j]);
          $days[$h]= $some[2];
          $months[$h]= $some[1];
          $years[$h]= $some[0];
          $h++;
        }
      for($j=0;$j<sizeof($days);$j++)
        for($k=0;$k<sizeof($day);$k++)
          if($day[$j]<$day[$k])
          {
            $temp=$day[$j];
            $day[$j]=$day[$k];
            $day[$k]=$temp;
            $temp=$names[$j];
            $names[$j]=$names[$k];
            $names[$k]=$temp;
            $temp=$days[$j];
            $days[$j]=$days[$k];
            $days[$k]=$temp;
            $temp=$months[$j];
            $months[$j]=$months[$k];
            $months[$k]=$temp;
            $temp=$years[$j];
            $years[$j]=$years[$k];
            $years[$k]=$temp;
          }
      $chain = " var days = [";
        for($k=0;$k<sizeof($days);$k++)
        {
          if($k>0)
            $chain .= ",";
          $chain .= $days[$k];
        }
      $chain .= "]; ";
      $chain .= "var months = [";
        for($k=0;$k<sizeof($months);$k++)
        {
          if($k>0)
            $chain .= ",";
          $chain .= $months[$k];
        }
      $chain .= "]; var years = [";
        for($k=0;$k<sizeof($years);$k++)
        {
          if($k>0)
            $chain .= ",";
          $chain .= $years[$k];
        }
      $chain .= "]; var flags=[";
        for($k=0;$k<sizeof($names);$k++)
        {
          if($k>0)
            $chain .= ",";
          if($k==(sizeof($names)-1))
            $chain .="1";
          else
            $chain .= "0";
        }
      $chain .= "]; var thoseclases=[";
        for($k=0;$k<sizeof($names);$k++)
        {
          if($k>0)
            $chain .= ",";
          $chain .= "'".strtolower($names[$k])."'";
        }
      $chain .= "];";
      echo $chain." </script>";
    }
    function dias_pasados($DBFecha,$hoy)
    {
      $dias = (strtotime($DBFecha)-strtotime($hoy))/86400;
      $dias = abs($dias); $dias = floor($dias);
      return $dias;
    }
  ?>
</head>
<body>
  <div class="containerOfRod">
    <h3 align="center">&nbsp;&nbsp;&nbsp;Calendario</h3>
    <table id="calendar" align="center">
      <caption></caption>
      <thead>
        <tr>
          <th>Lun</th>
          <th>Mar</th>
          <th>Mie</th>
          <th>Jue</th>
          <th>Vie</th>
          <th>Sab</th>
          <th>Dom</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <br>
    <button type="button" style="display: inline; background-color:#8c3c26;font-size:11px;cursor:default;height:20px;">Solicitud MOP</button>
    <?php
      if($thosedays['fechaSoliMOP']=="")
        $dia0="No registrado";
      else
        $dia0=dias_pasados($thosedays['fechaSoliMOP'],$fecha_actual);
      echo "  dias desde Solicitud MOP: ".$dia0;
    ?>
    <br>
    <button type="button" style="display: inline; background-color:#b8db67;font-size:11px;cursor:default;height:20px;">Entrega Server</button>
    <?php
      if($thosedays['fechaEntregaServer']=="")
        $dia1="No registrado";
      else
        $dia1=dias_pasados($thosedays['fechaEntregaServer'],$fecha_actual);
      echo "dias desde entrega Server: ".$dia1;
    ?>
    <br>
    <button type="button" style="display: inline; background-color:#708a6b;font-size:11px;cursor:default;height:20px;">Entrega Usuario</button>
    <?php
      if($thosedays['entregaUser']=="")
        $dia2="No registrado";
      else
        $dia2=dias_pasados($thosedays['entregaUser'],$fecha_actual);
      echo "dias desde entregaa usuario: ".$dia2;
      //echo "<br>".$sql." post ".$_POST['folio'];
    ?>
    <br>
    <button type="button" style="display: inline; background-color:#256c70;font-size:11px;cursor:default;height:20px;">inicio PreATP</button>
    <?php
      if($thosedays['inicioPreATP']=="")
        $dia3="No registrado";
      else
        $dia3=dias_pasados($thosedays['inicioPreATP'],$fecha_actual);
      echo "  dias desde Inicio PreATP: ".$dia3;
    ?>
    <br>
    <button type="button" style="display: inline; background-color:#253e70;font-size:11px;cursor:default;height:20px;">fin PreATP</button>
    <?php
      if($thosedays['finPreATP']=="")
        $dia4="No registrado";
      else
        $dia4=dias_pasados($thosedays['finPreATP'],$fecha_actual);
      echo "dias desde fin PreATP: ".$dia4;
    ?>
    <br>
    <button type="button" style="display: inline; background-color:#6eb9eb;font-size:11px;cursor:default;height:20px;">entrega a OYM</button>
    <?php
      if($thosedays['entregaOYM']=="")
        $dia5="No registrado";
      else
        $dia5=dias_pasados($thosedays['entregaOYM'],$fecha_actual);
      echo "dias desde entrega a OYM: ".$dia5;
      //echo "<br>".$sql." post ".$_POST['folio'];
    ?>
  </div>
</body>
<script>
  var actual=new Date();
  function mostrarCalendario(year,month)
  {
    var istodaypainted=0;
    var p=0,w=0,t=0;
    var now=new Date(year,month-1,1);
    var last=new Date(year,month,0);
    var primerDiaSemana=(now.getDay()==0)?7:now.getDay();
    var ultimoDiaMes=last.getDate();
    var dia=0;
    var resultado="<tr bgcolor='silver'>";
    var diaActual=0;
    console.log(ultimoDiaMes);
    var last_cell=primerDiaSemana+ultimoDiaMes;
    for(var i=1;i<=42;i++)
    {
      if(i==primerDiaSemana)
        dia=1;
      if(i<primerDiaSemana || i>=last_cell)
        resultado+="<td>&nbsp;</td>";
      else
      {
      //       entregaUser,fechaSoliMOP,fechaEntregaServer
        if(year<=actual.getFullYear())
        {
          p=1;
          if(year==actual.getFullYear()&&month>=actual.getMonth()+1)
          {
            p=0;
            if(month==actual.getMonth()+1&&dia<actual.getDate())
              p=1;
          }
        }
        else
          p=0;
        if(year>=years[0])
        {
          w=1;
          if(year==years[0]&&month<=months[0])
          {
            w=0;
            if(month==months[0]&&dia>=days[0])
              w=1;
          }
        }
        else
          w=0;
        if(p==1&&w==1)
          for(h=0;h<thoseclases.length;h++)
          {
            if(year>=years[h])
            {
              t=1;
              if(year==years[h]&&month<=months[h])
              {
                t=0;
                if(month==months[h]&&dia>=days[h])
                  t=1;
              }
            }
            else
              t=0;
            if(t==1)
            {
              for(j=0;j<thoseclases.length;j++)
                flags[j]=0;
              flags[h]=1;
            }
          }
        istodaypainted=0;
        for(j=0;j<thoseclases.length;j++)
          if(flags[j]==1&&istodaypainted==0&&p==1&&w==1)
          {
            resultado+="<td class='"+thoseclases[j]+"'>"+dia+"</td>";
            istodaypainted=1;
          }
        if(istodaypainted==0)
          resultado+="<td>"+dia+"</td>";
        dia++;
                  }
                  if(i%7==0)
                  {
                          if(dia>ultimoDiaMes)
                                  break;
                          resultado+="</tr><tr>\n";
                  }
          }
          resultado+="</tr>";
    var meses=Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        nextMonth=month+1;
          nextYear=year;
          if(month+1>12)
          {
                  nextMonth=1;
                  nextYear=year+1;
          }
    prevMonth=month-1;
          prevYear=year;
          if(month-1<1)
          {
                  prevMonth=12;
                  prevYear=year-1;
          }
        document.getElementById("calendar").getElementsByTagName("caption")[0].innerHTML="<div>"+meses[month-1]+" / "+year+"</div><div><a onclick='mostrarCalendario("+prevYear+","+prevMonth+")'>&lt;</a> <a onclick='mostrarCalendario("+nextYear+","+nextMonth+")'>&gt;</a></div>";
          document.getElementById("calendar").getElementsByTagName("tbody")[0].innerHTML=resultado;
  }
  mostrarCalendario(actual.getFullYear(),actual.getMonth()+1);
</script>
</html>