<html lang="es"> 
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <title>Maquinas Virtuales</title>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
    ?>
  </head>
  <style>
    table
    {
      font-size: 10px;
    }
  </style>
  <script>
    function isdate(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=45))
        return false;
      return true;
    }
    function isTheip(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
        return false;
      return true;
    }
    function isIntNumber(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
    function rev(event)
    {
      var k = (event.which) ? event.which : event.keyCode;
      if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k ==32) )
        return true;
      else
        return false;
    }
    function giveSomeStandalone(ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="standalonePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStandalone").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:7%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:10%";
          theStatusTitle.innerHTML = "Estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:8%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:7%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:8%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:10%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="RAM (Gb) :";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:10%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:10%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "StandaloneTitle";
          newTLine.id = "StandaloneTitle";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="StandaloneIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('1',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPStandalone"+mimic;
            IPCount.id="IPStandalone"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="S1arreglo"+mimic;
            theArreglo.id="S1arreglo"+mimic;
            theArreglo.value=0;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="S1tipo"+mimic;
            theTipo.id="S1tipo"+mimic;
            theTipo.value=0;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="S1id"+mimic;
            theId.id="S1id"+mimic;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:7%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="S1hostname"+mimic;
            theHostname.id="S1hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:10%";
            theStatus = document.createElement("select");
            theStatus.name="S1status"+mimic;
            theStatus.id="S1status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
            theStatusPlace.innerHTML += '<br><br>Detalles :<br><br>';
            //  Status Detail
              theStatusDetail = document.createElement("input");
              theStatusDetail.style="width:100%";
              theStatusDetail.name="S1statusDetail"+mimic;
              theStatusDetail.id="S1statusDetail"+mimic;
              theStatusDetail.type="text";
              theStatusDetail.onkeypress = function(){return rev(event)}; 
              theStatusDetail.setAttribute("autocomplete", "off");
              theStatusPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:8%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="S1infraDef"+mimic;
            theInfraDef.id="S1infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="S1aplicacion"+mimic;
            theAplication.id="S1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:7%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="S1hipervisor"+mimic;
            theHipervisor.id="S1hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente 
            theAmbientePlace = document.createElement("td");
            theAmbientePlace.style="width:8%";
            theAmbientePlace.innerHTML += 'Solicitado :<br><br>';
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteSolicitado);
            theAmbientePlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  ambiente entregado
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="S1ambienteEntregado"+mimic;
            theAmbienteEntregado.id="S1ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteEntregado);
          //  vCPU 
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:10%";
            thevCPUPlace.innerHTML += 'Solicitado :<br><br>';
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="S1CPUS"+mimic;
            thevCPUSolicitado.id="S1CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPUSolicitado);
            thevCPUPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  vCPU entregado
              thevCPUEntregado = document.createElement("input");
              thevCPUEntregado.name="S1CPUE"+mimic;
              thevCPUEntregado.id="S1CPUE"+mimic;
              thevCPUEntregado.type="number";
              thevCPUEntregado.style="width:100%";
              thevCPUEntregado.min=1;
              thevCPUEntregado.step=1;
              thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
              thevCPUPlace.appendChild(thevCPUEntregado);
          //  RAM Solicitado
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAMPlace.innerHTML += 'Solicitado :<br><br>';
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="S1RAMS"+mimic;
            theRAMSolicitado.id="S1RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAMSolicitado);
            theRAMPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  RAM Entregado
              theRAMEntregado = document.createElement("input");
              theRAMEntregado.name="S1RAME"+mimic;
              theRAMEntregado.id="S1RAME"+mimic;
              theRAMEntregado.type="number";
              theRAMEntregado.style="width:100%";
              theRAMEntregado.min=1;
              theRAMEntregado.step=1;
              theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
              theRAMPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:10%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="S1TDOYM"+mimic;
            theTDOYM.id="S1TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
            theTDOYMPlace.innerHTML += '<br><br>Status :<br><br>';
            //  Status OYM
              theStatusOYM = document.createElement("select");
              theStatusOYM.style="width:100%";
              theStatusOYM.name="S1statusOYM"+mimic;
              theStatusOYM.id="S1statusOYM"+mimic;
              var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
              for(i=0;i<possibleStatusOYM.length;i++)
              {
                opt = document.createElement('option');
                opt.value = possibleStatusOYM[i];
                opt.innerHTML = possibleStatusOYM[i];
                theStatusOYM.appendChild(opt);
              }
              theTDOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs2 = document.createElement("input");
            theSpecs2.type="button";
            theSpecs2.style="width:100%";
            theSpecs2.value="DB Requerimientos";
            theSpecs2.onclick = function(){window.open('specs.php?accion=4&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            theSpecs4 = document.createElement('a');
            theSpecs4.href = 'reporteRemedy.php?folio='+interId;
            theSpecs4.innerText = 'Formato OyM';
            theSpecsPlace.appendChild(theSpecs1); 
            theSpecsPlace.appendChild(theSpecs2); 
            theSpecsPlace.appendChild(theSpecs3);
            theSpecsPlace.appendChild(theSpecs4); 
          //  Line
            newLine.name = "childStandalone"+mimic.toString();
            newLine.id = "childStandalone"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbientePlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfStandalone").value=mimic;
    }
    function giveSomeStandalone2(interId,folio)
    {
      //  recuperar informacion de equipo
        workplace1="standalonePlaceholder2";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStandalone").value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:10%";
          theDiscoTitle.innerHTML="Storage :";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:10%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:10%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:10%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:10%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:10%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:10%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:10%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:10%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:10%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "StandaloneTitle2";
          newTLine.id = "StandaloneTitle2";
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:10%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Storage";
            theHD.onclick = function(){window.open('specs.php?accion=7&machine=s1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=300,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD);
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:10%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="S1internalConf"+mimic;
            theInternalConf.id="S1internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:10%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="S1remedy"+mimic;
            theRemedy.id="S1remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:10%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="S1soliMOP"+mimic;
            theSoliMOP.id="S1soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:10%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="S1entregaServer"+mimic;
            theEntregaServer.id="S1entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:10%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="S1entregaUser"+mimic;
            theEntregaUser.id="S1entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:10%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="S1inicioATP"+mimic;
            theInicioATP.id="S1inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:10%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="S1finATP"+mimic;
            theFInATP.id="S1finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:10%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="S1entregaOYM"+mimic;
            theEntregaOYM.id="S1entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=400,height=450,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = "childStandalone2"+mimic.toString();
            newLine.id = "childStandalone2"+mimic.toString();
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function giveSomeApliance(ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="apliancePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfApliance").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:7%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:10%";
          theStatusTitle.innerHTML = "Estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:8%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:7%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:8%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:10%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="RAM (Gb) :";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:10%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:10%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "AplianceTitle";
          newTLine.id = "AplianceTitle";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="AplianceIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('2',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPApliance"+mimic;
            IPCount.id="IPApliance"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="A1arreglo"+mimic;
            theArreglo.id="A1arreglo"+mimic;
            theArreglo.value=0;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="A1tipo"+mimic;
            theTipo.id="A1tipo"+mimic;
            theTipo.value=0;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="A1id"+mimic;
            theId.id="A1id"+mimic;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:7%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="A1hostname"+mimic;
            theHostname.id="A1hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:10%";
            theStatus = document.createElement("select");
            theStatus.name="A1status"+mimic;
            theStatus.id="A1status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
            theStatusPlace.innerHTML += '<br><br>Detalles :<br><br>';
            //  Status Detail
              theStatusDetail = document.createElement("input");
              theStatusDetail.style="width:100%";
              theStatusDetail.name="A1statusDetail"+mimic;
              theStatusDetail.id="A1statusDetail"+mimic;
              theStatusDetail.type="text";
              theStatusDetail.onkeypress = function(){return rev(event)}; 
              theStatusDetail.setAttribute("autocomplete", "off");
              theStatusPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:8%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="A1infraDef"+mimic;
            theInfraDef.id="A1infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="A1aplicacion"+mimic;
            theAplication.id="A1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:7%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="A1hipervisor"+mimic;
            theHipervisor.id="A1hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente 
            theAmbientePlace = document.createElement("td");
            theAmbientePlace.style="width:8%";
            theAmbientePlace.innerHTML += 'Solicitado :<br><br>';
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteSolicitado);
            theAmbientePlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  ambiente entregado
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="A1ambienteEntregado"+mimic;
            theAmbienteEntregado.id="A1ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteEntregado);
          //  vCPU solicitado
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:10%";
            thevCPUPlace.innerHTML += 'Solicitado :<br><br>';
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="A1CPUS"+mimic;
            thevCPUSolicitado.id="A1CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPUSolicitado);
            thevCPUPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  vCPU entregado
            thevCPUEntregado = document.createElement("input");
            thevCPUEntregado.name="A1CPUE"+mimic;
            thevCPUEntregado.id="A1CPUE"+mimic;
            thevCPUEntregado.type="number";
            thevCPUEntregado.style="width:100%";
            thevCPUEntregado.min=1;
            thevCPUEntregado.step=1;
            thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPUEntregado);
          //  RAM Solicitado
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAMPlace.innerHTML += 'Solicitado :<br><br>';
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="A1RAMS"+mimic;
            theRAMSolicitado.id="A1RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAMSolicitado);
            theRAMPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  RAM Entregado
            theRAMEntregado = document.createElement("input");
            theRAMEntregado.name="A1RAME"+mimic;
            theRAMEntregado.id="A1RAME"+mimic;
            theRAMEntregado.type="number";
            theRAMEntregado.style="width:100%";
            theRAMEntregado.min=1;
            theRAMEntregado.step=1;
            theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:10%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="A1TDOYM"+mimic;
            theTDOYM.id="A1TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
            theTDOYMPlace.innerHTML += '<br><br>Status :<br><br>';
            //  Status OYM
              theStatusOYM = document.createElement("select");
              theStatusOYM.style="width:100%";
              theStatusOYM.name="A1statusOYM"+mimic;
              theStatusOYM.id="A1statusOYM"+mimic;
              var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
              for(i=0;i<possibleStatusOYM.length;i++)
              {
                opt = document.createElement('option');
                opt.value = possibleStatusOYM[i];
                opt.innerHTML = possibleStatusOYM[i];
                theStatusOYM.appendChild(opt);
              }
              theTDOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            theSpecs4 = document.createElement('a');
            theSpecs4.href = 'reporteRemedy.php?folio='+interId;
            theSpecs4.innerText = 'Formato OyM';
            theSpecsPlace.appendChild(theSpecs1); 
            theSpecsPlace.appendChild(theSpecs3); 
            theSpecsPlace.appendChild(theSpecs4); 
          //  Line
            newLine.name = "childApliance"+mimic.toString();
            newLine.id = "childApliance"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbientePlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfApliance").value=mimic;
    }
    function giveSomeApliance2(interId,folio)
    {
      //  recuperar informacion de equipo
        workplace1="apliancePlaceholder2";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfApliance").value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:10%";
          theDiscoTitle.innerHTML="Storage :";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:10%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:10%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:10%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:10%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:10%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:10%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:10%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:10%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:10%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "AplianceTitle2";
          newTLine.id = "AplianceTitle2";
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:10%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Storage";
            theHD.onclick = function(){window.open('specs.php?accion=7&machine=a1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=300,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD); 
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:10%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="A1internalConf"+mimic;
            theInternalConf.id="A1internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:10%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="A1remedy"+mimic;
            theRemedy.id="A1remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:10%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="A1soliMOP"+mimic;
            theSoliMOP.id="A1soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:10%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="A1entregaServer"+mimic;
            theEntregaServer.id="A1entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:10%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="A1entregaUser"+mimic;
            theEntregaUser.id="A1entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:10%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="A1inicioATP"+mimic;
            theInicioATP.id="A1inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:10%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="A1finATP"+mimic;
            theFInATP.id="A1finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:10%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="A1entregaOYM"+mimic;
            theEntregaOYM.id="A1entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=400,height=450,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = "childApliance2"+mimic.toString();
            newLine.id = "childApliance2"+mimic.toString();
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function giveSomeCluster(folio)
    {
      //  recuperar informacion de equipo
        workplace1="clusterPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfCluster").value;
        mimic=parseInt(mimic);
      //  Titulos
        newTLine = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine.id="ClusterTitle"+mimic;
        theNOTitle = document.createElement("th");
        theNOTitle.innerHTML = "Cluster "+parseInt(mimic+1)+":";
        theNOTitle.style="font-size: 12px;";
        theNumberMachine = document.createElement("input");
        theNumberMachine.name="machinesOnCluster"+mimic;
        theNumberMachine.id="machinesOnCluster"+mimic;
        theNumberMachine.value=0;
        theNumberMachine.type="hidden";
        newTLine.name = "ClusterTitle"+mimic;
        newTLine.id = "ClusterTitle"+mimic;
        newTLine.appendChild(theNOTitle);
        newTLine.appendChild(theNumberMachine);
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      //  Placeholder
        newTLine = document.createElement("tr");
        var someCLTable = document.createElement("table");
        someCLTable.style="width:100%";
        newTLine2 = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine2.style="width:100%";
        newTLine2.id="ClusterInfo"+mimic;
        var someCLTable2 = document.createElement("table");
        someCLTable2.style="width:100%";
        newTLine3 = document.createElement("tr");
        newTLine3.style="width:100%";
        newTLine3.id="ClusterInfo2"+mimic;
        someCLTable2.appendChild(newTLine3);
        someCLTable.appendChild(newTLine2);
        newTLine.appendChild(someCLTable);
        newTLine.appendChild(someCLTable2);
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfCluster").value=mimic;
    }
    function AddClusterMachine(clusterNumb,ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="ClusterInfo"+clusterNumb;
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("machinesOnCluster"+clusterNumb).value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:7%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:10%";
          theStatusTitle.innerHTML = "Estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:8%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:7%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:8%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:10%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="RAM (Gb) :";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:10%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:10%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "Cluster"+clusterNumb+"Title";
          newTLine.id = "Cluster"+clusterNumb+"Title";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id=clusterNumb+"ClusterIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('3',numb,clusterNumb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name=clusterNumb+"IPCluster"+mimic;
            IPCount.id=clusterNumb+"IPCluster"+mimic;
            IPCount.value=0;
            IPCount.type="hidden"; 
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.id="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.value=clusterNumb;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="C"+clusterNumb+"tipo"+mimic;
            theTipo.id="C"+clusterNumb+"tipo"+mimic;
            theTipo.value=2;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="C"+clusterNumb+"id"+mimic;
            theId.id="C"+clusterNumb+"id"+mimic;
            theTipo.value=interId;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:7%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="C"+clusterNumb+"hostname"+mimic;
            theHostname.id="C"+clusterNumb+"hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:10%";
            theStatus = document.createElement("select");
            theStatus.name="C"+clusterNumb+"status"+mimic;
            theStatus.id="C"+clusterNumb+"status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
            theStatusPlace.innerHTML += '<br><br>Detalles :<br><br>';
          //  Status Detail
            theStatusDetail = document.createElement("input");
            theStatusDetail.style="width:100%";
            theStatusDetail.name="C"+clusterNumb+"statusDetail"+mimic;
            theStatusDetail.id="C"+clusterNumb+"statusDetail"+mimic;
            theStatusDetail.type="text";
            theStatusDetail.onkeypress = function(){return rev(event)}; 
            theStatusDetail.setAttribute("autocomplete", "off");
            theStatusPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:8%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="C"+clusterNumb+"infraDef"+mimic;
            theInfraDef.id="C"+clusterNumb+"infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.id="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:7%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="C"+clusterNumb+"hipervisor"+mimic;
            theHipervisor.id="C"+clusterNumb+"hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente
            theAmbientePlace = document.createElement("td");
            theAmbientePlace.style="width:8%";
            theAmbientePlace.innerHTML += 'Solicitado :<br><br>';
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteSolicitado);
            theAmbientePlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  ambiente entregado
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="C"+clusterNumb+"ambienteEntregado"+mimic;
            theAmbienteEntregado.id="C"+clusterNumb+"ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbientePlace.appendChild(theAmbienteEntregado);
          //  vCPU 
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:10%";
            thevCPUPlace.innerHTML += 'Solicitado :<br><br>';
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="C"+clusterNumb+"CPUS"+mimic;
            thevCPUSolicitado.id="C"+clusterNumb+"CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPUSolicitado);
            thevCPUPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  vCPU entregado
              thevCPUEntregado = document.createElement("input");
              thevCPUEntregado.name="C"+clusterNumb+"CPUE"+mimic;
              thevCPUEntregado.id="C"+clusterNumb+"CPUE"+mimic;
              thevCPUEntregado.type="number";
              thevCPUEntregado.style="width:100%";
              thevCPUEntregado.min=1;
              thevCPUEntregado.step=1;
              thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
              thevCPUPlace.appendChild(thevCPUEntregado);
          //  RAM 
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAMPlace.innerHTML += 'Solicitado :<br><br>';
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="C"+clusterNumb+"RAMS"+mimic;
            theRAMSolicitado.id="C"+clusterNumb+"RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAMSolicitado);
            theRAMPlace.innerHTML += '<br><br>Entregado :<br><br>';
            //  RAM Entregado
              theRAMEntregado = document.createElement("input");
              theRAMEntregado.name="C"+clusterNumb+"RAME"+mimic;
              theRAMEntregado.id="C"+clusterNumb+"RAME"+mimic;
              theRAMEntregado.type="number";
              theRAMEntregado.style="width:100%";
              theRAMEntregado.min=1;
              theRAMEntregado.step=1;
              theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
              theRAMPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:10%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="C"+clusterNumb+"TDOYM"+mimic;
            theTDOYM.id="C"+clusterNumb+"TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
            theTDOYMPlace.innerHTML += '<br><br>Status :<br><br>';
            //  Status OYM
              theStatusOYM = document.createElement("select");
              theStatusOYM.style="width:100%";
              theStatusOYM.name="C"+clusterNumb+"statusOYM"+mimic;
              theStatusOYM.id="C"+clusterNumb+"statusOYM"+mimic;
              var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
              for(i=0;i<possibleStatusOYM.length;i++)
              {
                opt = document.createElement('option');
                opt.value = possibleStatusOYM[i];
                opt.innerHTML = possibleStatusOYM[i];
                theStatusOYM.appendChild(opt);
              }
              theTDOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            if(mimic==0)
            {
              theSpecs1 = document.createElement("input");
              theSpecs1.type="button";
              theSpecs1.style="width:100%";
              theSpecs1.value="S.O.Requerimientos";
              theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
              theSpecs2 = document.createElement("input");
              theSpecs2.type="button";
              theSpecs2.style="width:100%";
              theSpecs2.value="DB Requerimientos";
              theSpecs2.onclick = function(){window.open('specs.php?accion=4&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            }
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            theSpecs4 = document.createElement('a');
            theSpecs4.href = 'reporteRemedy.php?folio='+interId;
            theSpecs4.innerText = 'Formato OyM';
            if(mimic==0)
            {
              theSpecsPlace.appendChild(theSpecs1); 
              theSpecsPlace.appendChild(theSpecs2);
            } 
            theSpecsPlace.appendChild(theSpecs3); 
            theSpecsPlace.appendChild(theSpecs4); 
          //  Line
            newLine.name = clusterNumb+"childCluster"+mimic.toString();
            newLine.id = clusterNumb+"childCluster"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbientePlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("machinesOnCluster"+clusterNumb).value=mimic;
    }
    function AddClusterMachine2 (clusterNumb,interId,folio)
    {
      //  recuperar informacion de equipo
        workplace1="ClusterInfo2"+clusterNumb;
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("machinesOnCluster"+clusterNumb).value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:10%";
          theDiscoTitle.innerHTML="Storage <br>Normal/Compartido:";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:10%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:10%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:10%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:10%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:10%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:10%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:10%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:10%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:10%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "Cluster"+clusterNumb+"Title2";
          newTLine.id = "Cluster"+clusterNumb+"Title2";
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:10%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Storage Normal/Compartido";
            theHD.onclick = function(){window.open('specs.php?accion=7&machine=c'+clusterNumb+'m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=600,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD); 
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:10%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="C"+clusterNumb+"internalConf"+mimic;
            theInternalConf.id="C"+clusterNumb+"internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:10%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="C"+clusterNumb+"remedy"+mimic;
            theRemedy.id="C"+clusterNumb+"remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:10%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="C"+clusterNumb+"soliMOP"+mimic;
            theSoliMOP.id="C"+clusterNumb+"soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:10%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="C"+clusterNumb+"entregaServer"+mimic;
            theEntregaServer.id="C"+clusterNumb+"entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:10%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="C"+clusterNumb+"entregaUser"+mimic;
            theEntregaUser.id="C"+clusterNumb+"entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:10%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="C"+clusterNumb+"inicioATP"+mimic;
            theInicioATP.id="C"+clusterNumb+"inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:10%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="C"+clusterNumb+"finATP"+mimic;
            theFInATP.id="C"+clusterNumb+"finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:10%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="C"+clusterNumb+"entregaOYM"+mimic;
            theEntregaOYM.id="C"+clusterNumb+"entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:10%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=400,height=450,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = clusterNumb+"childCluster2"+mimic.toString();
            newLine.id = clusterNumb+"childCluster2"+mimic.toString();
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function AddIP(type,diskInfo,cluster)
    {
      //  recuperar en que disco se trabajara
        if(type==1)
        { 
          workInThisDisk="StandaloneIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPStandalone"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd="IPStandalone"+diskInfo;
        }
        if(type==2)
        { 
          workInThisDisk="AplianceIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPApliance"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd="IPApliance"+diskInfo;
        }
        if(type==3)
        { 
          workInThisDisk=cluster+"ClusterIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic=cluster+"IPCluster"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd=cluster+"IPCluster"+diskInfo;
        }
      //  create nueva tabla de discos
        newLine = document.createElement("tr");
        if(type==1)
          newLine.id = diskInfo+"IPStandalone"+mimic;
        if(type==2)
          newLine.id = diskInfo+"IPApliance"+mimic;
        if(type==3)
          newLine.id = cluster+"machine"+diskInfo+"IPCluster"+mimic;
        newLine.style="width:100%";
      //  agregar IP
        theIPPlace2 = document.createElement("td");
        theIPPlace2.style="width:30%";
        theIP2 = document.createElement("input");
        theIP2.style="width:100%";
        if(type==1)
        {
          theIP2.name="s1IP"+diskInfo+"e"+mimic;
          theIP2.id="s1IP"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theIP2.name="a1IP"+diskInfo+"e"+mimic;
          theIP2.id="a1IP"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theIP2.name="c"+cluster+"IP"+diskInfo+"e"+mimic;
          theIP2.id="c"+cluster+"IP"+diskInfo+"e"+mimic;
        }
        theIP2.type="text";
        theIP2.onkeypress = function(){return isTheip(event)}; 
        theIP2.setAttribute("autocomplete", "off");
        theIPPlace2.appendChild(theIP2);
      //  añadir descripcion de disco
        theDescIPPlace = document.createElement("td");
        theDescIPPlace.style="width:32%";
        theDescIP = document.createElement("input");
        theDescIP.style="width:100%";
        if(type==1)
        {
          theDescIP.name="s1dip"+diskInfo+"e"+mimic;
          theDescIP.id="s1dip"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theDescIP.name="a1dip"+diskInfo+"e"+mimic;
          theDescIP.id="a1dip"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theDescIP.name="c"+cluster+"dip"+diskInfo+"e"+mimic;
          theDescIP.id="c"+cluster+"dip"+diskInfo+"e"+mimic;
        }
        theDescIP.type="text";
        theDescIP.setAttribute("autocomplete", "off");
        theDescIPPlace.appendChild(theDescIP);
      //  añadir boton para eliminar
        theButtonPlace = document.createElement("td");
        theButtonPlace.style="width:20%";
        theButtonPlace.class="eliminar";
        theButton = document.createElement("input");
        theButton.type="button";
        theButton.value="X";
        if(type==1)
        {
          var numb=diskInfo+"IPStandalone"+mimic;
          var prefix1="s1IP"+diskInfo+"e";
          var prefix2="s1dip"+diskInfo+"e";
          var prefix3=diskInfo+"IPStandalone";
        }
        if(type==2)
        {
          var numb=diskInfo+"IPApliance"+mimic;
          var prefix1="a1IP"+diskInfo+"e";
          var prefix2="a1dip"+diskInfo+"e";
          var prefix3=diskInfo+"IPApliance";
        }
        if(type==3)
        {
          var numb=cluster+"machine"+diskInfo+"IPCluster"+mimic;
          var prefix1="c"+cluster+"IP"+diskInfo+"e";
          var prefix2="c"+cluster+"dip"+diskInfo+"e";
          var prefix3=cluster+"machine"+diskInfo+"IPCluster";
        }
        var extraNumb=mimic;
        theButton.onclick = function(){killThisIP(prefix3,extraNumb,wkitd,prefix1,prefix2)}; 
        theButtonPlace.appendChild(theButton);
      //  agregar datos predefinidos a nueva tabla 
        newLine.appendChild(theIPPlace2);
        newLine.appendChild(theDescIPPlace);
        newLine.appendChild(theButtonPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.parentNode.insertBefore(newLine,workInThisDisk);
        mimic=parseInt(mimic)+1;
        if(type==1)
          document.getElementById("IPStandalone"+diskInfo).value=mimic;
        if(type==2)
          document.getElementById("IPApliance"+diskInfo).value=mimic;
        if(type==3)
          document.getElementById(cluster+"IPCluster"+diskInfo).value=mimic;
    }
    function killThisIP(prefix3,numero,wkitd,prefix1,prefix2)
    {
      //alert(prefix3+","+numero+","+wkitd+","+prefix1+","+prefix2);
      // recuperar informacion de equipo
        someMime=document.getElementById(wkitd).value;
        someMime=parseInt(someMime)-1;
      // eliminar
        if(numero!=someMime)
        {
          for(i=numero;i<someMime;i++)
          {
            // IP
                temp1=prefix1+(i+1);
                some=document.getElementById(temp1).value;
                temp1=prefix1+i;
                document.getElementById(temp1).value=some;
            // dip
                temp1=prefix2+(i+1);
                some=document.getElementById(temp1).value;
                temp1=prefix2+i;
                document.getElementById(temp1).value=some;
          }
        }
      document.getElementById(wkitd).value=someMime;
      var el =prefix3+someMime.toString();
      document.getElementById(el).remove();
    }
    function chargeThisMachine(datos,numero,Prefijo1)
    {
      //alert(numero);
      //alert(datos);
      bullets=datos.split(",");
      var bulletVal = ["id","hostname","status","statusDetail","infraDef","aplicacion","hipervisor","ambienteSolicitado","ambienteEntregado","CPUS","CPUE","RAMS","RAME","TDOYM","statusOYM"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(Prefijo1+bulletVal[hh]+numero).style="width:100%;background:#85807d;";
        else
          document.getElementById(Prefijo1+bulletVal[hh]+numero).value=bullets[hh];
      }
    }
    function chargeThisMachine2(datos,numero,Prefijo1)
    {
      //alert(numero);
      //alert(datos);
      bullets=datos.split(",");
      var bulletVal = ["internalConf","remedy","soliMOP","entregaServer","entregaUser","inicioATP","finATP","entregaOYM"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(Prefijo1+bulletVal[hh]+numero).style="width:100%;background:#85807d;";
        else
          document.getElementById(Prefijo1+bulletVal[hh]+numero).value=bullets[hh];
      }
    }
    function chargeIP(datos,maquinan,disco,prefijo)
    {
      bullets=datos.split(",");
      document.getElementById(prefijo+"IP"+maquinan+"e"+disco).value=bullets[0];
      document.getElementById(prefijo+"dip"+maquinan+"e"+disco).value=bullets[1];
    }
  </script>
  <body>
    <?php
      $sql="select nombre from ambiente";
      $toFillTemplate = mysqli_query($conn,$sql);
      $k=0;
      if(! $toFillTemplate)
        echo "<option value=\"\">Error de conexion</option>";
      else
        while($templateValues = mysqli_fetch_array($toFillTemplate))
        {
          if($k==0)
            $Ambiente=$templateValues['nombre'];
          else
            $Ambiente=$Ambiente.",".$templateValues['nombre'];
          $k=1;
        }
      $sql="select nombre from infdef";
      $toFillTemplate = mysqli_query($conn,$sql);
      $k=0;
      if(! $toFillTemplate)
        echo "<option value=\"\">Error de conexion</option>";
      else
        while($templateValues = mysqli_fetch_array($toFillTemplate))
        {
          if($k==0)
            $Infra=$templateValues['nombre'];
          else
            $Infra=$Infra.",".$templateValues['nombre'];
          $k=1;
        }
    ?>
    <form method="post"  action="moveMachineMode2.php" class="containerOfRod">
      <br><br>
      <!-- Botones Principales -->
        <div class="shad" align="center">
          <?php for($i=0;$i<8;$i++) echo "&nbsp;"; ?><input type="submit" value="Modificar datos de maquinas"> &nbsp;
        <!--
          <button id="addStandalone" name="addStandalone" type="button" class="btn btn-warning" onclick="giveSomeStandalone('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Standalone + </button>
          <button id="addApliance" name="addApliance" type="button" class="btn btn-warning" onclick="giveSomeApliance('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Apliance + </button>
          <button id="addCluster" name="addCluster" type="button" class="btn btn-warning" onclick="giveSomeCluster('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Cluster + </button>
        -->
        </div>
        <br><br>
      <!-- Valores rescatados temp -->
        <input type="hidden" name="folio" value="<?php echo $_POST['folio'];?>" >
      <!-- Placeholders -->
        <input type="hidden" id="childOfStandalone" name="childOfStandalone" value="0">
        <input type="hidden" id="childOfApliance" name="childOfApliance" value="0">
        <input type="hidden" id="childOfCluster" name="childOfCluster" value="0">
        <h2>Standalone:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="standalonePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="standalonePlaceholder2" ></tr>
        </table>
        <h2>Apliance:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="apliancePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="apliancePlaceholder2" ></tr>
        </table>
        <h2>Cluster:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="clusterPlaceholder" ></tr>
        </table>
      <!--  cargar maquinas -->
        <?php
          $sql="select interId,arreglo,tipo,aplicacion,ambienteSolicitado,ambienteEntregado,CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,nombre,infraestructuraDef,entregaUser,estatus,detalleEstatus,estatusOYM,remedy,fechaSoliMOP,fechaEntregaServer,TDOYM,hipervisor,netcard,intConfig,inicioPreATP,finPreATP,entregaOYM,F60 from maquinas where folio='".$_POST['folio']."'";
          //echo '<script type="text/javascript">prompt("aaaaaaaaa","'.$sql.'");</script>';
          $someThings = mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r>0)
          {
            $lastArreglo=-1;
            while($maquinaPr = mysqli_fetch_array($someThings))
            {
              $dato=substr($maquinaPr['interId'],strlen($_POST['folio']));   
              if($maquinaPr['tipo']==2)
              {
                if($maquinaPr['arreglo']!=$lastArreglo)
                {
                  echo '<script type="text/javascript">giveSomeCluster("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                  $lastArreglo=$maquinaPr['arreglo'];
                }
              }
              switch($dato[0])
              {
                case "s":
                  echo '<script type="text/javascript">giveSomeStandalone("'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">giveSomeStandalone2("'.$maquinaPr['interId'].'","'.$_POST['folio'].'");</script>';
                  $pos = strpos($dato, 'm');
                  $dato=substr($dato,($pos+1));
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","S1");</script>';
                  $infoMachine=$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","S1");</script>';
                  /*$sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    echo '<script type="text/javascript">AddStaticDisk("1","'.$dato.'");</script>';
                    $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                    echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                    $k++;
                  }*/
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("1","'.$dato.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                    $k++;
                  }
                break;
                case "a":
                  echo '<script type="text/javascript">giveSomeApliance("'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">giveSomeApliance2("'.$maquinaPr['interId'].'","'.$_POST['folio'].'");</script>';
                  $pos = strpos($dato, 'm');
                  $dato=substr($dato,($pos+1));
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","A1");</script>';
                  $infoMachine=$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","A1");</script>';
                  /*$sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    echo '<script type="text/javascript">AddStaticDisk("2","'.$dato.'");</script>';
                    $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                    echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                    $k++;
                  }*/
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("2","'.$dato.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                    $k++;
                  }
                break;
                case "c":
                  $pos = strpos($dato, 'm');
                  $clusterNumber=substr($dato,0,-(strlen($dato)-$pos));
                  $clusterNumber=substr($clusterNumber,1);
                  $dato=substr($dato,($pos+1));
                  echo '<script type="text/javascript">AddClusterMachine("'.$clusterNumber.'","'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">AddClusterMachine2("'.$clusterNumber.'","'.$maquinaPr['interId'].'","'.$_POST['folio'].'");</script>';
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","C'.$clusterNumber.'");</script>';
                  $infoMachine=$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","C'.$clusterNumber.'");</script>';
                  /*$sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  $t=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    if($actDisk['tipoDisco']=="Estatico")
                    {
                      echo '<script type="text/javascript">AddStaticDisk("3","'.$dato.'","'.$clusterNumber.'");</script>';
                      $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                      echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                      $k++;
                    }
                    if($actDisk['tipoDisco']=="Compartido")
                    {
                      echo '<script type="text/javascript">AddSharedDisk("'.$dato.'","'.$clusterNumber.'");</script>';
                      $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                      echo '<script type="text/javascript">chargeSharedDisk("'.$infoDisk.'","'.$dato.'","'.$t.'","'.$clusterNumber.'");</script>';
                      $t++;
                    }
                  }*/
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("3","'.$dato.'","'.$clusterNumber.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                    $k++;
                  }
                break;
              }
              //echo '<script type="text/javascript">alert("'.$dato.'");</script>';
            }
          }
        ?>
    </form>
  </body>
</html>