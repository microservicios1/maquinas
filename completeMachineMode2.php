<html lang="es"> 
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <title>Maquinas Virtuales</title>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
    ?>
  </head>
  <style>
    table
    {
      font-size: 10px;
    }
  </style>
  <script>
    function isdate(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=45))
        return false;
      return true;
    }
    function isTheip(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
        return false;
      return true;
    }
    function isIntNumber(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
    function rev(event)
    {
      var k = (event.which) ? event.which : event.keyCode;
      if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k ==32) )
        return true;
      else
        return false;
    }
    function giveSomeStandalone(ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="standalonePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStandalone").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:6%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:6%";
          theStatusTitle.innerHTML = "Estatus :";
          theStatusDetailTitle = document.createElement("th");
          theStatusDetailTitle.style="width:7%";
          theStatusDetailTitle.innerHTML = "Detalle<br/>de estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:5%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:5%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteSolicitadoTitle = document.createElement("th");
          theAmbienteSolicitadoTitle.style="width:6%";
          theAmbienteSolicitadoTitle.innerHTML="Ambiente<br/>Solicitado :";
          theAmbienteEntregadoTitle = document.createElement("th");
          theAmbienteEntregadoTitle.style="width:6%";
          theAmbienteEntregadoTitle.innerHTML="Ambiente<br/>Entregado :";
          thevCPUSolicitadoTitle = document.createElement("th");
          thevCPUSolicitadoTitle.style="width:5%";
          thevCPUSolicitadoTitle.innerHTML="vCPUs<br/>Solicitado:";
          thevCPUEntregadoTitle = document.createElement("th");
          thevCPUEntregadoTitle.style="width:5%";
          thevCPUEntregadoTitle.innerHTML="vCPUs<br/>Entregado:";
          theRAMSolicitadoTitle = document.createElement("th");
          theRAMSolicitadoTitle.style="width:5%";
          theRAMSolicitadoTitle.innerHTML="RAM (Gb)<br/>Solicitado:";
          theRAMEntregadoTitle = document.createElement("th");
          theRAMEntregadoTitle.style="width:5%";
          theRAMEntregadoTitle.innerHTML="RAM (Gb)<br/>Entregado:";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:5%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEstatusOYMTitle = document.createElement("th");
          theEstatusOYMTitle.style="width:5%";
          theEstatusOYMTitle.innerHTML = "Estatus<br/>de OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:9%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "StandaloneTitle";
          newTLine.id = "StandaloneTitle";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theStatusDetailTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteSolicitadoTitle);
          newTLine.appendChild(theAmbienteEntregadoTitle);
          newTLine.appendChild(thevCPUSolicitadoTitle);
          newTLine.appendChild(thevCPUEntregadoTitle);
          newTLine.appendChild(theRAMSolicitadoTitle);
          newTLine.appendChild(theRAMEntregadoTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEstatusOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="StandaloneIPPlaceholder"+mimic;
            /* buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('1',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            */
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            //someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPStandalone"+mimic;
            IPCount.id="IPStandalone"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="S1arreglo"+mimic;
            theArreglo.id="S1arreglo"+mimic;
            theArreglo.value=0;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="S1tipo"+mimic;
            theTipo.id="S1tipo"+mimic;
            theTipo.value=0;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="S1id"+mimic;
            theId.id="S1id"+mimic;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:6%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="S1hostname"+mimic;
            theHostname.id="S1hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:6%";
            theStatus = document.createElement("select");
            theStatus.name="S1status"+mimic;
            theStatus.id="S1status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
          //  Status Detail
            theStatusDetailPlace = document.createElement("td");
            theStatusDetailPlace.style="width:7%";
            theStatusDetail = document.createElement("input");
            theStatusDetail.style="width:100%";
            theStatusDetail.name="S1statusDetail"+mimic;
            theStatusDetail.id="S1statusDetail"+mimic;
            theStatusDetail.type="text";
            theStatusDetail.onkeypress = function(){return rev(event)}; 
            theStatusDetail.setAttribute("autocomplete", "off");
            theStatusDetailPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:5%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="S1infraDef"+mimic;
            theInfraDef.id="S1infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="S1aplicacion"+mimic;
            theAplication.id="S1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:5%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="S1hipervisor"+mimic;
            theHipervisor.id="S1hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente solicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:6%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  ambiente entregado
            theAmbienteEntregadoPlace = document.createElement("td");
            theAmbienteEntregadoPlace.style="width:6%";
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="S1ambienteEntregado"+mimic;
            theAmbienteEntregado.id="S1ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbienteEntregadoPlace.appendChild(theAmbienteEntregado);
          //  vCPU solicitado
            thevCPUSolicitadoPlace = document.createElement("td");
            thevCPUSolicitadoPlace.style="width:5%";
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="S1CPUS"+mimic;
            thevCPUSolicitado.id="S1CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUSolicitadoPlace.appendChild(thevCPUSolicitado);
          //  vCPU entregado
            thevCPUEntregadoPlace = document.createElement("td");
            thevCPUEntregadoPlace.style="width:5%";
            thevCPUEntregado = document.createElement("input");
            thevCPUEntregado.name="S1CPUE"+mimic;
            thevCPUEntregado.id="S1CPUE"+mimic;
            thevCPUEntregado.type="number";
            thevCPUEntregado.style="width:100%";
            thevCPUEntregado.min=1;
            thevCPUEntregado.step=1;
            thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUEntregadoPlace.appendChild(thevCPUEntregado);
          //  RAM Solicitado
            theRAMSolicitadoPlace = document.createElement("td");
            theRAMSolicitadoPlace.style="width:5%";
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="S1RAMS"+mimic;
            theRAMSolicitado.id="S1RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMSolicitadoPlace.appendChild(theRAMSolicitado);
          //  RAM Entregado
            theRAMEntregadoPlace = document.createElement("td");
            theRAMEntregadoPlace.style="width:5%";
            theRAMEntregado = document.createElement("input");
            theRAMEntregado.name="S1RAME"+mimic;
            theRAMEntregado.id="S1RAME"+mimic;
            theRAMEntregado.type="number";
            theRAMEntregado.style="width:100%";
            theRAMEntregado.min=1;
            theRAMEntregado.step=1;
            theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMEntregadoPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:5%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="S1TDOYM"+mimic;
            theTDOYM.id="S1TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
          //  Status OYM
            theStatusOYMPlace = document.createElement("td");
            theStatusOYMPlace.style="width:5%";
            theStatusOYM = document.createElement("select");
            theStatusOYM.style="width:100%";
            theStatusOYM.name="S1statusOYM"+mimic;
            theStatusOYM.id="S1statusOYM"+mimic;
            var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
            for(i=0;i<possibleStatusOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatusOYM[i];
              opt.innerHTML = possibleStatusOYM[i];
              theStatusOYM.appendChild(opt);
            }
            theStatusOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:9%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs2 = document.createElement("input");
            theSpecs2.type="button";
            theSpecs2.style="width:100%";
            theSpecs2.value="DB Requerimientos";
            theSpecs2.onclick = function(){window.open('specs.php?accion=4&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            theSpecsPlace.appendChild(theSpecs1); 
            theSpecsPlace.appendChild(theSpecs2); 
            theSpecsPlace.appendChild(theSpecs3); 
          //  Line
            newLine.name = "childStandalone"+mimic.toString();
            newLine.id = "childStandalone"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theStatusDetailPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(theAmbienteEntregadoPlace);
            newLine.appendChild(thevCPUSolicitadoPlace);
            newLine.appendChild(thevCPUEntregadoPlace);
            newLine.appendChild(theRAMSolicitadoPlace);
            newLine.appendChild(theRAMEntregadoPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theStatusOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfStandalone").value=mimic;
    }
    function giveSomeStandalone2(interId)
    {
      //  recuperar informacion de equipo
        workplace1="standalonePlaceholder2";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStandalone").value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoSolicitadoTitle = document.createElement("th");
          theDiscoSolicitadoTitle.style="width:19%";
          theDiscoSolicitadoTitle.innerHTML="Disco Duro Solicitado :";
          theDiscoEntregadoTitle = document.createElement("th");
          theDiscoEntregadoTitle.style="width:19%";
          theDiscoEntregadoTitle.innerHTML="Disco Duro Entregado :";
          theNetCardTitle = document.createElement("th");
          theNetCardTitle.style="width:6%";
          theNetCardTitle.innerHTML = "Tarjetas<br />de Red:";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:6%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:6%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:6%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:6%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:6%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:6%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:6%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:6%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:8%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "StandaloneTitle2";
          newTLine.id = "StandaloneTitle2";
          newTLine.appendChild(theDiscoSolicitadoTitle);
          newTLine.appendChild(theDiscoEntregadoTitle);
          newTLine.appendChild(theNetCardTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro Solicitado
            theHDSPlace = document.createElement("td");
            theHDSPlace.style="width:19%";
            var someTable = document.createElement("table");
            someTable.style="width:100%";
            newLineIn = document.createElement("tr");
            newLineIn.style="width:100%";
            // Titulos
              theDiskName = document.createElement("th");
              theDiskName.style="width:35%";
              theDiskName.innerHTML = "Nombre :";
              theDiskSize = document.createElement("th");
              theDiskSize.style="width:25%";
              theDiskSize.innerHTML="Tamano :";
              theDiskDescription = document.createElement("th");
              theDiskDescription.style="width:40%";
              theDiskDescription.innerHTML="Descripcion :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineIn.appendChild(theDiskName);
              newLineIn.appendChild(theDiskSize);
              newLineIn.appendChild(theDiskDescription);
              //newLineIn.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholder = document.createElement("tr");
              newLinePlaceholder.style="width:100%";
              newLinePlaceholder.id="StandaloneSPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="Button"+mimic;
              theButton.id="Button"+mimic;
              theButton.value="Disk++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTable.appendChild(newLineIn);  
            someTable.appendChild(newLinePlaceholder); 
            //someTable.appendChild(theButtonPlace);  
            theHDSPlace.appendChild(someTable);
            DiskCount = document.createElement("input");
            DiskCount.name="DiskStaticStandaloneS"+mimic;
            DiskCount.id="DiskStaticStandaloneS"+mimic;
            DiskCount.value=0;
            DiskCount.type="hidden";
          //  Disco Duro Entregado
            theHDEPlace = document.createElement("td");
            theHDEPlace.style="width:19%";
            var someTableE = document.createElement("table");
            someTableE.style="width:100%";
            newLineOut = document.createElement("tr");
            newLineOut.style="width:100%";
            // Titulos
              theDiskName2 = document.createElement("th");
              theDiskName2.style="width:35%";
              theDiskName2.innerHTML = "Nombre :";
              theDiskSize2 = document.createElement("th");
              theDiskSize2.style="width:25%";
              theDiskSize2.innerHTML="Tamano :";
              theDiskDescription2 = document.createElement("th");
              theDiskDescription2.style="width:40%";
              theDiskDescription2.innerHTML="Notas :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineOut.appendChild(theDiskName2);
              newLineOut.appendChild(theDiskSize2);
              newLineOut.appendChild(theDiskDescription2);
              //newLineOut.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholderE = document.createElement("tr");
              newLinePlaceholderE.style="width:100%";
              newLinePlaceholderE.id="StandaloneEPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="ButtonSE"+mimic;
              theButton.id="ButtonSE"+mimic;
              theButton.value="++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTableE.appendChild(newLineOut);  
            someTableE.appendChild(newLinePlaceholderE); 
            //someTableE.appendChild(theButtonPlace);  
            theHDEPlace.appendChild(someTableE);
            DiskCountE = document.createElement("input");
            DiskCountE.name="DiskStaticStandaloneE"+mimic;
            DiskCountE.id="DiskStaticStandaloneE"+mimic;
            DiskCountE.value=0;
            DiskCountE.type="hidden";
          //  netCard
            theNetCardPlace = document.createElement("td");
            theNetCardPlace.style="width:6%";
            theNetCard = document.createElement("input");
            theNetCard.style="width:100%;";
            theNetCard.name="S1netcard"+mimic;
            theNetCard.id="S1netcard"+mimic;
            theNetCard.type="text";
            theNetCard.onkeypress = function(){return rev(event)}; 
            theNetCard.setAttribute("autocomplete", "off");
            theNetCardPlace.appendChild(theNetCard);
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:6%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="S1internalConf"+mimic;
            theInternalConf.id="S1internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:6%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="S1remedy"+mimic;
            theRemedy.id="S1remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:6%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="S1soliMOP"+mimic;
            theSoliMOP.id="S1soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:6%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="S1entregaServer"+mimic;
            theEntregaServer.id="S1entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:6%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="S1entregaUser"+mimic;
            theEntregaUser.id="S1entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:6%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="S1inicioATP"+mimic;
            theInicioATP.id="S1inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:6%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="S1finATP"+mimic;
            theFInATP.id="S1finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:6%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="S1entregaOYM"+mimic;
            theEntregaOYM.id="S1entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:8%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=350,height=400,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = "childStandalone2"+mimic.toString();
            newLine.id = "childStandalone2"+mimic.toString();
            newLine.appendChild(theHDSPlace);
            newLine.appendChild(DiskCount);
            newLine.appendChild(theHDEPlace);
            newLine.appendChild(DiskCountE);
            newLine.appendChild(theNetCardPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function giveSomeApliance(ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="apliancePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfApliance").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:6%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:6%";
          theStatusTitle.innerHTML = "Estatus :";
          theStatusDetailTitle = document.createElement("th");
          theStatusDetailTitle.style="width:7%";
          theStatusDetailTitle.innerHTML = "Detalle<br/>de estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:5%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:5%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteSolicitadoTitle = document.createElement("th");
          theAmbienteSolicitadoTitle.style="width:6%";
          theAmbienteSolicitadoTitle.innerHTML="Ambiente<br/>Solicitado :";
          theAmbienteEntregadoTitle = document.createElement("th");
          theAmbienteEntregadoTitle.style="width:6%";
          theAmbienteEntregadoTitle.innerHTML="Ambiente<br/>Entregado :";
          thevCPUSolicitadoTitle = document.createElement("th");
          thevCPUSolicitadoTitle.style="width:5%";
          thevCPUSolicitadoTitle.innerHTML="vCPUs<br/>Solicitado:";
          thevCPUEntregadoTitle = document.createElement("th");
          thevCPUEntregadoTitle.style="width:5%";
          thevCPUEntregadoTitle.innerHTML="vCPUs<br/>Entregado:";
          theRAMSolicitadoTitle = document.createElement("th");
          theRAMSolicitadoTitle.style="width:5%";
          theRAMSolicitadoTitle.innerHTML="RAM (Gb)<br/>Solicitado:";
          theRAMEntregadoTitle = document.createElement("th");
          theRAMEntregadoTitle.style="width:5%";
          theRAMEntregadoTitle.innerHTML="RAM (Gb)<br/>Entregado:";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:5%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEstatusOYMTitle = document.createElement("th");
          theEstatusOYMTitle.style="width:5%";
          theEstatusOYMTitle.innerHTML = "Estatus<br/>de OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:9%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "AplianceTitle";
          newTLine.id = "AplianceTitle";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theStatusDetailTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteSolicitadoTitle);
          newTLine.appendChild(theAmbienteEntregadoTitle);
          newTLine.appendChild(thevCPUSolicitadoTitle);
          newTLine.appendChild(thevCPUEntregadoTitle);
          newTLine.appendChild(theRAMSolicitadoTitle);
          newTLine.appendChild(theRAMEntregadoTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEstatusOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="AplianceIPPlaceholder"+mimic;
            /* buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('1',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            */
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            //someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPApliance"+mimic;
            IPCount.id="IPApliance"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="A1arreglo"+mimic;
            theArreglo.id="A1arreglo"+mimic;
            theArreglo.value=0;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="A1tipo"+mimic;
            theTipo.id="A1tipo"+mimic;
            theTipo.value=0;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="A1id"+mimic;
            theId.id="A1id"+mimic;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:6%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="A1hostname"+mimic;
            theHostname.id="A1hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:6%";
            theStatus = document.createElement("select");
            theStatus.name="A1status"+mimic;
            theStatus.id="A1status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
          //  Status Detail
            theStatusDetailPlace = document.createElement("td");
            theStatusDetailPlace.style="width:7%";
            theStatusDetail = document.createElement("input");
            theStatusDetail.style="width:100%";
            theStatusDetail.name="A1statusDetail"+mimic;
            theStatusDetail.id="A1statusDetail"+mimic;
            theStatusDetail.type="text";
            theStatusDetail.onkeypress = function(){return rev(event)}; 
            theStatusDetail.setAttribute("autocomplete", "off");
            theStatusDetailPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:5%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="A1infraDef"+mimic;
            theInfraDef.id="A1infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="A1aplicacion"+mimic;
            theAplication.id="A1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:5%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="A1hipervisor"+mimic;
            theHipervisor.id="A1hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente solicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:6%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  ambiente entregado
            theAmbienteEntregadoPlace = document.createElement("td");
            theAmbienteEntregadoPlace.style="width:6%";
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="A1ambienteEntregado"+mimic;
            theAmbienteEntregado.id="A1ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbienteEntregadoPlace.appendChild(theAmbienteEntregado);
          //  vCPU solicitado
            thevCPUSolicitadoPlace = document.createElement("td");
            thevCPUSolicitadoPlace.style="width:5%";
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="A1CPUS"+mimic;
            thevCPUSolicitado.id="A1CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUSolicitadoPlace.appendChild(thevCPUSolicitado);
          //  vCPU entregado
            thevCPUEntregadoPlace = document.createElement("td");
            thevCPUEntregadoPlace.style="width:5%";
            thevCPUEntregado = document.createElement("input");
            thevCPUEntregado.name="A1CPUE"+mimic;
            thevCPUEntregado.id="A1CPUE"+mimic;
            thevCPUEntregado.type="number";
            thevCPUEntregado.style="width:100%";
            thevCPUEntregado.min=1;
            thevCPUEntregado.step=1;
            thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUEntregadoPlace.appendChild(thevCPUEntregado);
          //  RAM Solicitado
            theRAMSolicitadoPlace = document.createElement("td");
            theRAMSolicitadoPlace.style="width:5%";
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="A1RAMS"+mimic;
            theRAMSolicitado.id="A1RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMSolicitadoPlace.appendChild(theRAMSolicitado);
          //  RAM Entregado
            theRAMEntregadoPlace = document.createElement("td");
            theRAMEntregadoPlace.style="width:5%";
            theRAMEntregado = document.createElement("input");
            theRAMEntregado.name="A1RAME"+mimic;
            theRAMEntregado.id="A1RAME"+mimic;
            theRAMEntregado.type="number";
            theRAMEntregado.style="width:100%";
            theRAMEntregado.min=1;
            theRAMEntregado.step=1;
            theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMEntregadoPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:5%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="A1TDOYM"+mimic;
            theTDOYM.id="A1TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
          //  Status OYM
            theStatusOYMPlace = document.createElement("td");
            theStatusOYMPlace.style="width:5%";
            theStatusOYM = document.createElement("select");
            theStatusOYM.style="width:100%";
            theStatusOYM.name="A1statusOYM"+mimic;
            theStatusOYM.id="A1statusOYM"+mimic;
            var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
            for(i=0;i<possibleStatusOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatusOYM[i];
              opt.innerHTML = possibleStatusOYM[i];
              theStatusOYM.appendChild(opt);
            }
            theStatusOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:9%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            theSpecsPlace.appendChild(theSpecs1); 
            theSpecsPlace.appendChild(theSpecs3); 
          //  Line
            newLine.name = "childApliance"+mimic.toString();
            newLine.id = "childApliance"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theStatusDetailPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(theAmbienteEntregadoPlace);
            newLine.appendChild(thevCPUSolicitadoPlace);
            newLine.appendChild(thevCPUEntregadoPlace);
            newLine.appendChild(theRAMSolicitadoPlace);
            newLine.appendChild(theRAMEntregadoPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theStatusOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfApliance").value=mimic;
    }
    function giveSomeApliance2(interId)
    {
      //  recuperar informacion de equipo
        workplace1="apliancePlaceholder2";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfApliance").value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoSolicitadoTitle = document.createElement("th");
          theDiscoSolicitadoTitle.style="width:19%";
          theDiscoSolicitadoTitle.innerHTML="Disco Duro Solicitado :";
          theDiscoEntregadoTitle = document.createElement("th");
          theDiscoEntregadoTitle.style="width:19%";
          theDiscoEntregadoTitle.innerHTML="Disco Duro Entregado :";
          theNetCardTitle = document.createElement("th");
          theNetCardTitle.style="width:6%";
          theNetCardTitle.innerHTML = "Tarjetas<br />de Red:";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:6%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:6%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:6%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:6%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:6%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:6%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:6%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:6%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:8%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "AplianceTitle2";
          newTLine.id = "AplianceTitle2";
          newTLine.appendChild(theDiscoSolicitadoTitle);
          newTLine.appendChild(theDiscoEntregadoTitle);
          newTLine.appendChild(theNetCardTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro Solicitado
            theHDSPlace = document.createElement("td");
            theHDSPlace.style="width:19%";
            var someTable = document.createElement("table");
            someTable.style="width:100%";
            newLineIn = document.createElement("tr");
            newLineIn.style="width:100%";
            // Titulos
              theDiskName = document.createElement("th");
              theDiskName.style="width:35%";
              theDiskName.innerHTML = "Nombre :";
              theDiskSize = document.createElement("th");
              theDiskSize.style="width:25%";
              theDiskSize.innerHTML="Tamano :";
              theDiskDescription = document.createElement("th");
              theDiskDescription.style="width:40%";
              theDiskDescription.innerHTML="Descripcion :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineIn.appendChild(theDiskName);
              newLineIn.appendChild(theDiskSize);
              newLineIn.appendChild(theDiskDescription);
              //newLineIn.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholder = document.createElement("tr");
              newLinePlaceholder.style="width:100%";
              newLinePlaceholder.id="AplianceSPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="Button"+mimic;
              theButton.id="Button"+mimic;
              theButton.value="Disk++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTable.appendChild(newLineIn);  
            someTable.appendChild(newLinePlaceholder); 
            //someTable.appendChild(theButtonPlace);  
            theHDSPlace.appendChild(someTable);
            DiskCount = document.createElement("input");
            DiskCount.name="DiskStaticAplianceS"+mimic;
            DiskCount.id="DiskStaticAplianceS"+mimic;
            DiskCount.value=0;
            DiskCount.type="hidden";
          //  Disco Duro Entregado
            theHDEPlace = document.createElement("td");
            theHDEPlace.style="width:19%";
            var someTableE = document.createElement("table");
            someTableE.style="width:100%";
            newLineOut = document.createElement("tr");
            newLineOut.style="width:100%";
            // Titulos
              theDiskName2 = document.createElement("th");
              theDiskName2.style="width:35%";
              theDiskName2.innerHTML = "Nombre :";
              theDiskSize2 = document.createElement("th");
              theDiskSize2.style="width:25%";
              theDiskSize2.innerHTML="Tamano :";
              theDiskDescription2 = document.createElement("th");
              theDiskDescription2.style="width:40%";
              theDiskDescription2.innerHTML="Notas :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineOut.appendChild(theDiskName2);
              newLineOut.appendChild(theDiskSize2);
              newLineOut.appendChild(theDiskDescription2);
              //newLineOut.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholderE = document.createElement("tr");
              newLinePlaceholderE.style="width:100%";
              newLinePlaceholderE.id="AplianceEPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="ButtonSE"+mimic;
              theButton.id="ButtonSE"+mimic;
              theButton.value="++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTableE.appendChild(newLineOut);  
            someTableE.appendChild(newLinePlaceholderE); 
            //someTableE.appendChild(theButtonPlace);  
            theHDEPlace.appendChild(someTableE);
            DiskCountE = document.createElement("input");
            DiskCountE.name="DiskStaticAplianceE"+mimic;
            DiskCountE.id="DiskStaticAplianceE"+mimic;
            DiskCountE.value=0;
            DiskCountE.type="hidden";
          //  netCard
            theNetCardPlace = document.createElement("td");
            theNetCardPlace.style="width:6%";
            theNetCard = document.createElement("input");
            theNetCard.style="width:100%;";
            theNetCard.name="A1netcard"+mimic;
            theNetCard.id="A1netcard"+mimic;
            theNetCard.type="text";
            theNetCard.onkeypress = function(){return rev(event)}; 
            theNetCard.setAttribute("autocomplete", "off");
            theNetCardPlace.appendChild(theNetCard);
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:6%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="A1internalConf"+mimic;
            theInternalConf.id="A1internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:6%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="A1remedy"+mimic;
            theRemedy.id="A1remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:6%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="A1soliMOP"+mimic;
            theSoliMOP.id="A1soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:6%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="A1entregaServer"+mimic;
            theEntregaServer.id="A1entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:6%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="A1entregaUser"+mimic;
            theEntregaUser.id="A1entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:6%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="A1inicioATP"+mimic;
            theInicioATP.id="A1inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:6%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="A1finATP"+mimic;
            theFInATP.id="A1finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:6%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="A1entregaOYM"+mimic;
            theEntregaOYM.id="A1entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:8%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=350,height=400,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = "childApliance2"+mimic.toString();
            newLine.id = "childApliance2"+mimic.toString();
            newLine.appendChild(theHDSPlace);
            newLine.appendChild(DiskCount);
            newLine.appendChild(theHDEPlace);
            newLine.appendChild(DiskCountE);
            newLine.appendChild(theNetCardPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function giveSomeCluster(folio)
    {
      //  recuperar informacion de equipo
        workplace1="clusterPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfCluster").value;
        mimic=parseInt(mimic);
      //  Titulos
        newTLine = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine.id="ClusterTitle"+mimic;
        theNOTitle = document.createElement("th");
        theNOTitle.innerHTML = "Cluster "+parseInt(mimic+1)+":";
        theNOTitle.style="font-size: 12px;";
        theNumberMachine = document.createElement("input");
        theNumberMachine.name="machinesOnCluster"+mimic;
        theNumberMachine.id="machinesOnCluster"+mimic;
        theNumberMachine.value=0;
        theNumberMachine.type="hidden";
        newTLine.name = "ClusterTitle"+mimic;
        newTLine.id = "ClusterTitle"+mimic;
        newTLine.appendChild(theNOTitle);
        newTLine.appendChild(theNumberMachine);
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      //  Placeholder
        newTLine = document.createElement("tr");
        var someCLTable = document.createElement("table");
        someCLTable.style="width:100%";
        newTLine2 = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine2.style="width:100%";
        newTLine2.id="ClusterInfo"+mimic;
        var someCLTable2 = document.createElement("table");
        someCLTable2.style="width:100%";
        newTLine3 = document.createElement("tr");
        newTLine3.style="width:100%";
        newTLine3.id="ClusterInfo2"+mimic;
        someCLTable2.appendChild(newTLine3);
        someCLTable.appendChild(newTLine2);
        newTLine.appendChild(someCLTable);
        newTLine.appendChild(someCLTable2);
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfCluster").value=mimic;
    }
    function AddClusterMachine(clusterNumb,ambiente,folio,infra,interId)
    {
      //  recuperar informacion de equipo
        workplace1="ClusterInfo"+clusterNumb;
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("machinesOnCluster"+clusterNumb).value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:12%";
          theIPTitle.innerHTML = "Interfaces :";
          theNameTitle = document.createElement("th");
          theNameTitle.style="width:6%";
          theNameTitle.innerHTML = "Nombre :";
          theStatusTitle = document.createElement("th");
          theStatusTitle.style="width:6%";
          theStatusTitle.innerHTML = "Estatus :";
          theStatusDetailTitle = document.createElement("th");
          theStatusDetailTitle.style="width:7%";
          theStatusDetailTitle.innerHTML = "Detalle<br/>de estatus :";
          theInfraDefTitle = document.createElement("th");
          theInfraDefTitle.style="width:5%";
          theInfraDefTitle.innerHTML = "Infra.<br/>Definida :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:8%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theHipervisorTitle = document.createElement("th");
          theHipervisorTitle.style="width:5%";
          theHipervisorTitle.innerHTML = "Hipervisor :";
          theAmbienteSolicitadoTitle = document.createElement("th");
          theAmbienteSolicitadoTitle.style="width:6%";
          theAmbienteSolicitadoTitle.innerHTML="Ambiente<br/>Solicitado :";
          theAmbienteEntregadoTitle = document.createElement("th");
          theAmbienteEntregadoTitle.style="width:6%";
          theAmbienteEntregadoTitle.innerHTML="Ambiente<br/>Entregado :";
          thevCPUSolicitadoTitle = document.createElement("th");
          thevCPUSolicitadoTitle.style="width:5%";
          thevCPUSolicitadoTitle.innerHTML="vCPUs<br/>Solicitado:";
          thevCPUEntregadoTitle = document.createElement("th");
          thevCPUEntregadoTitle.style="width:5%";
          thevCPUEntregadoTitle.innerHTML="vCPUs<br/>Entregado:";
          theRAMSolicitadoTitle = document.createElement("th");
          theRAMSolicitadoTitle.style="width:5%";
          theRAMSolicitadoTitle.innerHTML="RAM (Gb)<br/>Solicitado:";
          theRAMEntregadoTitle = document.createElement("th");
          theRAMEntregadoTitle.style="width:5%";
          theRAMEntregadoTitle.innerHTML="RAM (Gb)<br/>Entregado:";
          theTDOYMTitle = document.createElement("th");
          theTDOYMTitle.style="width:5%";
          theTDOYMTitle.innerHTML = "TD/OYM :";
          theEstatusOYMTitle = document.createElement("th");
          theEstatusOYMTitle.style="width:5%";
          theEstatusOYMTitle.innerHTML = "Estatus<br/>de OYM :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:9%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "Cluster"+clusterNumb+"Title";
          newTLine.id = "Cluster"+clusterNumb+"Title";
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theNameTitle);
          newTLine.appendChild(theStatusTitle);
          newTLine.appendChild(theStatusDetailTitle);
          newTLine.appendChild(theInfraDefTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theHipervisorTitle);
          newTLine.appendChild(theAmbienteSolicitadoTitle);
          newTLine.appendChild(theAmbienteEntregadoTitle);
          newTLine.appendChild(thevCPUSolicitadoTitle);
          newTLine.appendChild(thevCPUEntregadoTitle);
          newTLine.appendChild(theRAMSolicitadoTitle);
          newTLine.appendChild(theRAMEntregadoTitle);
          newTLine.appendChild(theTDOYMTitle);
          newTLine.appendChild(theEstatusOYMTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:12%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "Interfaz :";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id=clusterNumb+"ClusterIPPlaceholder"+mimic;
            /* buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="Interfaz++";
              theIPButton.onclick = function(){AddIP('1',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            */
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            //someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name=clusterNumb+"IPCluster"+mimic;
            IPCount.id=clusterNumb+"IPCluster"+mimic;
            IPCount.value=0;
            IPCount.type="hidden"; 
          //  hidden  Data
            theArreglo = document.createElement("input");
            theArreglo.name="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.id="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.value=clusterNumb;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="C"+clusterNumb+"tipo"+mimic;
            theTipo.id="C"+clusterNumb+"tipo"+mimic;
            theTipo.value=2;
            theTipo.type="hidden";
            theId = document.createElement("input");
            theId.name="C"+clusterNumb+"id"+mimic;
            theId.id="C"+clusterNumb+"id"+mimic;
            theTipo.value=interId;
            theId.type="hidden";
          //  nombre
            theHostnamePlace = document.createElement("td");
            theHostnamePlace.style="width:6%";
            theHostname = document.createElement("input");
            theHostname.style="width:100%;";
            theHostname.name="C"+clusterNumb+"hostname"+mimic;
            theHostname.id="C"+clusterNumb+"hostname"+mimic;
            theHostname.type="text";
            theHostname.onkeypress = function(){return rev(event)}; 
            theHostname.setAttribute("autocomplete", "off");
            theHostnamePlace.appendChild(theHostname);
          //  Status
            theStatusPlace = document.createElement("td");
            theStatusPlace.style="width:6%";
            theStatus = document.createElement("select");
            theStatus.name="C"+clusterNumb+"status"+mimic;
            theStatus.id="C"+clusterNumb+"status"+mimic;
            theStatus.required=true; 
            theStatus.style="width:100%";
            var possibleStatus = ["","EN PROCESO","PENDIENTE","ENTREGADO","CANCELADO"];
            for(i=0;i<possibleStatus.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatus[i];
              opt.innerHTML = possibleStatus[i];
              theStatus.appendChild(opt);
            }
            theStatusPlace.appendChild(theStatus);
          //  Status Detail
            theStatusDetailPlace = document.createElement("td");
            theStatusDetailPlace.style="width:7%";
            theStatusDetail = document.createElement("input");
            theStatusDetail.style="width:100%";
            theStatusDetail.name="C"+clusterNumb+"statusDetail"+mimic;
            theStatusDetail.id="C"+clusterNumb+"statusDetail"+mimic;
            theStatusDetail.type="text";
            theStatusDetail.onkeypress = function(){return rev(event)}; 
            theStatusDetail.setAttribute("autocomplete", "off");
            theStatusDetailPlace.appendChild(theStatusDetail);
          //  InfraDef
            theInfraDefPlace = document.createElement("td");
            theInfraDefPlace.style="width:5%";
            theInfraDef = document.createElement("select");
            theInfraDef.name="C"+clusterNumb+"infraDef"+mimic;
            theInfraDef.id="C"+clusterNumb+"infraDef"+mimic; 
            theInfraDef.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theInfraDef.appendChild(opt);
            var InfraDefData = infra.split(",");
            for(i=0;i<InfraDefData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = InfraDefData[i];
              opt.innerHTML = InfraDefData[i];
              theInfraDef.appendChild(opt);
            }
            theInfraDefPlace.appendChild(theInfraDef);
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:8%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.id="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  Hipervisor
            theHipervisorPlace = document.createElement("td");
            theHipervisorPlace.style="width:5%";
            theHipervisor = document.createElement("input");
            theHipervisor.style="width:100%";
            theHipervisor.name="C"+clusterNumb+"hipervisor"+mimic;
            theHipervisor.id="C"+clusterNumb+"hipervisor"+mimic;
            theHipervisor.type="text";
            theHipervisor.onkeypress = function(){return rev(event)}; 
            theHipervisorPlace.appendChild(theHipervisor);
          //  ambiente solicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:6%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  ambiente entregado
            theAmbienteEntregadoPlace = document.createElement("td");
            theAmbienteEntregadoPlace.style="width:6%";
            theAmbienteEntregado = document.createElement("select");
            theAmbienteEntregado.name="C"+clusterNumb+"ambienteEntregado"+mimic;
            theAmbienteEntregado.id="C"+clusterNumb+"ambienteEntregado"+mimic; 
            theAmbienteEntregado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteEntregado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteEntregado.appendChild(opt);
            }
            theAmbienteEntregadoPlace.appendChild(theAmbienteEntregado);
          //  vCPU solicitado
            thevCPUSolicitadoPlace = document.createElement("td");
            thevCPUSolicitadoPlace.style="width:5%";
            thevCPUSolicitado = document.createElement("input");
            thevCPUSolicitado.name="C"+clusterNumb+"CPUS"+mimic;
            thevCPUSolicitado.id="C"+clusterNumb+"CPUS"+mimic;
            thevCPUSolicitado.type="number";
            thevCPUSolicitado.style="width:100%";
            thevCPUSolicitado.min=1;
            thevCPUSolicitado.step=1;
            thevCPUSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUSolicitadoPlace.appendChild(thevCPUSolicitado);
          //  vCPU entregado
            thevCPUEntregadoPlace = document.createElement("td");
            thevCPUEntregadoPlace.style="width:5%";
            thevCPUEntregado = document.createElement("input");
            thevCPUEntregado.name="C"+clusterNumb+"CPUE"+mimic;
            thevCPUEntregado.id="C"+clusterNumb+"CPUE"+mimic;
            thevCPUEntregado.type="number";
            thevCPUEntregado.style="width:100%";
            thevCPUEntregado.min=1;
            thevCPUEntregado.step=1;
            thevCPUEntregado.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUEntregadoPlace.appendChild(thevCPUEntregado);
          //  RAM Solicitado
            theRAMSolicitadoPlace = document.createElement("td");
            theRAMSolicitadoPlace.style="width:5%";
            theRAMSolicitado = document.createElement("input");
            theRAMSolicitado.name="C"+clusterNumb+"RAMS"+mimic;
            theRAMSolicitado.id="C"+clusterNumb+"RAMS"+mimic;
            theRAMSolicitado.type="number";
            theRAMSolicitado.style="width:100%";
            theRAMSolicitado.min=1;
            theRAMSolicitado.step=1;
            theRAMSolicitado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMSolicitadoPlace.appendChild(theRAMSolicitado);
          //  RAM Entregado
            theRAMEntregadoPlace = document.createElement("td");
            theRAMEntregadoPlace.style="width:5%";
            theRAMEntregado = document.createElement("input");
            theRAMEntregado.name="C"+clusterNumb+"RAME"+mimic;
            theRAMEntregado.id="C"+clusterNumb+"RAME"+mimic;
            theRAMEntregado.type="number";
            theRAMEntregado.style="width:100%";
            theRAMEntregado.min=1;
            theRAMEntregado.step=1;
            theRAMEntregado.onkeypress = function(){return isIntNumber(event)}; 
            theRAMEntregadoPlace.appendChild(theRAMEntregado);
          //  TD/OYM
            theTDOYMPlace = document.createElement("td");
            theTDOYMPlace.style="width:5%";
            theTDOYM = document.createElement("select");
            theTDOYM.name="C"+clusterNumb+"TDOYM"+mimic;
            theTDOYM.id="C"+clusterNumb+"TDOYM"+mimic;
            theTDOYM.style="width:100%";
            var possibleTDOYM = ["","TD","OYM"];
            for(i=0;i<possibleTDOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleTDOYM[i];
              opt.innerHTML = possibleTDOYM[i];
              theTDOYM.appendChild(opt);
            }
            theTDOYMPlace.appendChild(theTDOYM);
          //  Status OYM
            theStatusOYMPlace = document.createElement("td");
            theStatusOYMPlace.style="width:5%";
            theStatusOYM = document.createElement("select");
            theStatusOYM.style="width:100%";
            theStatusOYM.name="C"+clusterNumb+"statusOYM"+mimic;
            theStatusOYM.id="C"+clusterNumb+"statusOYM"+mimic;
            var possibleStatusOYM = ["","Baja","En implementacion","En TD","Entregado a OYM","Para desarrollo","productivo sin entregar a OYM","Standby"];
            for(i=0;i<possibleStatusOYM.length;i++)
            {
              opt = document.createElement('option');
              opt.value = possibleStatusOYM[i];
              opt.innerHTML = possibleStatusOYM[i];
              theStatusOYM.appendChild(opt);
            }
            theStatusOYMPlace.appendChild(theStatusOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:9%";
            if(mimic==0)
            {
              theSpecs1 = document.createElement("input");
              theSpecs1.type="button";
              theSpecs1.style="width:100%";
              theSpecs1.value="S.O.Requerimientos";
              theSpecs1.onclick = function(){window.open('specs.php?accion=3&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
              theSpecs2 = document.createElement("input");
              theSpecs2.type="button";
              theSpecs2.style="width:100%";
              theSpecs2.value="DB Requerimientos";
              theSpecs2.onclick = function(){window.open('specs.php?accion=4&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            }
            theSpecs3 = document.createElement("input");
            theSpecs3.type="button";
            theSpecs3.style="width:100%";
            theSpecs3.value="Historico/notas";
            theSpecs3.onclick = function(){window.open('specs.php?accion=5&machine='+interId+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=400,resizable=0,left=40px,top=50px')};
            if(mimic==0)
            {
              theSpecsPlace.appendChild(theSpecs1); 
              theSpecsPlace.appendChild(theSpecs2);
            } 
            theSpecsPlace.appendChild(theSpecs3); 
          //  Line
            newLine.name = clusterNumb+"childCluster"+mimic.toString();
            newLine.id = clusterNumb+"childCluster"+mimic.toString();
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theId);
            newLine.appendChild(theHostnamePlace);
            newLine.appendChild(theStatusPlace);
            newLine.appendChild(theStatusDetailPlace);
            newLine.appendChild(theInfraDefPlace);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theHipervisorPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(theAmbienteEntregadoPlace);
            newLine.appendChild(thevCPUSolicitadoPlace);
            newLine.appendChild(thevCPUEntregadoPlace);
            newLine.appendChild(theRAMSolicitadoPlace);
            newLine.appendChild(theRAMEntregadoPlace);
            newLine.appendChild(theTDOYMPlace);
            newLine.appendChild(theStatusOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("machinesOnCluster"+clusterNumb).value=mimic;
    }
    function AddClusterMachine2 (clusterNumb,interId)
    {
      //  recuperar informacion de equipo
        workplace1="ClusterInfo2"+clusterNumb;
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("machinesOnCluster"+clusterNumb).value;
        mimic=parseInt(mimic-1);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiscoSolicitadoTitle = document.createElement("th");
          theDiscoSolicitadoTitle.style="width:13%";
          theDiscoSolicitadoTitle.innerHTML="Disco Duro<br/>Solicitado :";
          theDiscoEntregadoTitle = document.createElement("th");
          theDiscoEntregadoTitle.style="width:13%";
          theDiscoEntregadoTitle.innerHTML="Disco Duro<br/>Entregado :";
          theSharedSolicitadoTitle = document.createElement("th");
          theSharedSolicitadoTitle.style="width:12%";
          theSharedSolicitadoTitle.innerHTML="Disco Compartido<br/>Solicitado :";
          theSharedEntregadoTitle = document.createElement("th");
          theSharedEntregadoTitle.style="width:12%";
          theSharedEntregadoTitle.innerHTML="Disco Compartido<br/>SEntregado :";
          theNetCardTitle = document.createElement("th");
          theNetCardTitle.style="width:4%";
          theNetCardTitle.innerHTML = "Tarjetas<br />de Red:";
          theInternalConfTitle = document.createElement("th");
          theInternalConfTitle.style="width:5%";
          theInternalConfTitle.innerHTML = "Configuracion<br />Interna:";
          theRemedyTitle = document.createElement("th");
          theRemedyTitle.style="width:5%";
          theRemedyTitle.innerHTML = "Folio<br/>Remedy :";
          theSoliMOPTitle = document.createElement("th");
          theSoliMOPTitle.style="width:5%";
          theSoliMOPTitle.innerHTML = "F. Solicitud<br />de MOP :";
          theEntregaServerTitle = document.createElement("th");
          theEntregaServerTitle.style="width:5%";
          theEntregaServerTitle.innerHTML = "F. Entrega<br />Server :";
          theEntregaUserTitle = document.createElement("th");
          theEntregaUserTitle.style="width:5%";
          theEntregaUserTitle.innerHTML = "F. Entrega<br />a User :";
          theInicioATPTitle = document.createElement("th");
          theInicioATPTitle.style="width:5%";
          theInicioATPTitle.innerHTML = "F. Inicio<br />PreATP :";
          theFInATPTitle = document.createElement("th");
          theFInATPTitle.style="width:5%";
          theFInATPTitle.innerHTML = "F. Fin<br />PreATP :";
          theEntregaOYMTitle = document.createElement("th");
          theEntregaOYMTitle.style="width:5%";
          theEntregaOYMTitle.innerHTML = "F. Entrega<br />OYM :";
          theHistoricoTitle = document.createElement("th");
          theHistoricoTitle.style="width:6%";
          theHistoricoTitle.innerHTML = "Historico :";
          newTLine.name = "Cluster"+clusterNumb+"Title2";
          newTLine.id = "Cluster"+clusterNumb+"Title2";
          newTLine.appendChild(theDiscoSolicitadoTitle);
          newTLine.appendChild(theDiscoEntregadoTitle);
          newTLine.appendChild(theSharedSolicitadoTitle);
          newTLine.appendChild(theSharedEntregadoTitle);
          newTLine.appendChild(theNetCardTitle);
          newTLine.appendChild(theInternalConfTitle);
          newTLine.appendChild(theRemedyTitle);
          newTLine.appendChild(theSoliMOPTitle);
          newTLine.appendChild(theEntregaServerTitle);
          newTLine.appendChild(theEntregaUserTitle);
          newTLine.appendChild(theInicioATPTitle);
          newTLine.appendChild(theFInATPTitle);
          newTLine.appendChild(theEntregaOYMTitle);
          newTLine.appendChild(theHistoricoTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Disco Duro Solicitado
            theHDSPlace = document.createElement("td");
            theHDSPlace.style="width:13%";
            var someTable = document.createElement("table");
            someTable.style="width:100%";
            newLineIn = document.createElement("tr");
            newLineIn.style="width:100%";
            // Titulos
              theDiskName = document.createElement("th");
              theDiskName.style="width:35%";
              theDiskName.innerHTML = "Nombre :";
              theDiskSize = document.createElement("th");
              theDiskSize.style="width:25%";
              theDiskSize.innerHTML="Tamano :";
              theDiskDescription = document.createElement("th");
              theDiskDescription.style="width:40%";
              theDiskDescription.innerHTML="Descripcion :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineIn.appendChild(theDiskName);
              newLineIn.appendChild(theDiskSize);
              newLineIn.appendChild(theDiskDescription);
              //newLineIn.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholder = document.createElement("tr");
              newLinePlaceholder.style="width:100%";
              newLinePlaceholder.id=clusterNumb+"ClusterSPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="Button"+mimic;
              theButton.id="Button"+mimic;
              theButton.value="Disk++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTable.appendChild(newLineIn);  
            someTable.appendChild(newLinePlaceholder); 
            //someTable.appendChild(theButtonPlace);  
            theHDSPlace.appendChild(someTable);
            DiskCount = document.createElement("input");
            DiskCount.name=clusterNumb+"DiskStaticClusterS"+mimic;
            DiskCount.id=clusterNumb+"DiskStaticClusterS"+mimic;
            DiskCount.value=0;
            DiskCount.type="hidden";
          //  Disco Duro Entregado
            theHDEPlace = document.createElement("td");
            theHDEPlace.style="width:13%";
            var someTableE = document.createElement("table");
            someTableE.style="width:100%";
            newLineOut = document.createElement("tr");
            newLineOut.style="width:100%";
            // Titulos
              theDiskName2 = document.createElement("th");
              theDiskName2.style="width:35%";
              theDiskName2.innerHTML = "Nombre :";
              theDiskSize2 = document.createElement("th");
              theDiskSize2.style="width:25%";
              theDiskSize2.innerHTML="Tamano :";
              theDiskDescription2 = document.createElement("th");
              theDiskDescription2.style="width:40%";
              theDiskDescription2.innerHTML="Notas :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineOut.appendChild(theDiskName2);
              newLineOut.appendChild(theDiskSize2);
              newLineOut.appendChild(theDiskDescription2);
              //newLineOut.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholderE = document.createElement("tr");
              newLinePlaceholderE.style="width:100%";
              newLinePlaceholderE.id=clusterNumb+"ClusterEPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="ButtonSE"+mimic;
              theButton.id="ButtonSE"+mimic;
              theButton.value="++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTableE.appendChild(newLineOut);  
            someTableE.appendChild(newLinePlaceholderE); 
            //someTableE.appendChild(theButtonPlace);  
            theHDEPlace.appendChild(someTableE);
            DiskCountE = document.createElement("input");
            DiskCountE.name=clusterNumb+"DiskStaticClusterE"+mimic;
            DiskCountE.id=clusterNumb+"DiskStaticClusterE"+mimic;
            DiskCountE.value=0;
            DiskCountE.type="hidden";
          //  Disco Compartido Solicitado
            theHDSPlaceo = document.createElement("td");
            theHDSPlaceo.style="width:12%";
            var someTableo = document.createElement("table");
            someTableo.style="width:100%";
            newLineIno = document.createElement("tr");
            newLineIno.style="width:100%";
            // Titulos
              theDiskNamo = document.createElement("th");
              theDiskNamo.style="width:35%";
              theDiskNamo.innerHTML = "Nombre :";
              theDiskSizo = document.createElement("th");
              theDiskSizo.style="width:25%";
              theDiskSizo.innerHTML="Tamano :";
              theDiskDescriptio = document.createElement("th");
              theDiskDescriptio.style="width:40%";
              theDiskDescriptio.innerHTML="Descripcion :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineIno.appendChild(theDiskNamo);
              newLineIno.appendChild(theDiskSizo);
              newLineIno.appendChild(theDiskDescriptio);
              //newLineIn.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholdero = document.createElement("tr");
              newLinePlaceholdero.style="width:100%";
              newLinePlaceholdero.id=clusterNumb+"ClusterSharedSPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="Button"+mimic;
              theButton.id="Button"+mimic;
              theButton.value="Disk++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTableo.appendChild(newLineIno);  
            someTableo.appendChild(newLinePlaceholdero); 
            //someTable.appendChild(theButtonPlace);  
            theHDSPlaceo.appendChild(someTableo);
            DiskCounto = document.createElement("input");
            DiskCounto.name=clusterNumb+"DiskSharedSCluster"+mimic;
            DiskCounto.id=clusterNumb+"DiskSharedSCluster"+mimic;
            DiskCounto.value=0;
            DiskCounto.type="hidden";
          //  Disco Compartido Entregado
            theHDEPlaceo = document.createElement("td");
            theHDEPlaceo.style="width:12%";
            var someTableEo = document.createElement("table");
            someTableEo.style="width:100%";
            newLineOuto = document.createElement("tr");
            newLineOuto.style="width:100%";
            // Titulos
              theDiskName2o = document.createElement("th");
              theDiskName2o.style="width:35%";
              theDiskName2o.innerHTML = "Nombre :";
              theDiskSize2o = document.createElement("th");
              theDiskSize2o.style="width:25%";
              theDiskSize2o.innerHTML="Tamano :";
              theDiskDescription2o = document.createElement("th");
              theDiskDescription2o.style="width:40%";
              theDiskDescription2o.innerHTML="Notas :";
              //theKillButton = document.createElement("th");
              //theKillButton.style="width:15%";
              newLineOuto.appendChild(theDiskName2o);
              newLineOuto.appendChild(theDiskSize2o);
              newLineOuto.appendChild(theDiskDescription2o);
              //newLineOut.appendChild(theKillButton);
            // Placeholder
              newLinePlaceholderEo = document.createElement("tr");
              newLinePlaceholderEo.style="width:100%";
              newLinePlaceholderEo.id=clusterNumb+"ClusterSharedEPlaceholder"+mimic;
            /* buton ++
              theButtonPlace = document.createElement("tr");
              theButtonPlace.style="width:100%";
              theButtonPlace.class="agregar";
              theButtonPlace.Colspan="4";
              theButton = document.createElement("input");
              theButton.type="button";
              theButton.style="width:100%";
              theButton.name="ButtonSE"+mimic;
              theButton.id="ButtonSE"+mimic;
              theButton.value="++";
              theButton.onclick = function(){AddStaticDisk('1',numb)}; 
              theButtonPlace.appendChild(theButton); */
            someTableEo.appendChild(newLineOuto);  
            someTableEo.appendChild(newLinePlaceholderEo); 
            //someTableE.appendChild(theButtonPlace);  
            theHDEPlaceo.appendChild(someTableEo);
            DiskCountEo = document.createElement("input");
            DiskCountEo.name=clusterNumb+"DiskSharedECluster"+mimic;
            DiskCountEoid=clusterNumb+"DiskSharedECluster"+mimic;
            DiskCountEo.value=0;
            DiskCountEo.type="hidden";
          //  netCard
            theNetCardPlace = document.createElement("td");
            theNetCardPlace.style="width:6%";
            theNetCard = document.createElement("input");
            theNetCard.style="width:100%;";
            theNetCard.name="C"+clusterNumb+"netcard"+mimic;
            theNetCard.id="C"+clusterNumb+"netcard"+mimic;
            theNetCard.type="text";
            theNetCard.onkeypress = function(){return rev(event)}; 
            theNetCard.setAttribute("autocomplete", "off");
            theNetCardPlace.appendChild(theNetCard);
          //  internal Conf
            theInternalConfPlace = document.createElement("td");
            theInternalConfPlace.style="width:6%";
            theInternalConf = document.createElement("input");
            theInternalConf.style="width:100%;";
            theInternalConf.name="C"+clusterNumb+"internalConf"+mimic;
            theInternalConf.id="C"+clusterNumb+"internalConf"+mimic;
            theInternalConf.type="text";
            theInternalConf.onkeypress = function(){return rev(event)}; 
            theInternalConf.setAttribute("autocomplete", "off");
            theInternalConfPlace.appendChild(theInternalConf);
          //  Remedy
            theRemedyPlace = document.createElement("td");
            theRemedyPlace.style="width:6%";
            theRemedy = document.createElement("input");
            theRemedy.style="width:100%";
            theRemedy.name="C"+clusterNumb+"remedy"+mimic;
            theRemedy.id="C"+clusterNumb+"remedy"+mimic;
            theRemedy.type="text";
            theRemedy.onkeypress = function(){return rev(event)}; 
            theRemedy.setAttribute("autocomplete", "off");
            theRemedyPlace.appendChild(theRemedy);
          //  SoliMOP
            theSoliMOPPlace = document.createElement("td");
            theSoliMOPPlace.style="width:6%";
            theSoliMOP = document.createElement("input");
            theSoliMOP.style="width:100%";
            theSoliMOP.name="C"+clusterNumb+"soliMOP"+mimic;
            theSoliMOP.id="C"+clusterNumb+"soliMOP"+mimic;
            theSoliMOP.type="date";
            theSoliMOPPlace.appendChild(theSoliMOP);
          //  Entrega Server
            theEntregaServerPlace = document.createElement("td");
            theEntregaServerPlace.style="width:6%";
            theEntregaServer = document.createElement("input");
            theEntregaServer.style="width:100%";
            theEntregaServer.name="C"+clusterNumb+"entregaServer"+mimic;
            theEntregaServer.id="C"+clusterNumb+"entregaServer"+mimic;
            theEntregaServer.type="date";
            theEntregaServerPlace.appendChild(theEntregaServer);
          //  Entrega User
            theEntregaUserPlace = document.createElement("td");
            theEntregaUserPlace.style="width:6%";
            theEntregaUser = document.createElement("input");
            theEntregaUser.style="width:100%";
            theEntregaUser.name="C"+clusterNumb+"entregaUser"+mimic;
            theEntregaUser.id="C"+clusterNumb+"entregaUser"+mimic;
            theEntregaUser.type="date";
            theEntregaUserPlace.appendChild(theEntregaUser);
          //  Inicio ATP
            theInicioATPPlace = document.createElement("td");
            theInicioATPPlace.style="width:6%";
            theInicioATP = document.createElement("input");
            theInicioATP.style="width:100%";
            theInicioATP.name="C"+clusterNumb+"inicioATP"+mimic;
            theInicioATP.id="C"+clusterNumb+"inicioATP"+mimic;
            theInicioATP.type="date";
            theInicioATPPlace.appendChild(theInicioATP);
          //  Fin ATP
            theFInATPPlace = document.createElement("td");
            theFInATPPlace.style="width:6%";
            theFInATP = document.createElement("input");
            theFInATP.style="width:100%";
            theFInATP.name="C"+clusterNumb+"finATP"+mimic;
            theFInATP.id="C"+clusterNumb+"finATP"+mimic;
            theFInATP.type="date";
            theFInATPPlace.appendChild(theFInATP);
          //  entregaOYM
            theEntregaOYMPlace = document.createElement("td");
            theEntregaOYMPlace.style="width:6%";
            theEntregaOYM = document.createElement("input");
            theEntregaOYM.style="width:100%";
            theEntregaOYM.name="C"+clusterNumb+"entregaOYM"+mimic;
            theEntregaOYM.id="C"+clusterNumb+"entregaOYM"+mimic;
            theEntregaOYM.type="date";
            theEntregaOYMPlace.appendChild(theEntregaOYM);
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:8%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="Calendario";
            theSpecs1.onclick = function(){window.open('timemachine.php?&folio='+interId,'','menubar=0,titlebar=0,width=350,height=400,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
          //  Line
            newLine.name = clusterNumb+"childCluster2"+mimic.toString();
            newLine.id = clusterNumb+"childCluster2"+mimic.toString();
            newLine.appendChild(theHDSPlace);
            newLine.appendChild(DiskCount);
            newLine.appendChild(theHDEPlace);
            newLine.appendChild(DiskCountE);
            newLine.appendChild(theHDSPlaceo);
            newLine.appendChild(DiskCounto);
            newLine.appendChild(theHDEPlaceo);
            newLine.appendChild(DiskCountEo);
            newLine.appendChild(theNetCardPlace);
            newLine.appendChild(theInternalConfPlace);
            newLine.appendChild(theRemedyPlace);
            newLine.appendChild(theSoliMOPPlace);
            newLine.appendChild(theEntregaServerPlace);
            newLine.appendChild(theEntregaUserPlace);
            newLine.appendChild(theInicioATPPlace);
            newLine.appendChild(theFInATPPlace);
            newLine.appendChild(theEntregaOYMPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
    }
    function AddStaticDisk(type,diskInfo,cluster)
    {
      //  recuperar en que disco se trabajara
      if(type==1)
      { 
        workInThisDisk="StandaloneSPlaceholder"+diskInfo;
        workInThisDisk2="StandaloneEPlaceholder"+diskInfo;
        workInThisDisk=document.getElementById(workInThisDisk);
        workInThisDisk2=document.getElementById(workInThisDisk2);
        mimic="DiskStaticStandaloneS"+diskInfo;
        mimic=document.getElementById(mimic).value;
        mimic=parseInt(mimic);
      }
      if(type==2)
      { 
        workInThisDisk="AplianceSPlaceholder"+diskInfo;
        workInThisDisk2="AplianceEPlaceholder"+diskInfo;
        workInThisDisk=document.getElementById(workInThisDisk);
        workInThisDisk2=document.getElementById(workInThisDisk2);
        mimic="DiskStaticAplianceS"+diskInfo;
        mimic=document.getElementById(mimic).value;
        mimic=parseInt(mimic);
      }
      if(type==3)
      { 
        workInThisDisk=cluster+"ClusterSPlaceholder"+diskInfo;
        workInThisDisk2=cluster+"ClusterEPlaceholder"+diskInfo;
        workInThisDisk=document.getElementById(workInThisDisk);
        workInThisDisk2=document.getElementById(workInThisDisk2);
        mimic=cluster+"DiskStaticClusterS"+diskInfo;
        mimic=document.getElementById(mimic).value;
        mimic=parseInt(mimic);
      }
      //  create nueva tabla de discos
        newLine = document.createElement("tr");
        newLine2 = document.createElement("tr");
        if(type==1)
        {
            newLine.id = diskInfo+"DiskStaticStandaloneS"+mimic;
            newLine2.id = diskInfo+"DiskStaticStandaloneE"+mimic;
        }
        if(type==2)
        {
            newLine.id = diskInfo+"DiskStaticAplianceS"+mimic;
            newLine2.id = diskInfo+"DiskStaticAplianceE"+mimic;
        }
        if(type==3)
        {
          newLine.id = cluster+"machine"+diskInfo+"DiskStaticClusterS"+mimic;
          newLine2.id = cluster+"machine"+diskInfo+"DiskStaticClusterE"+mimic;
        }
        newLine.style="width:100%";
        newLine2.style="width:100%";
      //  agregar nombre de disco
        theNamePlace = document.createElement("td");
        theNamePlace.style="width:35%";
        theName = document.createElement("input");
        theName.style="width:100%";
        theNamePlace2 = document.createElement("td");
        theNamePlace2.style="width:35%";
        theName2 = document.createElement("input");
        theName2.style="width:100%";
        if(type==1)
        {
          theName.name="s1name"+diskInfo+"s"+mimic;
          theName.id="s1name"+diskInfo+"s"+mimic;
          theName2.name="s1name"+diskInfo+"e"+mimic;
          theName2.id="s1name"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theName.name="a1name"+diskInfo+"s"+mimic;
          theName.id="a1name"+diskInfo+"s"+mimic;
          theName2.name="a1name"+diskInfo+"e"+mimic;
          theName2.id="a1name"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theName.name="c"+cluster+"name"+diskInfo+"s"+mimic;
          theName.id="c"+cluster+"name"+diskInfo+"s"+mimic;
          theName2.name="c"+cluster+"name"+diskInfo+"e"+mimic;
          theName2.id="c"+cluster+"name"+diskInfo+"e"+mimic;
        }
        theName.type="text";
        theName.onkeypress = function(){return rev(event)}; 
        theName.setAttribute("autocomplete", "off");
        theNamePlace.appendChild(theName);
        theName2.type="text";
        theName2.onkeypress = function(){return rev(event)}; 
        theName2.setAttribute("autocomplete", "off");
        theNamePlace2.appendChild(theName2);
      //  añadir tamaño de disco definiendo primeros 2 con minimo
        theSizePlace = document.createElement("td");
        theSizePlace.style="width:25%";
        theSize = document.createElement("input");
        theSizePlace2 = document.createElement("td");
        theSizePlace2.style="width:25%";
        theSize2 = document.createElement("input");
        if(type==1)
        {
          theSize.name="s1tam"+diskInfo+"s"+mimic;
          theSize.id="s1tam"+diskInfo+"s"+mimic;
          theSize2.name="s1tam"+diskInfo+"e"+mimic;
          theSize2.id="s1tam"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theSize.name="a1tam"+diskInfo+"s"+mimic;
          theSize.id="a1tam"+diskInfo+"s"+mimic;
          theSize2.name="a1tam"+diskInfo+"e"+mimic;
          theSize2.id="a1tam"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theSize.name="c"+cluster+"tam"+diskInfo+"s"+mimic;
          theSize.id="c"+cluster+"tam"+diskInfo+"s"+mimic;
          theSize2.name="c"+cluster+"tam"+diskInfo+"e"+mimic;
          theSize2.id="c"+cluster+"tam"+diskInfo+"e"+mimic;
        }
        theSize.type="number";
        theSize.style="width:100%";
        theSize.min=1;
        theSize.step=1;
        theSizePlace.appendChild(theSize);
        theSize2.type="number";
        theSize2.style="width:100%";
        theSize2.min=1;
        theSize2.step=1;
        theSizePlace2.appendChild(theSize2);
      //  añadir descripcion de disco
        theDescPlace = document.createElement("td");
        theDescPlace.style="width:40%";
        theDesc = document.createElement("input");
        theDesc.style="width:100%";
        theDescPlace2 = document.createElement("td");
        theDescPlace2.style="width:40%";
        theDesc2 = document.createElement("input");
        theDesc2.style="width:100%";
        if(type==1)
        {
          theDesc.name="s1des"+diskInfo+"s"+mimic;
          theDesc.id="s1des"+diskInfo+"s"+mimic;
          theDesc2.name="s1des"+diskInfo+"e"+mimic;
          theDesc2.id="s1des"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theDesc.name="a1des"+diskInfo+"s"+mimic;
          theDesc.id="a1des"+diskInfo+"s"+mimic;
          theDesc2.name="a1des"+diskInfo+"e"+mimic;
          theDesc2.id="a1des"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theDesc.name="c"+cluster+"des"+diskInfo+"s"+mimic;
          theDesc.id="c"+cluster+"des"+diskInfo+"s"+mimic;
          theDesc2.name="c"+cluster+"des"+diskInfo+"e"+mimic;
          theDesc2.id="c"+cluster+"des"+diskInfo+"e"+mimic;
        }
        theDesc.type="text";
        theDesc.onkeypress = function(){return rev(event)}; 
        theDesc.setAttribute("autocomplete", "off");
        theDescPlace.appendChild(theDesc);
        theDesc2.type="text";
        theDesc2.onkeypress = function(){return rev(event)}; 
        theDesc2.setAttribute("autocomplete", "off");
        theDescPlace2.appendChild(theDesc2);
      /*  añadir boton para eliminar
        theButtonPlace = document.createElement("td");
        theButtonPlace.style="width:20%";
        theButtonPlace.class="eliminar";
        theButton = document.createElement("input");
        theButton.type="button";
        theButton.value="eliminar";
        if(type==1)
          var numb=diskInfo+"DiskStaticStandalone"+mimic;
        if(type==2)
          var numb=diskInfo+"DiskStaticApliance"+mimic;
        if(type==3)
          var numb=cluster+"machine"+diskInfo+"DiskStaticCluster"+mimic;
        theButton.onclick = function(){killThisDisk(numb)}; 
        theButtonPlace.appendChild(theButton);*/
      //  agregar datos predefinidos a nueva tabla 
        newLine.appendChild(theNamePlace);
        newLine.appendChild(theSizePlace);
        newLine.appendChild(theDescPlace);
        newLine2.appendChild(theNamePlace2);
        newLine2.appendChild(theSizePlace2);
        newLine2.appendChild(theDescPlace2);
        //newLine.appendChild(theButtonPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.parentNode.insertBefore(newLine,workInThisDisk);
        workInThisDisk2.parentNode.insertBefore(newLine2,workInThisDisk2);
        mimic=parseInt(mimic)+1;
        if(type==1)
        {
          document.getElementById("DiskStaticStandaloneS"+diskInfo).value=mimic;
          document.getElementById("DiskStaticStandaloneE"+diskInfo).value=mimic;
        }
        if(type==2)
        {
          document.getElementById("DiskStaticAplianceS"+diskInfo).value=mimic;
          document.getElementById("DiskStaticAplianceE"+diskInfo).value=mimic;
        }
        if(type==3)
        {
          document.getElementById(cluster+"DiskStaticClusterS"+diskInfo).value=mimic;
          document.getElementById(cluster+"DiskStaticClusterE"+diskInfo).value=mimic;
        }
    }
    function AddSharedDisk(machine,cluster)
    {
      //  recuperar en que disco se trabajara
        workInThisDisk=cluster+"ClusterSharedSPlaceholder"+machine;
        workInThisDisk2=cluster+"ClusterSharedEPlaceholder"+machine;
        workInThisDisk=document.getElementById(workInThisDisk);
        workInThisDisk2=document.getElementById(workInThisDisk2);
        mimic=cluster+"DiskSharedSCluster"+machine;
        mimic=document.getElementById(mimic).value;
        mimic=parseInt(mimic);
      //  create nueva tabla de discos
        newLine = document.createElement("tr");
        newLine.id = cluster+"machine"+machine+"DiskSharedSCluster"+mimic;
        newLine.style="width:100%";
        newLine2 = document.createElement("tr");
        newLine2.id = cluster+"machine"+machine+"DiskSharedECluster"+mimic;
        newLine2.style="width:100%";
      //  agregar nombre de disco
        theNamePlace = document.createElement("td");
        theNamePlace.style="width:35%";
        theName = document.createElement("input");
        theName.style="width:100%";
        theName.name=cluster+"namo"+machine+"s"+mimic;
        theName.id=cluster+"namo"+machine+"s"+mimic;
        theName.type="text";
        theName.onkeypress = function(){return rev(event)}; 
        theName.setAttribute("autocomplete", "off");
        theNamePlace.appendChild(theName);
        theNamePlace2 = document.createElement("td");
        theNamePlace2.style="width:35%";
        theName2 = document.createElement("input");
        theName2.style="width:100%";
        theName2.name=cluster+"namo"+machine+"e"+mimic;
        theName2.id=cluster+"namo"+machine+"e"+mimic;
        theName2.type="text";
        theName2.onkeypress = function(){return rev(event)}; 
        theName2.setAttribute("autocomplete", "off");
        theNamePlace2.appendChild(theName2);
      //  añadir tamaño de disco definiendo primeros 2 con minimo
        theSizePlace = document.createElement("td");
        theSizePlace.style="width:25%";
        theSize = document.createElement("input");
        theSize.name=cluster+"tamo"+machine+"s"+mimic;
        theSize.id=cluster+"tamo"+machine+"s"+mimic;
        theSize.type="number";
        theSize.style="width:100%";
        theSize.min=1;
        theSize.step=1;
        theSizePlace.appendChild(theSize);
        theSizePlace2 = document.createElement("td");
        theSizePlace2.style="width:25%";
        theSize2 = document.createElement("input");
        theSize2.name=cluster+"tamo"+machine+"e"+mimic;
        theSize2.id=cluster+"tamo"+machine+"e"+mimic;
        theSize2.type="number";
        theSize2.style="width:100%";
        theSize2.min=1;
        theSize2.step=1;
        theSizePlace2.appendChild(theSize2);
      //  añadir descripcion de disco
        theDescPlace = document.createElement("td");
        theDescPlace.style="width:40%";
        theDesc = document.createElement("input");
        theDesc.style="width:100%";
        theDesc.name=cluster+"deso"+machine+"s"+mimic;
        theDesc.id=cluster+"deso"+machine+"s"+mimic;
        theDesc.type="text";
        theDesc.onkeypress = function(){return rev(event)}; 
        theDesc.setAttribute("autocomplete", "off");
        theDescPlace.appendChild(theDesc);
        theDescPlace2 = document.createElement("td");
        theDescPlace2.style="width:40%";
        theDesc2 = document.createElement("input");
        theDesc2.style="width:100%";
        theDesc2.name=cluster+"deso"+machine+"e"+mimic;
        theDesc2.id=cluster+"deso"+machine+"e"+mimic;
        theDesc2.type="text";
        theDesc2.onkeypress = function(){return rev(event)}; 
        theDesc2.setAttribute("autocomplete", "off");
        theDescPlace2.appendChild(theDesc2);
      //  agregar datos predefinidos a nueva tabla 
        newLine.appendChild(theNamePlace);
        newLine.appendChild(theSizePlace);
        newLine.appendChild(theDescPlace);
        newLine2.appendChild(theNamePlace2);
        newLine2.appendChild(theSizePlace2);
        newLine2.appendChild(theDescPlace2);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.parentNode.insertBefore(newLine,workInThisDisk);
        workInThisDisk2.parentNode.insertBefore(newLine2,workInThisDisk2);
        mimic=parseInt(mimic)+1;
        document.getElementById(cluster+"DiskSharedSCluster"+machine).value=mimic;
        document.getElementById(cluster+"DiskSharedECluster"+machine).value=mimic;
    }
    function AddIP(type,diskInfo,cluster)
    {
      //  recuperar en que disco se trabajara
        if(type==1)
        { 
          workInThisDisk="StandaloneIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPStandalone"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
        }
        if(type==2)
        { 
          workInThisDisk="AplianceIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPApliance"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
        }
        if(type==3)
        { 
          workInThisDisk=cluster+"ClusterIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic=cluster+"IPCluster"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
        }
      //  create nueva tabla de discos
        newLine = document.createElement("tr");
        if(type==1)
          newLine.id = diskInfo+"IPStandalone"+mimic;
        if(type==2)
          newLine.id = diskInfo+"IPApliance"+mimic;
        if(type==3)
          newLine.id = cluster+"machine"+diskInfo+"IPCluster"+mimic;
        newLine.style="width:100%";
      //  agregar IP
        theIPPlace2 = document.createElement("td");
        theIPPlace2.style="width:30%";
        theIP2 = document.createElement("input");
        theIP2.style="width:100%";
        if(type==1)
        {
          theIP2.name="s1IP"+diskInfo+"e"+mimic;
          theIP2.id="s1IP"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theIP2.name="a1IP"+diskInfo+"e"+mimic;
          theIP2.id="a1IP"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theIP2.name="c"+cluster+"IP"+diskInfo+"e"+mimic;
          theIP2.id="c"+cluster+"IP"+diskInfo+"e"+mimic;
        }
        theIP2.type="text";
        theIP2.onkeypress = function(){return isTheip(event)}; 
        theIP2.setAttribute("autocomplete", "off");
        theIPPlace2.appendChild(theIP2);
      //  añadir descripcion de disco
        theDescIPPlace = document.createElement("td");
        theDescIPPlace.style="width:32%";
        theDescIP = document.createElement("input");
        theDescIP.style="width:100%";
        if(type==1)
        {
          theDescIP.name="s1dip"+diskInfo+"e"+mimic;
          theDescIP.id="s1dip"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theDescIP.name="a1dip"+diskInfo+"e"+mimic;
          theDescIP.id="a1dip"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theDescIP.name="c"+cluster+"dip"+diskInfo+"e"+mimic;
          theDescIP.id="c"+cluster+"dip"+diskInfo+"e"+mimic;
        }
        theDescIP.type="text";
        theDescIP.setAttribute("autocomplete", "off");
        theDescIPPlace.appendChild(theDescIP);
      //  añadir boton para eliminar
        theButtonPlace = document.createElement("td");
        theButtonPlace.style="width:20%";
        theButtonPlace.class="eliminar";
        theButton = document.createElement("input");
        theButton.type="button";
        theButton.value="X";
        if(type==1)
          var numb=diskInfo+"IPStandalone"+mimic;
        if(type==2)
          var numb=diskInfo+"IPApliance"+mimic;
        if(type==3)
          var numb=cluster+"machine"+diskInfo+"IPCluster"+mimic;
        theButton.onclick = function(){killThisIP(numb)}; 
        theButtonPlace.appendChild(theButton);
      //  agregar datos predefinidos a nueva tabla 
        newLine.appendChild(theIPPlace2);
        newLine.appendChild(theDescIPPlace);
        newLine.appendChild(theButtonPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.parentNode.insertBefore(newLine,workInThisDisk);
        mimic=parseInt(mimic)+1;
        if(type==1)
          document.getElementById("IPStandalone"+diskInfo).value=mimic;
        if(type==2)
          document.getElementById("IPApliance"+diskInfo).value=mimic;
        if(type==3)
          document.getElementById(cluster+"IPCluster"+diskInfo).value=mimic;
    }
    function killThisIP(some)
    {

    }
    function chargeThisMachine(datos,numero,Prefijo1)
    {
      //alert(numero);
      //alert(datos);
      bullets=datos.split(",");
      var bulletVal = ["id","hostname","status","statusDetail","infraDef","aplicacion","hipervisor","ambienteSolicitado","ambienteEntregado","CPUS","CPUE","RAMS","RAME","TDOYM","statusOYM"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(Prefijo1+bulletVal[hh]+numero).style="width:100%;background:#85807d;";
        else
          document.getElementById(Prefijo1+bulletVal[hh]+numero).value=bullets[hh];
      }
    }
    function chargeThisMachine2(datos,numero,Prefijo1)
    {
      //alert(numero);
      //alert(datos);
      bullets=datos.split(",");
      var bulletVal = ["netcard","internalConf","remedy","soliMOP","entregaServer","entregaUser","inicioATP","finATP","entregaOYM"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(Prefijo1+bulletVal[hh]+numero).style="width:100%;background:#85807d;";
        else
          document.getElementById(Prefijo1+bulletVal[hh]+numero).value=bullets[hh];
      }
    }
    function chargeStaticDisk(datos,maquinan,disco,prefijo)
    {
      //alert(numero);
      //alert(maquinan+','+disco+','+prefijo);
      bullets=datos.split(",");
      var bulletVal = ["name","name","tam","tam","des","des"];
      var typeOfDisk = ["s","e","s","e","s","e"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(prefijo+bulletVal[hh]+maquinan+typeOfDisk[hh]+disco).style="width:100%;background:#85807d;";
        else
          document.getElementById(prefijo+bulletVal[hh]+maquinan+typeOfDisk[hh]+disco).value=bullets[hh];
      }
    }
    function chargeIP(datos,maquinan,disco,prefijo)
    {
      bullets=datos.split(",");
      document.getElementById(prefijo+"IP"+maquinan+"e"+disco).value=bullets[0];
      document.getElementById(prefijo+"dip"+maquinan+"e"+disco).value=bullets[1];
    }
    function chargeSharedDisk(datos,maquinan,disco,prefijo)
    {
      //alert(numero);
      bullets=datos.split(",");
      var bulletVal = ["namo","namo","tamo","tamo","deso","deso"];
      var typeOfDisk = ["s","e","s","e","s","e"];
      for(hh=0;hh<bulletVal.length;hh++)
      {
        if(bullets[hh]=="")
          document.getElementById(prefijo+bulletVal[hh]+maquinan+typeOfDisk[hh]+disco).style="width:100%;background:#85807d;";
        else
          document.getElementById(prefijo+bulletVal[hh]+maquinan+typeOfDisk[hh]+disco).value=bullets[hh];
      }
    }
  </script>
  <body>
    <?php
      $sql="select nombre from ambiente";
      $toFillTemplate = mysqli_query($conn,$sql);
      $k=0;
      if(! $toFillTemplate)
        echo "<option value=\"\">Error de conexion</option>";
      else
        while($templateValues = mysqli_fetch_array($toFillTemplate))
        {
          if($k==0)
            $Ambiente=$templateValues['nombre'];
          else
            $Ambiente=$Ambiente.",".$templateValues['nombre'];
          $k=1;
        }
      $sql="select nombre from infdef";
      $toFillTemplate = mysqli_query($conn,$sql);
      $k=0;
      if(! $toFillTemplate)
        echo "<option value=\"\">Error de conexion</option>";
      else
        while($templateValues = mysqli_fetch_array($toFillTemplate))
        {
          if($k==0)
            $Infra=$templateValues['nombre'];
          else
            $Infra=$Infra.",".$templateValues['nombre'];
          $k=1;
        }
    ?>
    <form method="post"  action="moveMachine.php" >
      <br><br>
      <!-- Botones Principales -->
        <div class="shad" align="center">
          <?php for($i=0;$i<8;$i++) echo "&nbsp;"; ?><input type="submit" value="Modificar datos de maquinas"> &nbsp;
        <!--
          <button id="addStandalone" name="addStandalone" type="button" class="btn btn-warning" onclick="giveSomeStandalone('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Standalone + </button>
          <button id="addApliance" name="addApliance" type="button" class="btn btn-warning" onclick="giveSomeApliance('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Apliance + </button>
          <button id="addCluster" name="addCluster" type="button" class="btn btn-warning" onclick="giveSomeCluster('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Cluster + </button>
        -->
        </div>
        <br><br>
      <!-- Valores rescatados temp -->
        <input type="hidden" name="folio" value="<?php echo $_POST['folio'];?>" >
      <!-- Placeholders -->
        <input type="hidden" id="childOfStandalone" name="childOfStandalone" value="0">
        <input type="hidden" id="childOfApliance" name="childOfApliance" value="0">
        <input type="hidden" id="childOfCluster" name="childOfCluster" value="0">
        <h2>Standalone:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="standalonePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="standalonePlaceholder2" ></tr>
        </table>
        <h2>Apliance:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="apliancePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="apliancePlaceholder2" ></tr>
        </table>
        <h2>Cluster:</h2>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="clusterPlaceholder" ></tr>
        </table>
      <!--  cargar maquinas -->
        <?php
          $sql="select interId,arreglo,tipo,aplicacion,ambienteSolicitado,ambienteEntregado,
          CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,nombre,infraestructuraDef,
          entregaUser,estatus,detalleEstatus,estatusOYM,remedy,fechaSoliMOP,fechaEntregaServer,
          TDOYM,hipervisor,netcard,intConfig,inicioPreATP,finPreATP,entregaOYM,F60 from maquinas where folio='".$_POST['folio']."'";
          $someThings = mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r>0)
          {
            $lastArreglo=-1;
            while($maquinaPr = mysqli_fetch_array($someThings))
            {
              $dato=substr($maquinaPr['interId'],strlen($_POST['folio']));   
              if($maquinaPr['tipo']==2)
              {
                if($maquinaPr['arreglo']!=$lastArreglo)
                {
                  echo '<script type="text/javascript">giveSomeCluster("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                  $lastArreglo=$maquinaPr['arreglo'];
                }
              }
              switch($dato[0])
              {
                case "s":
                  echo '<script type="text/javascript">giveSomeStandalone("'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">giveSomeStandalone2("'.$maquinaPr['interId'].'");</script>';
                  $pos = strpos($dato, 'm');
                  $dato=substr($dato,($pos+1));
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","S1");</script>';
                  $infoMachine=$maquinaPr['netcard'].",".$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","S1");</script>';
                  $sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    echo '<script type="text/javascript">AddStaticDisk("1","'.$dato.'");</script>';
                    $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                    echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                    $k++;
                  }
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("1","'.$dato.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                    $k++;
                  }
                break;
                case "a":
                  echo '<script type="text/javascript">giveSomeApliance("'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">giveSomeApliance2("'.$maquinaPr['interId'].'");</script>';
                  $pos = strpos($dato, 'm');
                  $dato=substr($dato,($pos+1));
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","A1");</script>';
                  $infoMachine=$maquinaPr['netcard'].",".$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","A1");</script>';
                  $sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    echo '<script type="text/javascript">AddStaticDisk("2","'.$dato.'");</script>';
                    $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                    echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                    $k++;
                  }
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("2","'.$dato.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                    $k++;
                  }
                break;
                case "c":
                  $pos = strpos($dato, 'm');
                  $clusterNumber=substr($dato,0,-(strlen($dato)-$pos));
                  $clusterNumber=substr($clusterNumber,1);
                  $dato=substr($dato,($pos+1));
                  echo '<script type="text/javascript">AddClusterMachine("'.$clusterNumber.'","'.$Ambiente.'","'.$_POST['folio'].'","'.$Infra.'","'.$maquinaPr['interId'].'");</script>';
                  echo '<script type="text/javascript">AddClusterMachine2("'.$clusterNumber.'","'.$maquinaPr['interId'].'");</script>';
                  $infoMachine=$maquinaPr['interId'].",".$maquinaPr['nombre'].",".$maquinaPr['estatus'].",".$maquinaPr['detalleEstatus'].",".$maquinaPr['infraestructuraDef'].",".$maquinaPr['aplicacion'].",".$maquinaPr['hipervisor'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['ambienteEntregado'].",".$maquinaPr['CPUSolicitado'].",".$maquinaPr['CPUEntregado'].",".$maquinaPr['RAMSolicitado'].",".$maquinaPr['RAMEntregado'].",".$maquinaPr['TDOYM'].",".$maquinaPr['estatusOYM'];
                  echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","C'.$clusterNumber.'");</script>';
                  $infoMachine=$maquinaPr['netcard'].",".$maquinaPr['intConfig'].",".$maquinaPr['remedy'].",".$maquinaPr['fechaSoliMOP'].",".$maquinaPr['fechaEntregaServer'].",".$maquinaPr['entregaUser'].",".$maquinaPr['inicioPreATP'].",".$maquinaPr['finPreATP'].",".$maquinaPr['entregaOYM'];
                  echo '<script type="text/javascript">chargeThisMachine2("'.$infoMachine.'","'.$dato.'","C'.$clusterNumber.'");</script>';
                  $sql="select * from discos where interId='".$maquinaPr['interId']."'";
                  $diskThings = mysqli_query($conn,$sql);
                  $k=0;
                  $t=0;
                  while($actDisk = mysqli_fetch_array($diskThings))
                  {
                    if($actDisk['tipoDisco']=="Estatico")
                    {
                      echo '<script type="text/javascript">AddStaticDisk("3","'.$dato.'","'.$clusterNumber.'");</script>';
                      $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                      echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                      $k++;
                    }
                    if($actDisk['tipoDisco']=="Compartido")
                    {
                      echo '<script type="text/javascript">AddSharedDisk("'.$dato.'","'.$clusterNumber.'");</script>';
                      $infoDisk=$actDisk['nombreDiscoSolicitado'].",".$actDisk['nombreDiscoEntregado'].",".$actDisk['sizeDiscoSolicitado'].",".$actDisk['sizeDiscoEntregado'].",".$actDisk['descripcion'].",".$actDisk['notas'];
                      echo '<script type="text/javascript">chargeSharedDisk("'.$infoDisk.'","'.$dato.'","'.$t.'","'.$clusterNumber.'");</script>';
                      $t++;
                    }
                  }
                  $sql="select IP,descripcion from interIP where interId='".$maquinaPr['interId']."'";
                  $ipThings = mysqli_query($conn,$sql);
                  $k=0;
                  while($actip = mysqli_fetch_array($ipThings))
                  {
                    echo '<script type="text/javascript">AddIP("3","'.$dato.'","'.$clusterNumber.'");</script>';
                    $infoDisk=$actip['IP'].",".$actip['descripcion'];
                    echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                    $k++;
                  }
                break;
              }
              //echo '<script type="text/javascript">alert("'.$dato.'");</script>';
            }
          }
        ?>
    </form>
  </body>
</html>