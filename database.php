<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #SO
    {
      width:120px;
      float:right;
      margin-right:40px;
    }
    th,td
    {
      font-size: 16px;
    }
  </style>
  <script>
    function chargeUsuarios(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="usuariosPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos 
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir dbuName
          theDBUNamePlace = document.createElement("td");
          theDBUName = document.createElement("input");
          theDBUName.name="dbuName"+theData[0];
          theDBUName.value=theData[1];
          theDBUName.required=true;
          theDBUName.type="text";
          theDBUName.autocomplete="off";
          theDBUNamePlace.appendChild(theDBUName);
          theDBUName.size=20;
        //  añadir dbuRol
          theDBURolPlace = document.createElement("td");
          theDBURol = document.createElement("input");
          theDBURol.name="dbuRol"+theData[0];
          theDBURol.value=theData[2];
          theDBURol.type="text";
          theDBURol.autocomplete="off";
          theDBURol.required=true;
          theDBURolPlace.appendChild(theDBURol);
          theDBURol.size=34;
        //  añadir Privilegios
          theDBUPrivilegiosPlace = document.createElement("td");
          theDBUPrivilegios = document.createElement("input");
          theDBUPrivilegios.name="dbuPrivilegios"+theData[0];
          theDBUPrivilegios.value=theData[3];
          theDBUPrivilegios.type="text";
          theDBUPrivilegios.autocomplete="off";
          theDBUPrivilegiosPlace.appendChild(theDBUPrivilegios);
          theDBUPrivilegios.size=46;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfUsuarios"+theData[0];
          newTabla.id = "childOfUsuarios"+theData[0];
          newTabla.appendChild(theDBUNamePlace);
          newTabla.appendChild(theDBURolPlace);
          newTabla.appendChild(theDBUPrivilegiosPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfUsuarios").value=document.getElementById("childOfUsuarios").value++;
    }
    function chargeEspecificaciones(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="specPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos 
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir especificacion
          theSpecPlace = document.createElement("td");
          theSpec = document.createElement("input");
          theSpec.name="especificacion"+theData[0];
          theSpec.id="especificacion"+theData[0];
          theSpec.value=theData[1];
          theSpec.type="text";
          theSpec.autocomplete="off";
          theSpecPlace.appendChild(theSpec);
          theSpec.size=25;
        //  añadir valor
          theValuePlace = document.createElement("td");
          theValue = document.createElement("input");
          theValue.name="valor"+theData[0];
          theValue.value=theData[2];
          theValue.type="text";
          theValue.autocomplete="off";
          theValuePlace.appendChild(theValue);
          theValue.size=15;
        //  añadir valor Default
          theValDefPlace = document.createElement("td");
          theValDef = document.createElement("input");
          theValDef.name="valorDef"+theData[0];
          theValDef.id="valorDef"+theData[0];
          theValDef.value=theData[3];
          theValDef.type="text";
          theValDef.autocomplete="off";
          theValDefPlace.appendChild(theValDef);
          theValDef.size=60;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfSpec"+theData[0];
          newTabla.id = "childOfSpec"+theData[0];
          newTabla.appendChild(theSpecPlace);
          newTabla.appendChild(theValuePlace);
          newTabla.appendChild(theValDefPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfSpec").value=document.getElementById("childOfSpec").value++;
    }
  </script>
  <?php
    echo "<title>Database de maquina ".$_POST['machine']."</title>";
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
</head>
<body>
  <?php
    if($_POST['base']!="")
      $keyValue=$_POST['base'];
    else
    {
      $sql = "select * from filtroEspecificacionesDB where folioNumber='".$_POST['machine']."'";
      $r=mysqli_query($conn,$sql);
      if(mysqli_affected_rows($conn)==1)
      {
        $firstDBInfo = mysqli_fetch_array($r);
        $keyValue=$firstDBInfo['base'];
      }
    }
    echo "<script> var baseACT='".$keyValue."'; </script>";
  ?>
  <div class="container">
  <br><br><br>
    <form method='POST' action="dropTheBase.php" >
      Database: <select id="base" name="base" onchange="this.form.submit()" >
        <option value=""></option>
        <option <?php if($keyValue=='Oracle'){echo "selected";}?> value="Oracle">Oracle</option>
        <option <?php if($keyValue=='SQL Server'){echo "selected";}?> value="SQL Server">SQL Server</option>
        <option <?php if($keyValue=='MySQL'){echo "selected";}?> value="MySQL">MySQL</option>
        <option <?php if($keyValue=='DB2'){echo "selected";}?> value="DB2">DB2</option>
        <option <?php if($keyValue=='Informix'){echo "selected";}?> value="Informix">Informix</option>
      </select>
      <input type="hidden" name="machine2" id="machine2" value="<?php echo $_POST['machine'];?>" >
      <input type="hidden" name="folio2" id="folio2" value="<?php echo $_POST["folio"];?>" >
    </form>
    <form method='POST' action="processDatabase.php" > 
      <table width="100%">
        <!-- Renglon   1   -->
          <tr>
            <!-- Nombre -->
              <td width="25%"> 
                <input type="hidden" name="base2" id="base2" value="<?php echo $keyValue;?>" >
                Nombre: <input type="text" name="DBname" id="DBname" <?php if($keyValue==""){echo "disabled";}?> maxlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required onkeypress="return rev(event)" value="<?php echo $firstDBInfo['DBname']; ?>" autocomplete="off"> 
              </td>
            <!-- puerto -->
              <td width="25%">
                Puerto:<input type="text" id="puerto" <?php if($keyValue==""){echo "disabled";}?> onkeyup="portCheck(this.value)" name="puerto" style="max-width:50px;" pattern="[0-9]*" value="<?php echo $firstDBInfo['puerto']; ?>" autocomplete="off">
                <div id=hidePuerto style="display:none;">Puerto no valido!</div>
              </td>
            <!-- especificaciones -->
              <td width="25%">
                Especificaciones: <select id="especificaciones" name="especificaciones" <?php if($keyValue==""){echo "disabled";}?> onchange="giveSpc(this)">
                <?php
                  switch($keyValue)
                  {
                    case "Oracle":
                      for($o=2;$o<10;$o++)
                      {  
                        echo "<option value=\"".$o."\"";
                        if($firstDBInfo['especificaciones']==$o)
                          echo " selected ";
                        echo ">".$o."</option>";
                      }
                    break;
                    case "SQL Server":
                      for($o=2;$o<10;$o++)
                      {  
                        echo "<option value=\"".$o."\"";
                        if($firstDBInfo['especificaciones']==$o)
                          echo " selected ";
                        echo ">".$o."</option>";
                      }
                    break;
                    case "Informix":
                      for($o=1;$o<10;$o++)
                      {  
                        echo "<option value=\"".$o."\"";
                        if($firstDBInfo['especificaciones']==$o)
                          echo " selected ";
                        echo ">".$o."</option>";
                      }
                    break;
                    default:
                      echo "<option value=\"0\"></option>";
                      for($o=1;$o<10;$o++)
                      {  
                        echo "<option value=\"".$o."\"";
                        if($firstDBInfo['especificaciones']==$o)
                          echo " selected ";
                        echo ">".$o."</option>";
                      }
                    break;
                  }
                ?>
                </select>
              </td>
            <!-- usuarios -->
              <td width="25%">
                Usuarios: <select id="usuarios" name="usuarios" <?php if($keyValue==""){echo "disabled";}?> onchange="giveUsuarios(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {  
                      echo "<option value=\"".$o."\"";
                      if($firstDBInfo['usuarios']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
          </tr>
      </table>
      <input type="hidden" id="childOfUsuarios" name="childOfUsuarios" value="<?php if($firstDBInfo['usuarios']!=""){echo $firstDBInfo['usuarios'];}else{echo 0;}?>">
      <input type="hidden" id="childOfSpec" name="childOfSpec" value="<?php if($firstDBInfo['especificaciones']!=""){echo $firstDBInfo['especificaciones'];}else{echo 0;}?>">
      <input type="hidden" name="machine" id="machine" value="<?php echo $_POST['machine'];?>" >
      <input type="hidden" name="folio" id="folio" value="<?php echo $_POST["folio"];?>" >
      Respaldos:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="30%">Respaldos</th>
            <th width="15%">Si/no</th>
            <th width="55%">Comentarios</th>
          </tr>
          <tr>
            <td width="30%">Respaldo en linea</td>
            <td width="15%">
              Si <input type="radio" value="1" name="response1" id="response1" <?php if($keyValue==""){echo "disabled ";}?> <?php if($firstDBInfo['response1']==1){echo "checked";}?> required >
              / No <input type="radio" value="0" name="response1" id="response1" <?php if($keyValue==""){echo "disabled ";}?> <?php if($firstDBInfo['response1']==0){echo "checked";}?> required >
            </td>
            <td width="55%"><input type="text" size="100%" name="comentarios" id="comentarios" <?php if($keyValue==""){echo "disabled";}?> autocomplete="off" value="<?php echo $firstDBInfo['comentarios'];?>"></td>
          </tr>
      </table>
      Especificaciones:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="25%">Especificacion</th>
            <th width="15%">valor</th>
            <th width="60%">Valor default</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="specPlaceholder" ></tr>
      </table>
      <?php
        if($firstDBInfo['especificaciones']>0)
        {
          try
          {
            $sql = "select * from filtroDBEspecificaciones where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfSpecs = mysqli_fetch_array($r))
            {
              $rowSpecs=$a.",".$rowOfSpecs['especificacion'].",".$rowOfSpecs['valor'].",".$rowOfSpecs['valorDef'];
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowSpecs.'");</script>';
              if(($keyValue=="Oracle"&&$a<2)||($keyValue=="SQL Server"&&$a<2)||($keyValue=="Informix"&&$a<1))
                echo '<script type="text/javascript">document.getElementById("especificacion'.$a.'").readOnly = true;document.getElementById("valorDef'.$a.'").readOnly = true;</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");</script>';
          }
        }
        else
        {
          switch($keyValue)
          {
            case "Oracle":
              $rowforTheDB="0,NLS_CHARACTERSET,,AL16UTF16";
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowforTheDB.'");
              document.getElementById("especificacion0").readOnly = true;
              document.getElementById("valorDef0").readOnly = true;</script>';
              $rowforTheDB="1,NLS_NCHAR_CHARACTERSET,,WE8MSWIN1252";
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowforTheDB.'");
              document.getElementById("especificacion1").readOnly = true;
              document.getElementById("valorDef1").readOnly = true;
              document.getElementById("childOfSpec").value="2";</script>';
            break;
            case "SQL Server":
              $rowforTheDB="0,COLLECTION,,SQL_Latin1_General_CP1_CI_AS";
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowforTheDB.'");
              document.getElementById("especificacion0").readOnly = true;
              document.getElementById("valorDef0").readOnly = true;</script>';
              $rowforTheDB="1,Nombre de instancia,,MSSQLSERVER";
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowforTheDB.'");
              document.getElementById("especificacion1").readOnly = true;
              document.getElementById("valorDef1").readOnly = true;
              document.getElementById("childOfSpec").value="2";</script>';
            break;
            case "Informix":
              $rowforTheDB="0,Herramientas requeridas,,SI/NO (si: especificar el <br>nombre de la herramienta)";
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowforTheDB.'");
              document.getElementById("especificacion0").readOnly = true;
              document.getElementById("valorDef0").readOnly = true;
              document.getElementById("childOfSpec").value="1";</script>';
            break;
          }
        }
      ?>
      Usuarios:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="20%">Usuario</th>
            <th width="30%">Rol (aplicativo,<br>Consulta, reporteo, etc</th>
            <th width="50%">Privilegios</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="usuariosPlaceholder" ></tr>
      </table>
      <?php
        if($firstDBInfo['usuarios']>0)
        {
          try
          {
            $sql = "select * from filtroDBUsuarios where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['dbuName'].",".$rowOfUser['dbuRol'].",".$rowOfUser['dbuPrivilegios'];
              echo '<script type="text/javascript">chargeUsuarios("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
      <!-- Botones principales -->
        <div align="center">
          <input type="submit" <?php if($keyValue==""){echo "disabled";}?> id="theSubmit" value="Modificar especificaciones">
        </div>
    </form>
  </div>
</body>
<script>
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function giveSpc(select)
  {
    //  recuperar informacion de equipo
      workplace1="specPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfSpec").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfSpec"+mimic));
        }
      }
      else
      {
        //  Crear especificaciones extra 
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla 
            newTabla = document.createElement("tr");
          //  añadir Especificacion
            theSpecPlace = document.createElement("td");
            theSpec = document.createElement("input");
            theSpec.name="especificacion"+creatorOfNodes;
            theSpec.value="Especificacion "+(creatorOfNodes+1);
            theSpec.type="text";
            theSpec.required=true;
            theSpecPlace.appendChild(theSpec);
            theSpec.size=25;
          //  añadir valor
            theValuePlace = document.createElement("td");
            theValu1 = document.createElement("input");
            theValu1.name="valor"+creatorOfNodes;
            theValu1.value="Valor "+(creatorOfNodes+1);
            theValu1.type="text";
            theValu1.required=true;
            theValuePlace.appendChild(theValu1);
            theValu1.size=15;
          //  añadir comentarios
            theValDefPlace = document.createElement("td");
            theValDef = document.createElement("input");
            theValDef.name="valorDef"+creatorOfNodes;
            theValDef.value="Valor default "+(creatorOfNodes+1);
            theValDef.type="text";
            theValDefPlace.appendChild(theValDef);
            theValDef.size=60;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfSpec"+creatorOfNodes;
            newTabla.id = "childOfSpec"+creatorOfNodes;
            newTabla.appendChild(theSpecPlace);
            newTabla.appendChild(theValuePlace);
            newTabla.appendChild(theValDefPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfSpec").value=select.value;
  }
  function giveUsuarios(select)
  {
    //  recuperar informacion de equipo
      workplace1="usuariosPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfUsuarios").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfUsuarios"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra 
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos 
            newTabla = document.createElement("tr");
          //  añadir dbuName
            theDBUNamePlace = document.createElement("td");
            theDBUName = document.createElement("input");
            theDBUName.name="dbuName"+creatorOfNodes;
            theDBUName.value="Nombre "+(creatorOfNodes+1);
            theDBUName.required=true;
            theDBUName.type="text";
            theDBUNamePlace.appendChild(theDBUName);
            theDBUName.size=20;
          //  añadir dbuRol
            theDBURolPlace = document.createElement("td");
            theDBURol = document.createElement("input");
            theDBURol.name="dbuRol"+creatorOfNodes;
            theDBURol.value="Rol "+(creatorOfNodes+1);
            theDBURol.required=true;
            theDBURol.type="text";
            theDBURolPlace.appendChild(theDBURol);
            theDBURol.size=34;
          //  añadir Privilegios
            theDBUPrivilegiosPlace = document.createElement("td");
            theDBUPrivilegios = document.createElement("input");
            theDBUPrivilegios.name="dbuPrivilegios"+creatorOfNodes;
            theDBUPrivilegios.value="Privilegios "+(creatorOfNodes+1);
            theDBUPrivilegios.type="text";
            theDBUPrivilegiosPlace.appendChild(theDBUPrivilegios);
            theDBUPrivilegios.size=46;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfUsuarios"+creatorOfNodes;
            newTabla.id = "childOfUsuarios"+creatorOfNodes;
            newTabla.appendChild(theDBUNamePlace);
            newTabla.appendChild(theDBURolPlace);
            newTabla.appendChild(theDBUPrivilegiosPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfUsuarios").value=select.value;
  }
  function portCheck(value)
  {
    switch(baseACT)
    {
      case "Oracle":
        if(value=="1521")
        {
          document.getElementById("theSubmit").disabled=true;
          document.getElementById("hidePuerto").style ="";
        }
        else
        {
          document.getElementById("theSubmit").disabled=false;
          document.getElementById("hidePuerto").style ="display:none;";
        }
      break;
      case "MySQL":
        if(value=="3306")
        {
          document.getElementById("theSubmit").disabled=true;
          document.getElementById("hidePuerto").style ="";
        }
        else
        {
          document.getElementById("theSubmit").disabled=false;
          document.getElementById("hidePuerto").style ="display:none;";
        }
      break;
      case "DB2":
        if(value=="5000")
        {
          document.getElementById("theSubmit").disabled=true;
          document.getElementById("hidePuerto").style ="";
        }
        else
        {
          document.getElementById("theSubmit").disabled=false;
          document.getElementById("hidePuerto").style ="display:none;";
        }
      break;
      case "SQL Server":
        if(value=="1433")
        {
          document.getElementById("theSubmit").disabled=true;
          document.getElementById("hidePuerto").style ="";
        }
        else
        {
          document.getElementById("theSubmit").disabled=false;
          document.getElementById("hidePuerto").style ="display:none;";
        }
      break;
      case "Informix":
        if(value=="1500"||value=="1501"||value=="1502"||value=="1581")
        {
          document.getElementById("theSubmit").disabled=true;
          document.getElementById("hidePuerto").style ="";
        }
        else
        {
          document.getElementById("theSubmit").disabled=false;
          document.getElementById("hidePuerto").style ="display:none;";
        }
      break;
      default:
        document.getElementById("theSubmit").disabled=false;
        document.getElementById("hidePuerto").style ="display:none;";
      break;
    }
  }
</script>
</html>