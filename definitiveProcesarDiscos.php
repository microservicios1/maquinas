<html>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Discos registradas </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
        $sql="delete from disco where interId='".$_POST['folio'].$_POST['machine']."'";        
        mysqli_query($conn,$sql);
        $expectedData =array('disco','mountpoint','storage','tipo','proposito','performance');
        $expectedData2 =array('disco','storageS','storageE','proposito');
        $noError=1;
        $staticS=0;
        $staticE=0;
        $sharedS=0;
        $sharedE=0;
        for($i=0;$i<$_POST['childOfStatic'];$i++)
        {
          for($j=0;$j<sizeof($expectedData2);$j++)  
            $numeredData[$j]=$expectedData2[$j]."R".$i;
          if($noError==1)
          {
            $sql="insert into disco(interId,nombreDisco,sizeDiscoS,sizeDiscoE,proposito,tipo) values ('".$_POST['folio'].$_POST['machine']."','".$_POST[$numeredData[0]]."',".$_POST[$numeredData[1]].",".$_POST[$numeredData[2]].",'".$_POST[$numeredData[3]]."','Estatico')";            
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
            else
            {
              $staticS=$staticS+$_POST[$numeredData[1]];
              $staticE=$staticE+$_POST[$numeredData[2]];
            }
          }
        }
        $expectedData2 =array('disco','sharedS','sharedE','proposito');
        for($i=0;$i<$_POST['childOfShared'];$i++)
        {
          for($j=0;$j<sizeof($expectedData2);$j++)  
            $numeredDataS[$j]=$expectedData2[$j]."S".$i;
          if($noError==1)
          {
            $sql="insert into disco(interId,nombreDisco,sizeDiscoS,sizeDiscoE,proposito,tipo) values ('".$_POST['folio'].$_POST['machine']."','".$_POST[$numeredDataS[0]]."',".$_POST[$numeredDataS[1]].",".$_POST[$numeredDataS[2]].",'".$_POST[$numeredDataS[3]]."','Compartido')";            
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
            else
            {
              $sharedS=$sharedS+$_POST[$numeredDataS[1]];
              $sharedE=$sharedE+$_POST[$numeredDataS[2]];
            }
          }
        }
        for($i=0;$i<$_POST['childOfLogVol'];$i++)
        {
          for($j=0;$j<sizeof($expectedData);$j++)  
            $numeredDataLV[$j]=$expectedData[$j]."E".$i;
          if($noError==1)
          {
            $sql="insert into disco values ('".$_POST['folio'].$_POST['machine']."','".$_POST[$numeredDataLV[0]]."','".$_POST[$numeredDataLV[1]]."',".$_POST[$numeredDataLV[2]].",0,'".$_POST[$numeredDataLV[3]]."','".$_POST[$numeredDataLV[4]]."','".$_POST[$numeredDataLV[5]]."','LogVol')";            
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
          }
        }
        $sql="update maquinas set maquinas.storageSolicitado=".$staticS.",maquinas.storageEntregado=".$staticE.",maquinas.sharedSolicitado=".$sharedS.",maquinas.sharedEntregado=".$sharedE." where interId='".$_POST['folio'].$_POST['machine']."'";   
        mysqli_query($conn,$sql);
        if($noError==0)
        {
          mysqli_rollback($conn);
          echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br></p>";
        }
        else
        {
          mysqli_commit($conn);
          echo "<br><p>Discos agregados a VM <br></p>";
        }
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <!--<button type="button" class="evilbtn">TD Automatizaci�n <br>Servicios Infraestructura</button>-->
    <p>  </p><br><br>
  </div>
</body>
</html>