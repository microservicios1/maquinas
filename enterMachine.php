<html lang="es"> 
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <?php
      if($_POST['action']==2||$_POST['action']==4)
        echo "<title>Modificacion de  VM</title>";
      if($_POST['action']==1||$_POST['action']==3)
        echo "<title>Registro de VM</title>";
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
    ?>
  </head>
  <script>
    function isIntNumber(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
    function isTheip(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
        return false;
      return true;
    }
    function rev(event)
    {
      var k = (event.which) ? event.which : event.keyCode;
      if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k ==32) )
        return true;
      else
        return false;
    }
    function giveSomeStandalone(ambiente,folio)
    {
      //  recuperar informacion de equipo
        workplace1="standalonePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStandalone").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theNOTitle = document.createElement("th");
          theNOTitle.style="width:6%";
          theNOTitle.innerHTML = "Standalone :";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:20%";
          theIPTitle.innerHTML = "IP :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:16%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:12%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:12%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="Memoria<br>RAM (Gb) :";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:12%";
          theDiscoTitle.innerHTML="Disco Duro :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:12%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "StandaloneTitle";
          newTLine.id = "StandaloneTitle";
          newTLine.appendChild(theNOTitle);
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Numero de maquina
            theNOPlace = document.createElement("td");
            theNOPlace.style="width:6%";
            theNOPlace.innerHTML=mimic+1;
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:20%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "IP(Opcional):";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="StandaloneIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="IP ++";
              theIPButton.onclick = function(){AddIP('1',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPStandalone"+mimic;
            IPCount.id="IPStandalone"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  Numero de arreglo 
            theArreglo = document.createElement("input");
            theArreglo.name="S1arreglo"+mimic;
            theArreglo.id="S1arreglo"+mimic;
            theArreglo.value=0;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="S1tipo"+mimic;
            theTipo.id="S1tipo"+mimic;
            theTipo.value=0;
            theTipo.type="hidden";
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:16%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="S1aplicacion"+mimic;
            theAplication.id="S1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  ambienteSolicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:12%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="S1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.required=true; 
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  vCPU
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:12%";
            thevCPU = document.createElement("input");
            thevCPU.name="S1CPU"+mimic;
            thevCPU.id="S1CPU"+mimic;
            thevCPU.required=true; 
            thevCPU.type="number";
            thevCPU.style="width:100%";
            thevCPU.min=1;
            thevCPU.step=1;
            thevCPU.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPU);
          //  RAM
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAM = document.createElement("input");
            theRAM.name="S1RAM"+mimic;
            theRAM.id="S1RAM"+mimic;
            theRAM.required=true; 
            theRAM.style="width:100%";
            theRAM.type="number";
            theRAM.min=1;
            theRAM.step=1;
            theRAM.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAM);
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:12%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Agregar Storage";
            theHD.onclick = function(){window.open('specs.php?accion=6&machine=s1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=300,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD); 
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:12%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=1&machine=s1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecs2 = document.createElement("input");
            theSpecs2.type="button";
            theSpecs2.style="width:100%";
            theSpecs2.value="DB Requerimientos";
            theSpecs2.onclick = function(){window.open('specs.php?accion=2&machine=s1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1); 
            theSpecsPlace.appendChild(theSpecs2); 
          //  Line
            newLine.name = "childStandalone"+mimic.toString();
            newLine.id = "childStandalone"+mimic.toString();
            newLine.appendChild(theNOPlace);
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfStandalone").value=mimic;
    }
    function giveSomeApliance(ambiente,folio)
    {
      //  recuperar informacion de equipo
        workplace1="apliancePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfApliance").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theNOTitle = document.createElement("th");
          theNOTitle.style="width:6%";
          theNOTitle.innerHTML = "Apliance :";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:20%";
          theIPTitle.innerHTML = "IP :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:16%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:12%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:12%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="Memoria<br>RAM (Gb) :";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:12%";
          theDiscoTitle.innerHTML="Disco Duro :";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:12%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "AplianceTitle";
          newTLine.id = "AplianceTitle";
          newTLine.appendChild(theNOTitle);
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Numero de maquina
            theNOPlace = document.createElement("td");
            theNOPlace.style="width:6%";
            theNOPlace.innerHTML=mimic+1;
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:20%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%";
              theIP.innerHTML = "IP(Opcional):";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id="AplianceIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="IP ++";
              theIPButton.onclick = function(){AddIP('2',numb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name="IPApliance"+mimic;
            IPCount.id="IPApliance"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  Numero de arreglo 
            theArreglo = document.createElement("input");
            theArreglo.name="A1arreglo"+mimic;
            theArreglo.id="A1arreglo"+mimic;
            theArreglo.value=1;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="A1tipo"+mimic;
            theTipo.id="A1tipo"+mimic;
            theTipo.value=1;
            theTipo.type="hidden";
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:16%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="A1aplicacion"+mimic;
            theAplication.id="A1aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  ambienteSolicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:12%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="A1ambienteSolicitado"+mimic;
            theAmbienteSolicitado.required=true; 
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  vCPU
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:12%";
            thevCPU = document.createElement("input");
            thevCPU.name="A1CPU"+mimic;
            thevCPU.id="A1CPU"+mimic;
            thevCPU.required=true; 
            thevCPU.type="number";
            thevCPU.style="width:100%";
            thevCPU.min=1;
            thevCPU.step=1;
            thevCPU.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPU);
          //  RAM
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAM = document.createElement("input");
            theRAM.name="A1RAM"+mimic;
            theRAM.id="A1RAM"+mimic;
            theRAM.required=true; 
            theRAM.type="number";
            theRAM.style="width:100%";
            theRAM.min=1;
            theRAM.step=1;
            theRAM.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAM);
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:12%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Agregar Storage";
            theHD.onclick = function(){window.open('specs.php?accion=6&machine=a1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=300,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD); 
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:12%";
            theSpecs1 = document.createElement("input");
            theSpecs1.type="button";
            theSpecs1.style="width:100%";
            theSpecs1.value="S.O.Requerimientos";
            theSpecs1.onclick = function(){window.open('specs.php?accion=1&machine=a1m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
            theSpecsPlace.appendChild(theSpecs1);
          //  Line
            newLine.name = "childApliance"+mimic.toString();
            newLine.id = "childApliance"+mimic.toString();
            newLine.appendChild(theNOPlace);
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfApliance").value=mimic;
    }
    function giveSomeCluster(ambiente,folio)
    {
      //  recuperar informacion de equipo
        workplace1="clusterPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfCluster").value;
        mimic=parseInt(mimic);
      //  Titulos
        newTLine = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine.id="ClusterTitle"+mimic;
        theNOTitle = document.createElement("th");
        theNOTitle.innerHTML = "Cluster "+parseInt(mimic+1)+":";
        theNOTitle.style="font-size: 12px;";
        theCaller = document.createElement("th");
        theButton = document.createElement("input");
        theButton.type="button";
        theButton.value="Maquina++";
        var numb= parseInt(mimic);
        theButton.onclick = function(){AddClusterMachine(numb,ambiente,folio)}; 
        theCaller.appendChild(theButton); 
        theNumberMachine = document.createElement("input");
        theNumberMachine.name="machinesOnCluster"+mimic;
        theNumberMachine.id="machinesOnCluster"+mimic;
        theNumberMachine.value=0;
        theNumberMachine.type="hidden";
        newTLine.name = "ClusterTitle"+mimic;
        newTLine.id = "ClusterTitle"+mimic;
        newTLine.appendChild(theNOTitle);
        newTLine.appendChild(theCaller);
        newTLine.appendChild(theButton);
        newTLine.appendChild(theNumberMachine);
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      //  Titulos
        newTLine = document.createElement("tr");
        newTLine.style="width:100%";
        newTLine.id="ClusterInfo"+mimic;
        workplace1.parentNode.insertBefore(newTLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfCluster").value=mimic;
    }
    function AddClusterMachine(clusterNumb,ambiente,folio)
    {
      //  recuperar informacion de equipo
        workplace1="ClusterInfo"+clusterNumb;
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("machinesOnCluster"+clusterNumb).value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theNOTitle = document.createElement("th");
          theNOTitle.style="width:6%";
          theNOTitle.innerHTML = "Maquina :";
          theIPTitle = document.createElement("th");
          theIPTitle.style="width:20%";
          theIPTitle.innerHTML = "IP :";
          theAplicacionTitle = document.createElement("th");
          theAplicacionTitle.style="width:16%";
          theAplicacionTitle.innerHTML="Aplicacion :";
          theAmbienteTitle = document.createElement("th");
          theAmbienteTitle.style="width:12%";
          theAmbienteTitle.innerHTML="Ambiente :";
          thevCPUTitle = document.createElement("th");
          thevCPUTitle.style="width:12%";
          thevCPUTitle.innerHTML="vCPUs :";
          theRAMTitle = document.createElement("th");
          theRAMTitle.style="width:10%";
          theRAMTitle.innerHTML="Memoria<br>RAM (Gb) :";
          theDiscoTitle = document.createElement("th");
          theDiscoTitle.style="width:12%";
          theDiscoTitle.innerHTML="Disco <br>Duro/Compartido:";
          theEspecificacionesTitle = document.createElement("th");
          theEspecificacionesTitle.style="width:12%";
          theEspecificacionesTitle.innerHTML="Especificaciones :";
          newTLine.name = "Cluster"+clusterNumb+"Title";
          newTLine.id = "Cluster"+clusterNumb+"Title";
          newTLine.appendChild(theNOTitle);
          newTLine.appendChild(theIPTitle);
          newTLine.appendChild(theAplicacionTitle);
          newTLine.appendChild(theAmbienteTitle);
          newTLine.appendChild(thevCPUTitle);
          newTLine.appendChild(theRAMTitle);
          newTLine.appendChild(theDiscoTitle);
          newTLine.appendChild(theEspecificacionesTitle);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          //  Numero de maquina
            theNOPlace = document.createElement("td");
            theNOPlace.style="width:6%";
            theNOPlace.innerHTML=mimic+1;
          //  IP
            theIPPlace = document.createElement("td");
            theIPPlace.style="width:20%";
            var someIPTable = document.createElement("table");
            someIPTable.style="width:100%";
            newLineIn2 = document.createElement("tr");
            newLineIn2.style="width:100%";
            // Titulos
              theIP = document.createElement("th");
              theIP.style="width:30%;";
              theIP.innerHTML = "IP(Opcional):";
              theIPDescription = document.createElement("th");
              theIPDescription.style="width:50%; ";
              theIPDescription.innerHTML="Descripcion :";
              theKillIPButton = document.createElement("th");
              theKillIPButton.style="width:20%";
              newLineIn2.appendChild(theIP);
              newLineIn2.appendChild(theIPDescription);
              newLineIn2.appendChild(theKillIPButton);
            // Placeholder
              newIPPlaceholder = document.createElement("tr");
              newIPPlaceholder.style="width:100%";
              newIPPlaceholder.id=clusterNumb+"ClusterIPPlaceholder"+mimic;
            // buton ++
              theIPButtonPlace = document.createElement("tr");
              theIPButtonPlace.style="width:100%";
              theIPButtonPlace.class="agregar";
              theIPButtonPlace.Colspan="4";
              theIPButton = document.createElement("input");
              theIPButton.type="button";
              theIPButton.style="width:100%;font-size: 10px;";
              theIPButton.name="IPButton"+mimic;
              theIPButton.id="IPButton"+mimic;
              theIPButton.value="IP ++";
              theIPButton.onclick = function(){AddIP('3',numb,clusterNumb)}; 
              theIPButtonPlace.appendChild(theIPButton); 
            someIPTable.appendChild(newLineIn2);  
            someIPTable.appendChild(newIPPlaceholder); 
            someIPTable.appendChild(theIPButtonPlace);  
            theIPPlace.appendChild(someIPTable);
            IPCount = document.createElement("input");
            IPCount.name=clusterNumb+"IPCluster"+mimic;
            IPCount.id=clusterNumb+"IPCluster"+mimic;
            IPCount.value=0;
            IPCount.type="hidden";
          //  Numero de arreglo 
            theArreglo = document.createElement("input");
            theArreglo.name="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.id="C"+clusterNumb+"arreglo"+mimic;
            theArreglo.value=2+clusterNumb;
            theArreglo.type="hidden";
            theTipo = document.createElement("input");
            theTipo.name="C"+clusterNumb+"tipo"+mimic;
            theTipo.id="C"+clusterNumb+"tipo"+mimic;
            theTipo.value=2;
            theTipo.type="hidden";
          //  Aplication
            theAplicationPlace = document.createElement("td");
            theAplicationPlace.style="width:16%";
            theAplication = document.createElement("input");
            theAplication.style="width:100%";
            theAplication.name="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.id="C"+clusterNumb+"aplicacion"+mimic;
            theAplication.type="text";
            theAplication.onkeypress = function(){return rev(event)}; 
            theAplication.setAttribute("autocomplete", "off");
            theAplicationPlace.appendChild(theAplication);
          //  ambienteSolicitado
            theAmbienteSolicitadoPlace = document.createElement("td");
            theAmbienteSolicitadoPlace.style="width:12%";
            theAmbienteSolicitado = document.createElement("select");
            theAmbienteSolicitado.name="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.id="C"+clusterNumb+"ambienteSolicitado"+mimic;
            theAmbienteSolicitado.required=true; 
            theAmbienteSolicitado.style="width:100%";
            opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = '';
            theAmbienteSolicitado.appendChild(opt);
            var ambienteData = ambiente.split(",");
            for(i=0;i<ambienteData.length;i++)
            {
              opt = document.createElement('option');
              opt.value = ambienteData[i];
              opt.innerHTML = ambienteData[i];
              theAmbienteSolicitado.appendChild(opt);
            }
            theAmbienteSolicitadoPlace.appendChild(theAmbienteSolicitado);
          //  vCPU
            thevCPUPlace = document.createElement("td");
            thevCPUPlace.style="width:12%";
            thevCPU = document.createElement("input");
            thevCPU.name="C"+clusterNumb+"CPU"+mimic;
            thevCPU.id="C"+clusterNumb+"CPU"+mimic;
            thevCPU.required=true; 
            thevCPU.type="number";
            thevCPU.style="width:100%";
            thevCPU.min=1;
            thevCPU.step=1;
            thevCPU.onkeypress = function(){return isIntNumber(event)}; 
            thevCPUPlace.appendChild(thevCPU);
          //  RAM
            theRAMPlace = document.createElement("td");
            theRAMPlace.style="width:10%";
            theRAM = document.createElement("input");
            theRAM.name="C"+clusterNumb+"RAM"+mimic;
            theRAM.id="C"+clusterNumb+"RAM"+mimic;
            theRAM.required=true; 
            theRAM.style="width:100%";
            theRAM.type="number";
            theRAM.min=1;
            theRAM.step=1;
            theRAM.onkeypress = function(){return isIntNumber(event)}; 
            theRAMPlace.appendChild(theRAM);
          //  Disco Duro 
            theHDPlace = document.createElement("td");
            theHDPlace.style="width:12%";
            theHD = document.createElement("input");
            theHD.type="button";
            theHD.style="width:100%";
            theHD.value="Agregar Storage";
            theHD.onclick = function(){window.open('specs.php?accion=6&machine=c'+clusterNumb+'m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=800,height=600,resizable=0,left=40px,top=50px')}; 
            theHDPlace.appendChild(theHD); 
          //  Especificaciones
            theSpecsPlace = document.createElement("td");
            theSpecsPlace.style="width:6%";
            if(mimic==0)
            {
              theSpecs1 = document.createElement("input");
              theSpecs1.type="button";
              theSpecs1.style="width:100%";
              theSpecs1.value="S.O.Requerimientos";
              theSpecs1.onclick = function(){window.open('specs.php?accion=1&machine=c'+clusterNumb+'m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
              theSpecs2 = document.createElement("input");
              theSpecs2.type="button";
              theSpecs2.style="width:100%";
              theSpecs2.value="DB Requerimientos";
              theSpecs2.onclick = function(){window.open('specs.php?accion=2&machine=c'+clusterNumb+'m'+numb+'&folio='+folio,'','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')}; 
              theSpecsPlace.appendChild(theSpecs1);
              theSpecsPlace.appendChild(theSpecs2);
            }
          //  Line
            newLine.name = clusterNumb+"childCluster"+mimic.toString();
            newLine.id = clusterNumb+"childCluster"+mimic.toString();
            newLine.appendChild(theNOPlace);
            newLine.appendChild(theIPPlace);
            newLine.appendChild(IPCount);
            newLine.appendChild(theArreglo);
            newLine.appendChild(theTipo);
            newLine.appendChild(theAplicationPlace);
            newLine.appendChild(theAmbienteSolicitadoPlace);
            newLine.appendChild(thevCPUPlace);
            newLine.appendChild(theRAMPlace);
            newLine.appendChild(theHDPlace);
            newLine.appendChild(theSpecsPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("machinesOnCluster"+clusterNumb).value=mimic;
    }
    function AddIP(type,diskInfo,cluster)
    {
      //  recuperar en que disco se trabajara
        if(type==1)
        { 
          workInThisDisk="StandaloneIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPStandalone"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd="IPStandalone"+diskInfo;
        }
        if(type==2)
        { 
          workInThisDisk="AplianceIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic="IPApliance"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd="IPApliance"+diskInfo;
        }
        if(type==3)
        { 
          workInThisDisk=cluster+"ClusterIPPlaceholder"+diskInfo;
          workInThisDisk=document.getElementById(workInThisDisk);
          mimic=cluster+"IPCluster"+diskInfo;
          mimic=document.getElementById(mimic).value;
          mimic=parseInt(mimic);
          var wkitd=cluster+"IPCluster"+diskInfo;
        }
      //  create nueva tabla de discos
        newLine = document.createElement("tr");
        if(type==1)
          newLine.id = diskInfo+"IPStandalone"+mimic;
        if(type==2)
          newLine.id = diskInfo+"IPApliance"+mimic;
        if(type==3)
          newLine.id = cluster+"machine"+diskInfo+"IPCluster"+mimic;
        newLine.style="width:100%";
      //  agregar IP
        theIPPlace2 = document.createElement("td");
        theIPPlace2.style="width:30%";
        theIP2 = document.createElement("input");
        theIP2.style="width:100%";
        if(type==1)
        {
          theIP2.name="s1IP"+diskInfo+"e"+mimic;
          theIP2.id="s1IP"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theIP2.name="a1IP"+diskInfo+"e"+mimic;
          theIP2.id="a1IP"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theIP2.name="c"+cluster+"IP"+diskInfo+"e"+mimic;
          theIP2.id="c"+cluster+"IP"+diskInfo+"e"+mimic;
        }
        theIP2.type="text";
        theIP2.onkeypress = function(){return isTheip(event)}; 
        theIP2.setAttribute("autocomplete", "off");
        theIP2.required=true; 
        theIPPlace2.appendChild(theIP2);
      //  añadir descripcion de disco
        theDescIPPlace = document.createElement("td");
        theDescIPPlace.style="width:32%";
        theDescIP = document.createElement("input");
        theDescIP.style="width:100%";
        if(type==1)
        {
          theDescIP.name="s1dip"+diskInfo+"e"+mimic;
          theDescIP.id="s1dip"+diskInfo+"e"+mimic;
        }
        if(type==2)
        {
          theDescIP.name="a1dip"+diskInfo+"e"+mimic;
          theDescIP.id="a1dip"+diskInfo+"e"+mimic;
        }
        if(type==3)
        {
          theDescIP.name="c"+cluster+"dip"+diskInfo+"e"+mimic;
          theDescIP.id="c"+cluster+"dip"+diskInfo+"e"+mimic;
        }
        theDescIP.type="text";
        theDescIP.setAttribute("autocomplete", "off");
        theDescIPPlace.appendChild(theDescIP);
      //  añadir boton para eliminar
        theButtonPlace = document.createElement("td");
        theButtonPlace.style="width:20%";
        theButtonPlace.class="eliminar";
        theButton = document.createElement("input");
        theButton.type="button";
        theButton.value="X";
        if(type==1)
        {
          var numb=diskInfo+"IPStandalone"+mimic;
          var prefix1="s1IP"+diskInfo+"e";
          var prefix2="s1dip"+diskInfo+"e";
          var prefix3=diskInfo+"IPStandalone";
        }
        if(type==2)
        {
          var numb=diskInfo+"IPApliance"+mimic;
          var prefix1="a1IP"+diskInfo+"e";
          var prefix2="a1dip"+diskInfo+"e";
          var prefix3=diskInfo+"IPApliance";
        }
        if(type==3)
        {
          var numb=cluster+"machine"+diskInfo+"IPCluster"+mimic;
          var prefix1="c"+cluster+"IP"+diskInfo+"e";
          var prefix2="c"+cluster+"dip"+diskInfo+"e";
          var prefix3=cluster+"machine"+diskInfo+"IPCluster";
        }
        var extraNumb=mimic;
        theButton.onclick = function(){killThisIP(prefix3,extraNumb,wkitd,prefix1,prefix2)}; 
        theButtonPlace.appendChild(theButton);
      //  agregar datos predefinidos a nueva tabla 
        newLine.appendChild(theIPPlace2);
        newLine.appendChild(theDescIPPlace);
        newLine.appendChild(theButtonPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.parentNode.insertBefore(newLine,workInThisDisk);
        mimic=parseInt(mimic)+1;
        if(type==1)
          document.getElementById("IPStandalone"+diskInfo).value=mimic;
        if(type==2)
          document.getElementById("IPApliance"+diskInfo).value=mimic;
        if(type==3)
          document.getElementById(cluster+"IPCluster"+diskInfo).value=mimic;
    }
    function killThisIP(prefix3,numero,wkitd,prefix1,prefix2)
    {
      //alert(prefix3+","+numero+","+wkitd+","+prefix1+","+prefix2);
      // recuperar informacion de equipo
        someMime=document.getElementById(wkitd).value;
        someMime=parseInt(someMime)-1;
      // eliminar
        if(numero!=someMime)
        {
          for(i=numero;i<someMime;i++)
          {
            // IP
                temp1=prefix1+(i+1);
                some=document.getElementById(temp1).value;
                temp1=prefix1+i;
                document.getElementById(temp1).value=some;
            // dip
                temp1=prefix2+(i+1);
                some=document.getElementById(temp1).value;
                temp1=prefix2+i;
                document.getElementById(temp1).value=some;
          }
        }
      document.getElementById(wkitd).value=someMime;
      var el =prefix3+someMime.toString();
      document.getElementById(el).remove();
    }
    function chargeThisMachine(datos,numero,Prefijo1)
    {
      //alert(numero);
      //alert(datos);
      bullets=datos.split(",");
      document.getElementById(Prefijo1+"aplicacion"+numero).value=bullets[0];
      document.getElementById(Prefijo1+"ambienteSolicitado"+numero).value=bullets[1];
      document.getElementById(Prefijo1+"CPU"+numero).value=bullets[2];
      document.getElementById(Prefijo1+"RAM"+numero).value=bullets[3];
    }
    function chargeIP(datos,maquinan,disco,prefijo)
    {
      bullets=datos.split(",");
      document.getElementById(prefijo+"IP"+maquinan+"e"+disco).value=bullets[0];
      document.getElementById(prefijo+"dip"+maquinan+"e"+disco).value=bullets[1];
    }
  </script>
  <body>
  <?php
    $sql="select nombre from ambiente";
    $toFillTemplate = mysqli_query($conn,$sql);
    $k=0;
    if(! $toFillTemplate)
      echo "<option value=\"\">Error de conexion</option>";
    else
      while($templateValues = mysqli_fetch_array($toFillTemplate))
      {
        if($k==0)
          $Ambiente=$templateValues['nombre'];
        else
          $Ambiente=$Ambiente.",".$templateValues['nombre'];
        $k=1;
      }
  ?>
    <form method="post"  action="procesarMaquinasMode2.php" class="containerOfRod">
      <br><br>
      <!-- Botones Principales -->
        <div class="shad" align="center">
          &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Registrar Maquinas">
          <button id="addStandalone" name="addStandalone" type="button" class="btn btn-warning" onclick="giveSomeStandalone('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Standalone + </button>
          <button id="addApliance" name="addApliance" type="button" class="btn btn-warning" onclick="giveSomeApliance('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Apliance + </button>
          <button id="addCluster" name="addCluster" type="button" class="btn btn-warning" onclick="giveSomeCluster('<?php echo $Ambiente;?>','<?php echo $_POST['solicitud'];?>')"> Cluster + </button>
        </div>
        <br><br>
      <!-- Valores rescatados temp -->
        <input type="hidden" name="solicitud" value="<?php echo $_POST['solicitud'];?>" >
        <input type="hidden" name="action" value="<?php if($_POST['action']>2){$action1=$_POST['action']-2;}else {$action1=$_POST['action'];} echo $action1;?>" >
      <!-- Placeholders -->
        <input type="hidden" id="childOfStandalone" name="childOfStandalone" value="0">
        <input type="hidden" id="childOfApliance" name="childOfApliance" value="0">
        <input type="hidden" id="childOfCluster" name="childOfCluster" value="0">
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="standalonePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="apliancePlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="clusterPlaceholder" ></tr>
        </table>
        <?php
          if($_POST['action']==2)
          {
            $sql="select * from filtroMaquinas where folio='".$_POST['solicitud']."'";
            $someThings = mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r>0)
            {
              $lastArreglo=-1;
              while($maquinaPr = mysqli_fetch_array($someThings))
              {
                $dato=substr($maquinaPr['interId'],strlen($maquinaPr['folio']));   
                if($maquinaPr['tipo']==2)
                {
                  if($maquinaPr['arreglo']!=$lastArreglo)
                  {
                    echo '<script type="text/javascript">giveSomeCluster("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                    $lastArreglo=$maquinaPr['arreglo'];
                  }
                }
                switch($dato[0])
                {
                  case "s":
                    echo '<script type="text/javascript">giveSomeStandalone("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                    $pos = strpos($dato, 'm');
                    $dato=substr($dato,($pos+1));
                    $infoMachine=$maquinaPr['aplicacion'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['CPU'].",".$maquinaPr['RAM'];
                    echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","S1");</script>';
                    /*$sql="select * from filtroDiscos where interId='".$maquinaPr['interId']."'";
                    $diskThings = mysqli_query($conn,$sql);
                    $k=0;
                    while($actDisk = mysqli_fetch_array($diskThings))
                    {
                      echo '<script type="text/javascript">AddStaticDisk("1","'.$dato.'");</script>';
                      $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['descripcion'];
                      echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                      $k++;
                    }*/
                    $sql="select * from filtroIP where interId='".$maquinaPr['interId']."'";
                    $ipThings = mysqli_query($conn,$sql);
                    $k=0;
                    while($actip = mysqli_fetch_array($ipThings))
                    {
                      echo '<script type="text/javascript">AddIP("1","'.$dato.'");</script>';
                      $infoDisk=$actip['IP'].",".$actip['descripcion'];
                      echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","s1");</script>';
                      $k++;
                    }
                  break;
                  case "a":
                    echo '<script type="text/javascript">giveSomeApliance("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                    $pos = strpos($dato, 'm');
                    $dato=substr($dato,($pos+1));
                    $infoMachine=$maquinaPr['aplicacion'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['CPU'].",".$maquinaPr['RAM'];
                    echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","A1");</script>';
                    /*$sql="select * from filtroDiscos where interId='".$maquinaPr['interId']."'";
                    $diskThings = mysqli_query($conn,$sql);
                    $k=0;
                    while($actDisk = mysqli_fetch_array($diskThings))
                    {
                      echo '<script type="text/javascript">AddStaticDisk("2","'.$dato.'");</script>';
                      $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['descripcion'];
                      echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                      $k++;
                    }*/
                    $sql="select * from filtroIP where interId='".$maquinaPr['interId']."'";
                    $ipThings = mysqli_query($conn,$sql);
                    $k=0;
                    while($actip = mysqli_fetch_array($ipThings))
                    {
                      echo '<script type="text/javascript">AddIP("2","'.$dato.'");</script>';
                      $infoDisk=$actip['IP'].",".$actip['descripcion'];
                      echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","a1");</script>';
                      $k++;
                    }
                  break;
                  case "c":
                    $pos = strpos($dato, 'm');
                    $clusterNumber=substr($dato,0,-(strlen($dato)-$pos));
                    $clusterNumber=substr($clusterNumber,1);
                    $dato=substr($dato,($pos+1));
                    echo '<script type="text/javascript">AddClusterMachine("'.$clusterNumber.'","'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                    $infoMachine=$maquinaPr['aplicacion'].",".$maquinaPr['ambienteSolicitado'].",".$maquinaPr['CPU'].",".$maquinaPr['RAM'];
                    echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$dato.'","C'.$clusterNumber.'");</script>';
                    /*$sql="select * from filtroDiscos where interId='".$maquinaPr['interId']."'";
                    $diskThings = mysqli_query($conn,$sql);
                    $k=0;
                    $t=0;
                    while($actDisk = mysqli_fetch_array($diskThings))
                    {
                      if($actDisk['tipoDisco']=="Estatico")
                      {
                        echo '<script type="text/javascript">AddStaticDisk("3","'.$dato.'","'.$clusterNumber.'");</script>';
                        $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['descripcion'];
                        echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                        $k++;
                      }
                      if($actDisk['tipoDisco']=="Compartido")
                      {
                        echo '<script type="text/javascript">AddSharedDisk("'.$dato.'","'.$clusterNumber.'");</script>';
                        $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['descripcion'];
                        echo '<script type="text/javascript">chargeSharedDisk("'.$infoDisk.'","'.$dato.'","'.$t.'","'.$clusterNumber.'");</script>';
                        $t++;
                      }
                    }*/
                    $sql="select * from filtroIP where interId='".$maquinaPr['interId']."'";
                    $ipThings = mysqli_query($conn,$sql);
                    $k=0;
                    while($actip = mysqli_fetch_array($ipThings))
                    {
                      echo '<script type="text/javascript">AddIP("3","'.$dato.'","'.$clusterNumber.'");</script>';
                      $infoDisk=$actip['IP'].",".$actip['descripcion'];
                      echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$dato.'","'.$k.'","c'.$clusterNumber.'");</script>';
                      $k++;
                    }
                  break;
                }
                //echo '<script type="text/javascript">alert("'.$dato.'");</script>';
              }
            }
          }
          if($_POST['action']>2)
          {
            $expectedData =array('aplicacion','ambienteSolicitado','CPU','RAM');
            for($i=0;$i<$_POST['childOfStandalone'];$i++)
            {
              echo '<script type="text/javascript">giveSomeStandalone("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
              $infoMachine=$_POST['S1aplicacion'.$i].",".$_POST['S1ambienteSolicitado'.$i].",".$_POST['S1CPU'.$i].",".$_POST['S1RAM'.$i];
              echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$i.'","S1");</script>';
              for($j=0;$j<$_POST['IPStandalone'.$i];$j++)
              {
                echo '<script type="text/javascript">AddIP("1","'.$i.'");</script>';
                $infoDisk=$_POST['s1IP'.$i.'e'.$j].",".$_POST['s1dip'.$i.'e'.$j];
                echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$i.'","'.$j.'","s1");</script>';
              }
            }
            for($i=0;$i<$_POST['childOfApliance'];$i++)
            {
              echo '<script type="text/javascript">giveSomeApliance("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
              $infoMachine=$_POST['A1aplicacion'.$i].",".$_POST['A1ambienteSolicitado'.$i].",".$_POST['A1CPU'.$i].",".$_POST['A1RAM'.$i];
              echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$i.'","A1");</script>';
              for($j=0;$j<$_POST['IPApliance'.$i];$j++)
              {
                echo '<script type="text/javascript">AddIP("2","'.$i.'");</script>';
                $infoDisk=$_POST['a1IP'.$i.'e'.$j].",".$_POST['a1dip'.$i.'e'.$j];
                echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$i.'","'.$j.'","a1");</script>';
              }
            }
            for($i=0;$i<$_POST['childOfCluster'];$i++)
            {
              echo '<script type="text/javascript">giveSomeCluster("'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
              for($r=0;$r<$_POST['machinesOnCluster'.$i];$r++)
              {
                echo '<script type="text/javascript">AddClusterMachine("'.$i.'","'.$Ambiente.'","'.$_POST['solicitud'].'");</script>';
                $infoMachine=$_POST['C'.$i.'aplicacion'.$r].",".$_POST['C'.$i.'ambienteSolicitado'.$r].",".$_POST['C'.$i.'CPU'.$r].",".$_POST['C'.$i.'RAM'.$r];
                echo '<script type="text/javascript">chargeThisMachine("'.$infoMachine.'","'.$r.'","C'.$i.'");</script>';    
                for($j=0;$j<$_POST[$i."IPCluster".$r];$j++)
                {
                  echo '<script type="text/javascript">AddIP("3","'.$r.'","'.$i.'");</script>';      
                  $infoDisk=$_POST['c'.$i.'IP'.$r.'e'.$j].",".$_POST['c'.$i.'dip'.$r.'e'.$j];
                  echo '<script type="text/javascript">chargeIP("'.$infoDisk.'","'.$r.'","'.$j.'","c'.$i.'");</script>';                     
                }
              }
            }
          }
        ?>
    </form>
  </body>
</html>