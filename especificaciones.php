<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #SO
    {
      width:120px;
      float:right;
      margin-right:40px;
    }
    th,td
    {
      font-size: 16px;
    }
  </style>
  <script>
    function chargeUsers(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="table1Placeholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos 
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir username
          theUsernamePlace = document.createElement("td");
          theUsername = document.createElement("input");
          theUsername.name="username"+theData[0];
          theUsername.value=theData[1];
          theUsername.required=true;
          theUsername.type="text";
          theUsername.autocomplete="off";
          theUsernamePlace.appendChild(theUsername);
          theUsername.size=14;
        //  añadir group
          theGroupPlace = document.createElement("td");
          theGroup = document.createElement("input");
          theGroup.name="grupo"+theData[0];
          theGroup.value=theData[2];
          theGroup.type="text";
          theGroup.autocomplete="off";
          theGroupPlace.appendChild(theGroup);
          theGroup.size=14;
        //  añadir Home
          theHomePlace = document.createElement("td");
          theHome = document.createElement("input");
          theHome.name="homedir"+theData[0];
          theHome.value=theData[3];
          theHome.type="text";
          theHome.autocomplete="off";
          theHomePlace.appendChild(theHome);
          theHome.size=14;
        //  añadir Shell
          theShellPlace = document.createElement("td");
          theShell = document.createElement("input");
          theShell.name="shell"+theData[0];
          theShell.value=theData[4];
          theShell.type="text";
          theShell.autocomplete="off";
          theShellPlace.appendChild(theShell);
          theShell.size=14;
        //  añadir Perfil
          theProfilePlace = document.createElement("td");
          theProfile = document.createElement("input");
          theProfile.name="perfil"+theData[0];
          theProfile.value=theData[5];
          theProfile.type="text";
          theProfile.autocomplete="off";
          theProfilePlace.appendChild(theProfile);
          theProfile.size=14;
        //  añadir specConf
          thespecConfPlace = document.createElement("td");
          thespecConf = document.createElement("input");
          thespecConf.name="specConf"+theData[0];
          thespecConf.value=theData[6];
          thespecConf.type="text";
          thespecConf.autocomplete="off";
          thespecConfPlace.appendChild(thespecConf);
          thespecConf.size=24;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childUser"+theData[0];
          newTabla.id = "childUser"+theData[0];
          newTabla.appendChild(theUsernamePlace);
          newTabla.appendChild(theGroupPlace);
          newTabla.appendChild(theHomePlace);
          newTabla.appendChild(theShellPlace);
          newTabla.appendChild(theProfilePlace);
          newTabla.appendChild(thespecConfPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childUser").value=document.getElementById("childUser").value++;
    }
    function chargeFileSystem(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="filesystemPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos 
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir mountpoint
          theMountpointPlace = document.createElement("td");
          theMountpoint = document.createElement("input");
          theMountpoint.name="mountpoint"+theData[0];
          if(theData[0]==0)
          {
            theMountpoint.required=true;
            if(theData[1]!="")
              theMountpoint.value=theData[1];
          }
          else
            theMountpoint.value=theData[1];
          theMountpoint.type="text";
          theMountpoint.title="Ejemplo  Windows 'C:' 'D:' ; Linux '/dev/...'.";
          theMountpoint.autocomplete="off";
          theMountpointPlace.appendChild(theMountpoint);
          theMountpoint.size=20;
        //  añadir tamaño
          theSizePlace = document.createElement("td");
          theSize = document.createElement("input");
          theSize.name="size"+theData[0];
          theSize.required=true;
          theSize.type="number";
          theSize.value=theData[2];
          theSize.style="max-width:250px;";
          theSize.min=1;
          theSize.step=1;
          theSizePlace.appendChild(theSize);
        //  añadir propietario
          thePropietarioPlace = document.createElement("td");
          thePropietario = document.createElement("input");
          thePropietario.name="propietario"+theData[0];
          if(theData[0]==0)
          {
            thePropietario.required=true;
            if(theData[3]!="")
              thePropietario.value=theData[3];
          }
          else
            thePropietario.value=theData[3];
          thePropietario.type="text";
          thePropietario.autocomplete="off";
          thePropietarioPlace.appendChild(thePropietario);
          thePropietario.size=20;
        //  añadir grupo
          theGrupoFPlace = document.createElement("td");
          theGrupoF = document.createElement("input");
          theGrupoF.name="grupoF"+theData[0];
          if(theData[0]==0)
          {
            theGrupoF.required=true;
            if(theData[4]!="")
              theGrupoF.value=theData[4];
          }
          else
            theGrupoF.value=theData[4];
          theGrupoF.type="text";
          theGrupoF.autocomplete="off";
          theGrupoFPlace.appendChild(theGrupoF);
          theGrupoF.size=20;
        //  añadir permisos
          thePermisosPlace = document.createElement("td");
          thePermisos = document.createElement("input");
          thePermisos.name="permisos"+theData[0];
          if(theData[0]==0)
          {
            thePermisos.required=true;
            if(theData[5]!="")
              thePermisos.value=theData[5];
          }
          else
            thePermisos.value=theData[5];
          thePermisos.type="text";
          thePermisos.autocomplete="off";
          thePermisosPlace.appendChild(thePermisos);
          thePermisos.size=20;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfFilesystem"+theData[0];
          newTabla.id = "childOfFilesystem"+theData[0];
          newTabla.appendChild(theMountpointPlace);
          newTabla.appendChild(theSizePlace);
          newTabla.appendChild(thePropietarioPlace);
          newTabla.appendChild(theGrupoFPlace);
          newTabla.appendChild(thePermisosPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfFilesystem").value=document.getElementById("childOfFilesystem").value++;
    }
    function chargeSoftware(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="SoftwarePlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir producto
          theProductPlace = document.createElement("td");
          theProducto = document.createElement("input");
          theProducto.name="producto"+theData[0];
          theProducto.value=theData[1];
          theProducto.required=true;
          theProducto.type="text";
          theProducto.autocomplete="off";
          theProductPlace.appendChild(theProducto);
          theProducto.size=86;
        //  añadir version
          theVertionPlace = document.createElement("td");
          theVertion = document.createElement("input");
          theVertion.name="version"+theData[0];
          theVertion.value=theData[2];
          theVertion.required=true;
          theVertion.type="text";
          theVertion.autocomplete="off";
          theVertionPlace.appendChild(theVertion);
          theVertion.size=32;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childSoftware"+theData[0];
          newTabla.id = "childSoftware"+theData[0];
          newTabla.appendChild(theProductPlace);
          newTabla.appendChild(theVertionPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childSoftware").value=document.getElementById("childSoftware").value++;
    }
  </script>
  <?php
    echo "<title>especificaciones de maquina ".$_POST['machine']."</title>";
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
</head>
<body>
  <?php
    $sql = "select * from filtroEspecificacionesSO where folioNumber='".$_POST['machine']."'";
    $r=mysqli_query($conn,$sql);
    if(mysqli_affected_rows($conn)==1)
      $firstSOInfo = mysqli_fetch_array($r);
  ?>
  <div class="container">
    <form method='POST' action="saveSOspecs.php" > 
      <br>
      <!-- Botones principales -->
        <div align="center">
          <input type="submit" value="Modificar especificaciones">
        </div>
        <br>
      <input type="hidden" id="childUser" name="childUser" value="<?php if($firstSOInfo['usuarios']){echo $firstSOInfo['usuarios'];} else {echo "0";};?>">
      <input type="hidden" id="childOfFilesystem" name="childOfFilesystem" value="<?php if($firstSOInfo['filesystems']){echo $firstSOInfo['filesystems'];} else {echo "1";};?>">
      <input type="hidden" id="childSoftware" name="childSoftware" value="<?php if($firstSOInfo['software']){echo $firstSOInfo['software'];} else {echo "0";};?>">
      <input type="hidden" name="machine" id="machine" value="<?php echo $_POST['machine'];?>" >
      <input type="hidden" name="folio" id="folio" value="<?php echo $_POST["folio"];?>" >
      <table width="100%">
        <!-- Renglon   1   -->
          <tr>
            <!-- so -->
              <td width="23%"> 
                SO : <select id="SO" name="SO" required >
                  <option value=""></option>
                  <?php
                    $SOInfo = array("RHEL 5","RHEL 6","RHEL 7","RHEL 7.3","RHEL 7.4","RHEL 7.5","RHEL 7.6","RHEL 7.7","RHEL 7.8","Windows 7","Windows 10","Windows Server 2012 Std","Windows Server 2016 Std","Windows Server 2019 Std","Centos","Fedora","Debian","Suse","Ubuntu","Oracle Linux 6.1");
                    for($o=0;$o<sizeof($SOInfo);$o++)
                    {  
                      echo "<option value=\"".$SOInfo[$o]."\"";
                      if($firstSOInfo['SO']==$SOInfo[$o])
                        echo " selected ";
                      echo ">".$SOInfo[$o]."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- user -->
              <td width="23%">
                Usuarios: <select id="noUsers" name="noUsers" onchange="giveUsers(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {  
                      echo "<option value=\"".$o."\"";
                      if($firstSOInfo['usuarios']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- file system -->
              <td width="23%">
                Filesystem: <select id="fileSystem" name="fileSystem" onchange="giveFileSystem(this)">
                  <option value="1">1</option>
                  <?php
                    for($o=2;$o<10;$o++)
                    {  
                      echo "<option value=\"".$o."\"";
                      if($firstSOInfo['filesystems']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- software -->
              <td width="23%">
                Software: <select id="noSoftware" name="noSoftware" onchange="giveSoftware(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {  
                      echo "<option value=\"".$o."\"";
                      if($firstSOInfo['software']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
          </tr>
      </table>
      <table width="100%" >
        <!-- Renglon   titulos   -->
          <tr>
            <th width="15%">Usuario:</th>
            <th width="15%">Grupo:</th>
            <th width="15%">Home:</th>
            <th width="15%">Shell:</th>
            <th width="15%">Perfil:</th>
            <th width="25%">Configuracion especial<br>(solo si aplica)</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="table1Placeholder" ></tr>
      </table>
      <?php
        if($firstSOInfo['usuarios']>0)
        {
          try
          {
            $sql = "select * from filtroSOUsuario where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['username'].",".$rowOfUser['grupo'].",".$rowOfUser['home'].",".$rowOfUser['shell'].",".$rowOfUser['perfil'].",".$rowOfUser['specialConfig'];
              echo '<script type="text/javascript">chargeUsers("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
      Paremetros de kernel : 
      <textarea rows="1" cols="130" required name="kernel" id="kernel" onkeypress="return rev(event)" autocomplete="off" ><?php echo $firstSOInfo['kernel']; ?></textarea>
      File systems:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="20%" title="Ejemplo  Windows 'C:' 'D:' ; Linux '/dev/...'.">Punto de montaje</th>
            <th width="20%">Size GB</th>
            <th width="20%">Propietario</th>
            <th width="20%">Grupo</th>
            <th width="20%">Permisos</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="filesystemPlaceholder" ></tr>
      </table>
      <?php
        if($firstSOInfo['filesystems']>0)
        {
          try
          {
            $sql = "select * from filtroSOFilesystem where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['mountpoint'].",".$rowOfUser['sizeGB'].",".$rowOfUser['propietario'].",".$rowOfUser['grupo'].",".$rowOfUser['permisos'];
              echo '<script type="text/javascript">chargeFileSystem("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
        else
        {
          $rowUsr="0,,0,,,";
          echo '<script type="text/javascript">chargeFileSystem("'.$rowUsr.'");</script>';
        }
      ?>
      Software y/o paquetes necesarios:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="70%">Producto</th>
            <th width="30%">Version</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="SoftwarePlaceholder" ></tr>
      </table>
      <?php
        if($firstSOInfo['software']>0)
        {
          try
          {
            $sql = "select * from filtroSOSoftware where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['producto'].",".$rowOfUser['versionP'];
              echo '<script type="text/javascript">chargeSoftware("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
      Si es necesario agregar alguna configuracion adicional favor de indicarla:
      <textarea rows="1" cols="130" name="adicional" id="adicional" onkeypress="return rev(event)" autocomplete="off" ><?php echo $firstSOInfo['addicionalConfig']; ?></textarea>
      <!--
        <br><br>
        <button type="button" class="evilbtn">TD Automatización <br>Servicios Infraestructura</button>
        <br><br><br>
      -->
    </form>
    </div>
  </div>
</body>
<script>
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function giveUsers(select)
  {
    //  recuperar informacion de equipo
      workplace1="table1Placeholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childUser").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childUser"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra 
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos 
            newTabla = document.createElement("tr");
          //  añadir username
            theUsernamePlace = document.createElement("td");
            theUsername = document.createElement("input");
            theUsername.name="username"+creatorOfNodes;
            theUsername.value="Username"+(creatorOfNodes+1);
            theUsername.required=true;
            theUsername.type="text";
            theUsername.autocomplete="off";
            theUsernamePlace.appendChild(theUsername);
            theUsername.size=14;
          //  añadir group
            theGroupPlace = document.createElement("td");
            theGroup = document.createElement("input");
            theGroup.name="grupo"+creatorOfNodes;
            theGroup.value="Grupo"+(creatorOfNodes+1);
            theGroup.type="text";
            theGroup.autocomplete="off";
            theGroupPlace.appendChild(theGroup);
            theGroup.size=14;
          //  añadir Home
            theHomePlace = document.createElement("td");
            theHome = document.createElement("input");
            theHome.name="homedir"+creatorOfNodes;
            theHome.value="Homedir"+(creatorOfNodes+1);
            theHome.type="text";
            theHome.autocomplete="off";
            theHomePlace.appendChild(theHome);
            theHome.size=14;
          //  añadir Shell
            theShellPlace = document.createElement("td");
            theShell = document.createElement("input");
            theShell.name="shell"+creatorOfNodes;
            theShell.value="Shell"+(creatorOfNodes+1);
            theShell.type="text";
            theShell.autocomplete="off";
            theShellPlace.appendChild(theShell);
            theShell.size=14;
          //  añadir Perfil
            theProfilePlace = document.createElement("td");
            theProfile = document.createElement("input");
            theProfile.name="perfil"+creatorOfNodes;
            theProfile.value="Perfil"+(creatorOfNodes+1);
            theProfile.type="text";
            theProfile.autocomplete="off";
            theProfilePlace.appendChild(theProfile);
            theProfile.size=14;
          //  añadir specConf
            thespecConfPlace = document.createElement("td");
            thespecConf = document.createElement("input");
            thespecConf.name="specConf"+creatorOfNodes;
            thespecConf.value="";
            thespecConf.type="text";
            thespecConf.autocomplete="off";
            thespecConfPlace.appendChild(thespecConf);
            thespecConf.size=24;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childUser"+creatorOfNodes;
            newTabla.id = "childUser"+creatorOfNodes;
            newTabla.appendChild(theUsernamePlace);
            newTabla.appendChild(theGroupPlace);
            newTabla.appendChild(theHomePlace);
            newTabla.appendChild(theShellPlace);
            newTabla.appendChild(theProfilePlace);
            newTabla.appendChild(thespecConfPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childUser").value=select.value;
  }
  function giveFileSystem(select)
  {
    //  recuperar informacion de equipo
      workplace1="filesystemPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfFilesystem").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfFilesystem"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra 
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos 
            newTabla = document.createElement("tr");
          //  añadir mountpoint
            theMountpointPlace = document.createElement("td");
            theMountpoint = document.createElement("input");
            theMountpoint.name="mountpoint"+creatorOfNodes;
            if(creatorOfNodes==0)
              theMountpoint.required=true; 
            else
              theMountpoint.value="Mountpoint"+(creatorOfNodes+1);
            theMountpoint.type="text";
            theMountpoint.title="Ejemplo  Windows 'C:' 'D:' ; Linux '/dev/...'."
            theMountpoint.autocomplete="off";
            theMountpointPlace.appendChild(theMountpoint);
            theMountpoint.size=20;
          //  añadir tamaño
            theSizePlace = document.createElement("td");
            theSize = document.createElement("input");
            theSize.name="size"+creatorOfNodes;
            theSize.required=true; 
            theSize.type="number";
            theSize.style="max-width:250px;";
            theSize.min=1;
            theSize.step=1;
            theSizePlace.appendChild(theSize);
          //  añadir propietario
            thePropietarioPlace = document.createElement("td");
            thePropietario = document.createElement("input");
            thePropietario.name="propietario"+creatorOfNodes;
            if(creatorOfNodes==0)
              thePropietario.required=true; 
            else
              thePropietario.value="Propietario"+(creatorOfNodes+1);
            thePropietario.type="text";
            thePropietario.autocomplete="off";
            thePropietarioPlace.appendChild(thePropietario);
            thePropietario.size=20;
          //  añadir grupo
            theGrupoFPlace = document.createElement("td");
            theGrupoF = document.createElement("input");
            theGrupoF.name="grupoF"+creatorOfNodes;
            theGrupoF.value="Grupo"+(creatorOfNodes+1);
            theGrupoF.type="text";
            theGrupoF.autocomplete="off";
            theGrupoFPlace.appendChild(theGrupoF);
            theGrupoF.size=20;
          //  añadir permisos
            thePermisosPlace = document.createElement("td");
            thePermisos = document.createElement("input");
            thePermisos.name="permisos"+creatorOfNodes;
            if(creatorOfNodes==0)
              thePermisos.required=true; 
            else
              thePermisos.value="Permisos"+(creatorOfNodes+1);
            thePermisos.type="text";
            thePermisos.autocomplete="off";
            thePermisosPlace.appendChild(thePermisos);
            thePermisos.size=20;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfFilesystem"+creatorOfNodes;
            newTabla.id = "childOfFilesystem"+creatorOfNodes;
            newTabla.appendChild(theMountpointPlace);
            newTabla.appendChild(theSizePlace);
            newTabla.appendChild(thePropietarioPlace);
            newTabla.appendChild(theGrupoFPlace);
            newTabla.appendChild(thePermisosPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfFilesystem").value=select.value;
  }
  function giveSoftware(select)
  {
    //  recuperar informacion de equipo
      workplace1="SoftwarePlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childSoftware").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childSoftware"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra 
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos 
            newTabla = document.createElement("tr");
          //  añadir producto
            theProductPlace = document.createElement("td");
            theProducto = document.createElement("input");
            theProducto.name="producto"+creatorOfNodes;
            theProducto.value="Producto"+(creatorOfNodes+1);
            theProducto.required=true; 
            theProducto.type="text";
            theProducto.autocomplete="off";
            theProductPlace.appendChild(theProducto);
            theProducto.size=86;
          //  añadir version
            theVertionPlace = document.createElement("td");
            theVertion = document.createElement("input");
            theVertion.name="version"+creatorOfNodes;
            theVertion.required=true;
            theVertion.value="Version";
            theVertion.type="text";
            theVertion.autocomplete="off";
            theVertionPlace.appendChild(theVertion);
            theVertion.size=32;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childSoftware"+creatorOfNodes;
            newTabla.id = "childSoftware"+creatorOfNodes;
            newTabla.appendChild(theProductPlace);
            newTabla.appendChild(theVertionPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childSoftware").value=select.value;
  }
</script>
</html>