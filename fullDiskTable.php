<html lang="es"> 
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <title>Discos</title>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
    ?>
  </head>
  <script>
    function isIntNumber(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
    function rev(event)
    {
      var k = (event.which) ? event.which : event.keyCode;
      if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k ==32) )
        return true;
      else
        return false;
    }
    function giveSomeStatic()
    {
      //  recuperar informacion de equipo
        workplace1="StaticPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfStatic").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiskName = document.createElement("th");
          theDiskName.style="width:20%";
          theDiskName.innerHTML = "Diskname :";
          theStorageName = document.createElement("th");
          theStorageName.style="width:20%";
          theStorageName.innerHTML = "Almacenamiento (Gb) :";
          thePurposeName = document.createElement("th");
          thePurposeName.style="width:56%";
          thePurposeName.innerHTML = "Descripcion :";
          theKillButton = document.createElement("th");
          theKillButton.style="width:4%";
          newTLine.name = "StaticTitle";
          newTLine.id = "StaticTitle";
          newTLine.appendChild(theDiskName);
          newTLine.appendChild(theStorageName);
          newTLine.appendChild(thePurposeName);
          newTLine.appendChild(theKillButton);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          newLine.name = "childStatic"+mimic.toString();
          newLine.id = "childStatic"+mimic.toString();
          //  Disco
            theDiscoPlace = document.createElement("td");
            theDiscoPlace.style="width:20%";
            theDisco = document.createElement("input");
            theDisco.style="width:100%";
            theDisco.name="discoR"+mimic;
            theDisco.id="discoR"+mimic;
            theDisco.type="text";
            theDisco.onkeypress = function(){return rev(event)}; 
            theDisco.setAttribute("autocomplete", "off");
            theDiscoPlace.appendChild(theDisco);
          //  Storage
            theStoragePlace = document.createElement("td");
            theStoragePlace.style="width:20%";
            theStorage = document.createElement("input");
            theStorage.name="storageR"+mimic;
            theStorage.id="storageR"+mimic;
            theStorage.required=true; 
            theStorage.type="number";
            theStorage.style="width:100%";
            theStorage.min=0;
            theStorage.step=1;
            theStorage.onkeypress = function(){return isIntNumber(event)}; 
            theStoragePlace.appendChild(theStorage);
          //  Proposito
            thePurposePlace = document.createElement("td");
            thePurposePlace.style="width:56%";
            thePurpose = document.createElement("input");
            thePurpose.style="width:100%";
            thePurpose.name="propositoR"+mimic;
            thePurpose.id="propositoR"+mimic;
            thePurpose.type="text";
            thePurpose.onkeypress = function(){return rev(event)}; 
            thePurpose.setAttribute("autocomplete", "off");
            thePurposePlace.appendChild(thePurpose);
          //  Button
            theButtonPlace = document.createElement("td");
            theButtonPlace.style="width:4%";
            theButton = document.createElement("input");
            theButton.type="button";
            theButton.value="X";
            var numb=mimic;
            theButton.onclick = function(){killThisDisk(numb,3)}; 
            theButtonPlace.appendChild(theButton);
          //  Line
            newLine.appendChild(theDiscoPlace);
            newLine.appendChild(theStoragePlace);
            newLine.appendChild(thePurposePlace);
            newLine.appendChild(theButtonPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfStatic").value=mimic;
    }
    function giveSomeLogVol()
    {
      //  recuperar informacion de equipo
        workplace1="LogicVolumePlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfLogVol").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiskName = document.createElement("th");
          theDiskName.style="width:16%";
          theDiskName.innerHTML = "Disco :";
          theMountPointName = document.createElement("th");
          theMountPointName.style="width:16%";
          theMountPointName.innerHTML = "Punto de<br>Montaje :";
          theStorageName = document.createElement("th");
          theStorageName.style="width:16%";
          theStorageName.innerHTML = "Almacenamiento (Gb):";
          theTypeName = document.createElement("th");
          theTypeName.style="width:16%";
          theTypeName.innerHTML = "Tipo :";
          thePurposeName = document.createElement("th");
          thePurposeName.style="width:16%";
          thePurposeName.innerHTML = "Proposito :";
          thePerformanceName = document.createElement("th");
          thePerformanceName.style="width:16%";
          thePerformanceName.innerHTML = "Desempeno IOPS :";
          theKillButton = document.createElement("th");
          theKillButton.style="width:4%";
          newTLine.name = "LogVolTitle";
          newTLine.id = "LogVolTitle";
          newTLine.appendChild(theDiskName);
          newTLine.appendChild(theMountPointName);
          newTLine.appendChild(theStorageName);
          newTLine.appendChild(theTypeName);
          newTLine.appendChild(thePurposeName);
          newTLine.appendChild(thePerformanceName);
          newTLine.appendChild(theKillButton);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          newLine.name = "childLogVol"+mimic.toString();
          newLine.id = "childLogVol"+mimic.toString();
          //  Disco
            theDiscoPlace = document.createElement("td");
            theDiscoPlace.style="width:16%";
            theDisco = document.createElement("input");
            theDisco.style="width:100%";
            theDisco.name="discoE"+mimic;
            theDisco.id="discoE"+mimic;
            theDisco.type="text";
            theDisco.onkeypress = function(){return rev(event)}; 
            theDisco.setAttribute("autocomplete", "off");
            theDiscoPlace.appendChild(theDisco);
          //  Punto de Montaje
            theMountPointPlace = document.createElement("td");
            theMountPointPlace.style="width:16%";
            theMountPoint = document.createElement("input");
            theMountPoint.style="width:100%";
            theMountPoint.name="mountpointE"+mimic;
            theMountPoint.id="mountpointE"+mimic;
            theMountPoint.type="text";
            theMountPoint.onkeypress = function(){return rev(event)}; 
            theMountPoint.setAttribute("autocomplete", "off");
            theMountPointPlace.appendChild(theMountPoint);
          //  Storage
            theStoragePlace = document.createElement("td");
            theStoragePlace.style="width:8%";
            theStorage = document.createElement("input");
            theStorage.name="storageE"+mimic;
            theStorage.id="storageE"+mimic;
            theStorage.required=true; 
            theStorage.type="number";
            theStorage.style="width:100%";
            theStorage.min=1;
            theStorage.step=1;
            theStorage.onkeypress = function(){return isIntNumber(event)}; 
            theStoragePlace.appendChild(theStorage);
          //  Tipo
            theTypePlace = document.createElement("td");
            theTypePlace.style="width:16%";
            theType = document.createElement("input");
            theType.style="width:100%";
            theType.name="tipoE"+mimic;
            theType.id="tipoE"+mimic;
            theType.type="text";
            theType.onkeypress = function(){return rev(event)}; 
            theType.setAttribute("autocomplete", "off");
            theTypePlace.appendChild(theType);
          //  Proposito
            thePurposePlace = document.createElement("td");
            thePurposePlace.style="width:16%";
            thePurpose = document.createElement("input");
            thePurpose.style="width:100%";
            thePurpose.name="propositoE"+mimic;
            thePurpose.id="propositoE"+mimic;
            thePurpose.type="text";
            thePurpose.onkeypress = function(){return rev(event)}; 
            thePurpose.setAttribute("autocomplete", "off");
            thePurposePlace.appendChild(thePurpose);
          //  Performance
            thePerformancePlace = document.createElement("td");
            thePerformancePlace.style="width:16%";
            thePerformance = document.createElement("input");
            thePerformance.style="width:100%";
            thePerformance.name="performanceE"+mimic;
            thePerformance.id="performanceE"+mimic;
            thePerformance.type="text";
            thePerformance.onkeypress = function(){return rev(event)}; 
            thePerformance.setAttribute("autocomplete", "off");
            thePerformancePlace.appendChild(thePerformance);
          //  Button
            theButtonPlace = document.createElement("td");
            theButtonPlace.style="width:4%";
            theButton = document.createElement("input");
            theButton.type="button";
            theButton.value="X";
            var numb=mimic;
            theButton.onclick = function(){killThisDisk(numb,1)}; 
            theButtonPlace.appendChild(theButton);
          //  Line
            newLine.appendChild(theDiscoPlace);
            newLine.appendChild(theMountPointPlace);
            newLine.appendChild(theStoragePlace);
            newLine.appendChild(theTypePlace);
            newLine.appendChild(thePurposePlace);
            newLine.appendChild(thePerformancePlace);
            newLine.appendChild(theButtonPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfLogVol").value=mimic;
    }
    function giveSomeShared()
    {
      //  recuperar informacion de equipo
        workplace1="SharedPlaceholder";
        workplace1=document.getElementById(workplace1);
        mimic=document.getElementById("childOfShared").value;
        mimic=parseInt(mimic);
      //  Titulos
        if(mimic==0)
        {
          newTLine = document.createElement("tr");
          newTLine.style="width:100%";
          theDiskName = document.createElement("th");
          theDiskName.style="width:20%";
          theDiskName.innerHTML = "Disco<br>Compartido:";
          theStorageName = document.createElement("th");
          theStorageName.style="width:20%";
          theStorageName.innerHTML = "Almacenamiento (Mb):";
          thePurposeName = document.createElement("th");
          thePurposeName.style="width:56%";
          thePurposeName.innerHTML = "Proposito :";
          theKillButton = document.createElement("th");
          theKillButton.style="width:4%";
          newTLine.name = "SharedTitle";
          newTLine.id = "SharedTitle";
          newTLine.appendChild(theDiskName);
          newTLine.appendChild(theStorageName);
          newTLine.appendChild(thePurposeName);
          newTLine.appendChild(theKillButton);
          workplace1.parentNode.insertBefore(newTLine,workplace1);
        }
      //  añadir una maquina
        //  create nuevo line 
          var numb = parseInt(mimic);
          newLine = document.createElement("tr");
          newLine.style="width:100%";
          newLine.name = "childShared"+mimic.toString();
          newLine.id = "childShared"+mimic.toString();
          //  Disco
            theDiscoPlace = document.createElement("td");
            theDiscoPlace.style="width:20%";
            theDisco = document.createElement("input");
            theDisco.style="width:100%";
            theDisco.name="discoS"+mimic;
            theDisco.id="discoS"+mimic;
            theDisco.type="text";
            theDisco.onkeypress = function(){return rev(event)}; 
            theDisco.setAttribute("autocomplete", "off");
            theDiscoPlace.appendChild(theDisco);
          //  Storage
            theStoragePlace = document.createElement("td");
            theStoragePlace.style="width:20%";
            theStorage = document.createElement("input");
            theStorage.name="storageS"+mimic;
            theStorage.id="storageS"+mimic;
            theStorage.required=true; 
            theStorage.type="number";
            theStorage.style="width:100%";
            theStorage.min=0;
            theStorage.step=1;
            theStorage.onkeypress = function(){return isIntNumber(event)}; 
            theStoragePlace.appendChild(theStorage);
          //  Proposito
            thePurposePlace = document.createElement("td");
            thePurposePlace.style="width:56%";
            thePurpose = document.createElement("input");
            thePurpose.style="width:100%";
            thePurpose.name="propositoS"+mimic;
            thePurpose.id="propositoS"+mimic;
            thePurpose.type="text";
            thePurpose.onkeypress = function(){return rev(event)}; 
            thePurpose.setAttribute("autocomplete", "off");
            thePurposePlace.appendChild(thePurpose);
          //  Button
            theButtonPlace = document.createElement("td");
            theButtonPlace.style="width:4%";
            theButton = document.createElement("input");
            theButton.type="button";
            theButton.value="X";
            var numb=mimic;
            theButton.onclick = function(){killThisDisk(numb,2)}; 
            theButtonPlace.appendChild(theButton);
          //  Line
            newLine.appendChild(theDiscoPlace);
            newLine.appendChild(theStoragePlace);
            newLine.appendChild(thePurposePlace);
            newLine.appendChild(theButtonPlace);
      //
      workplace1.parentNode.insertBefore(newLine,workplace1);
      mimic=parseInt(mimic)+1;
      document.getElementById("childOfShared").value=mimic;
    }
    function killThisDisk(numero,kindOfDisk)
    {
      // recuperar informacion de equipo
        if(kindOfDisk==1)
        {
          expectedData = ["discoE","mountpointE","storageE","tipoE","propositoE","performanceE"];
          someMime=document.getElementById("childOfLogVol").value;
        }
        if(kindOfDisk==2)
        {
          expectedData = ["discoS","storageS","propositoS"];
          someMime=document.getElementById("childOfShared").value;
        }
        if(kindOfDisk==3)
        {
          expectedData = ["discoR","storageR","propositoR"];
          someMime=document.getElementById("childOfStatic").value;
        }
        someMime=parseInt(someMime)-1;
      // eliminar
        for(i=numero;i<someMime;i++)
        {
          for(j=0;j<expectedData.length;j++)
          {
            temp1=expectedData[j]+(i+1);
            some=document.getElementById(temp1).value;
            temp1=expectedData[j]+i;
            document.getElementById(temp1).value=some;
          }
        }
      if(kindOfDisk==1)
      {
        document.getElementById("childOfLogVol").value=someMime;
        var el ="childLogVol"+someMime.toString();
      }
      if(kindOfDisk==2)
      {
        document.getElementById("childOfShared").value=someMime;
        var el ="childShared"+someMime.toString();
      }
      if(kindOfDisk==3)
      {
        document.getElementById("childOfStatic").value=someMime;
        var el ="childStatic"+someMime.toString();
      }
      document.getElementById(el).remove();
      if(someMime==0)
      {
        if(kindOfDisk=="1")    
          document.getElementById("LogVolTitle").remove();
        if(kindOfDisk=="2")  
          document.getElementById("SharedTitle").remove();
        if(kindOfDisk=="3")  
          document.getElementById("StaticTitle").remove();
      }
    }
    function chargeLogVolDisk(datos,disco)
    {
      //alert(numero);
      //alert(maquinan+','+disco+','+prefijo);
      bullets=datos.split(",");
      document.getElementById("discoE"+disco).value=bullets[0];
      document.getElementById("mountpointE"+disco).value=bullets[1];
      document.getElementById("storageE"+disco).value=bullets[2];
      document.getElementById("tipoE"+disco).value=bullets[3];
      document.getElementById("propositoE"+disco).value=bullets[4];
      document.getElementById("performanceE"+disco).value=bullets[5];
    }
    function chargeSharedDisk(datos,disco)
    {
      //alert(numero);
      bullets=datos.split(",");
      document.getElementById("discoS"+disco).value=bullets[0];
      document.getElementById("storageS"+disco).value=bullets[1];
      document.getElementById("propositoS"+disco).value=bullets[2];
    }
    function chargeStaticDisk(datos,disco)
    {
      //alert(numero);
      bullets=datos.split(",");
      document.getElementById("discoR"+disco).value=bullets[0];
      document.getElementById("storageR"+disco).value=bullets[1];
      document.getElementById("propositoR"+disco).value=bullets[2];
    }
  </script>
  <body>
    <form method="post"  action="procesarDiscos.php" class="containerOfRod">
      <br><br>
      <!-- Botones Principales -->
        <div class="shad" align="center">
          &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit">
          <button id="addStatic" name="addStatic" type="button"  onclick="giveSomeStatic()"> Disco ++ </button>
          <?php 
            if(substr($_POST['machine'],0,1)=='c')
              echo '<button id="addShared" name="addShared" type="button" onclick="giveSomeShared()"> Disco Compartido ++ </button>';
          ?>
          <button id="addLogVol" name="addLogVol" type="button"  onclick="giveSomeLogVol()"> Volumen Logico ++ </button>
        </div>
        <br><br>
      <!-- Valores rescatados temp -->
        <input type="hidden" name="machine" value="<?php echo $_POST['machine'];?>" >
        <input type="hidden" name="folio" value="<?php echo $_POST['folio'];?>" >
      <!-- Placeholders -->
        <input type="hidden" id="childOfStatic" name="childOfStatic" value="0">
        <input type="hidden" id="childOfShared" name="childOfShared" value="0">
        <input type="hidden" id="childOfLogVol" name="childOfLogVol" value="0">
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="StaticPlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="SharedPlaceholder" ></tr>
        </table>
        <table width="96%" style="align:center;">
          <!-- Placeholder -->
            <tr id="LogicVolumePlaceholder" ></tr>
        </table>
        <?php
          $sql="select * from filtroDisco where interId='".$_POST['folio'].$_POST['machine']."'";
          //cho $sql;
          $diskThings = mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r>0)
          {
            $k=0;
            $t=0;
            $p=0;
            while($actDisk = mysqli_fetch_array($diskThings))
            {
              if($actDisk['tipo']=="LogVol")
              {
                echo '<script type="text/javascript">giveSomeLogVol();</script>';
                $infoDisk=$actDisk['nombreDisco'].",".$actDisk['mountpoint'].",".$actDisk['sizeDisco'].",".$actDisk['tipoDisco'].",".$actDisk['proposito'].",".$actDisk['performance'];
                echo '<script type="text/javascript">chargeLogVolDisk("'.$infoDisk.'","'.$k.'");</script>';
                $k++;
              }
              if($actDisk['tipo']=="Compartido")
              {
                echo '<script type="text/javascript">giveSomeShared();</script>';
                $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['proposito'];
                echo '<script type="text/javascript">chargeSharedDisk("'.$infoDisk.'","'.$t.'");</script>';
                $t++;
              }
              if($actDisk['tipo']=="Estatico")
              {
                echo '<script type="text/javascript">giveSomeStatic();</script>';
                $infoDisk=$actDisk['nombreDisco'].",".$actDisk['sizeDisco'].",".$actDisk['proposito'];
                echo '<script type="text/javascript">chargeStaticDisk("'.$infoDisk.'","'.$p.'");</script>';
                $p++;
              }
            }
          }
        ?>
    </form>
  </body>
</html>