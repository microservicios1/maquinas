<html>
<head>
  <meta charset="utf-8">
  <title>Maquinas actualizadas </title>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    body
    {
      background-size: 160px 75px;
      background-position: 94% 20px;
    }
    .evilbtn
    {
      font-size: 10px;
      height: 40px;
    }
    .container
    {
      padding: 4px 4px;
      box-sizing: border-box;
      font-size: 20px;
    }
  </style>
</head>
<body>
  <div class="container" style="border:10px groove #616161;border-radius: 10px;" align="center"><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>".mysqli_error();
      else
      {
        $noError=1;
        $HistoryUPD="";
        mysqli_autocommit($conn, FALSE);
        mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
        $some= array('aplicacion','ambienteSolicitado','ambienteEntregado','hostname','infraDef','status','statusDetail','statusOYM','remedy','TDOYM','hipervisor','internalConf','CPUS','RAMS','CPUE','RAME','entregaUser','soliMOP','entregaServer','inicioATP','finATP','entregaOYM','id');
        $some2= array('aplicacion','ambienteSolicitado','ambienteEntregado','nombre','infraestructuraDef','estatus','detalleEstatus','estatusOYM','remedy','TDOYM','hipervisor','intConfig','CPUSolicitado','RAMSolicitado','CPUEntregado','RAMEntregado','entregaUser','fechaSoliMOP','fechaEntregaServer','inicioPreATP','finPreATP','entregaOYM','id');
        for($i=0;$i<$_POST['childOfStandalone'];$i++)
        {
          for($j=0;$j<sizeof($some);$j++)  
          {
            $numeredData[$j]='S1'.$some[$j].$i;
          }
          $howManyIP='IPStandalone'.$i;
          giveIP($conn,$i,$howManyIP,'s1',$_POST[$numeredData[22]]);
          if($noError==1)
          {
            $sql="select aplicacion,ambienteSolicitado,ambienteEntregado,nombre,infraestructuraDef,estatus,detalleEstatus,estatusOYM,remedy,TDOYM,hipervisor,intConfig,CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,entregaUser,fechaSoliMOP,fechaEntregaServer,inicioPreATP,finPreATP,entregaOYM,storageSolicitado,sharedSolicitado,storageEntregado,sharedEntregado,historico from maquinas where interId='".$_POST[$numeredData[22]]."'";
            $mchIndoData=mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
            else
            {
              $infmach = mysqli_fetch_array($mchIndoData);
              $sql="";
              for($j=0;$j<(sizeof($some)-2);$j++)
              {
                if($j>11&&$j<16)
                {
                  if(!is_numeric($_POST[$numeredData[$j]]))
                    $temp=0;
                  else
                    $temp=$_POST[$numeredData[$j]];
                }
                else
                {
                  if(16<=$j)
                  {
                    $temp=$_POST[$numeredData[$j]];
                    if($temp!="")
                    {
                      //echo '<script type="text/javascript">alert("'.$temp.'");</script>';
                      $temp2=explode("/",$temp);
                      $temp3=$temp2[0];
                      $temp2[0]=$temp2[2];
                      $temp2[2]=$temp3;
                      $temp=implode("-",$temp2);
                      $temp2=explode("-",$temp);
                      for($k=0;$k<3;$k++)
                        $temp2[$k]=$temp2[$k+1];
                      unset($temp2[3]);
                      $temp=implode("-",$temp2);
                    }
                  }
                  else
                    $temp=$_POST[$numeredData[$j]];
                }
                if($temp!=$infmach[$some2[$j]])
                {
                  if($sql=="")
                  {
                    if($j>11&&$j<16)
                    {
                      $HistoryUPD="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql="update maquinas set maquinas.".$some2[$j]."=".$temp;
                    }
                    else
                    {
                      $HistoryUPD="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql="update maquinas set maquinas.".$some2[$j]."='".$temp."'";
                    } 
                  }
                  else
                  {
                    if($j>11&&$j<16)
                    {
                      $HistoryUPD .="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql .=",maquinas.".$some2[$j]."=".$temp;
                    }
                    else
                    {
                      $HistoryUPD .="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql .=",maquinas.".$some2[$j]."='".$temp."'";
                    }
                  }
                }
              }
              if($sql!="")
              {
                $sql .= " where maquinas.interId='".$_POST[$numeredData[22]]."'";
                mysqli_query($conn,$sql);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                  $noError=0;
                  //asegurar menos 1500
                $sql2 ="update maquinas set maquinas.historico='".$infmach['historico'].$HistoryUPD."' where maquinas.interId='".$_POST[$numeredData[22]]."'";
                //echo '<script type="text/javascript">prompt("ff","'.$sql2.'");</script>';
                mysqli_query($conn,$sql2);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                  $noError=0;
              }
            }
          }
        }
        for($i=0;$i<$_POST['childOfApliance'];$i++)
        {
          for($j=0;$j<sizeof($some);$j++)  
          {
            $numeredData[$j]='A1'.$some[$j].$i;
          }
          $howManyIP='IPApliance'.$i;
          giveIP($conn,$i,$howManyIP,'a1',$_POST[$numeredData[22]]);
          if($noError==1)
          {
            $sql="select aplicacion,ambienteSolicitado,ambienteEntregado,nombre,infraestructuraDef,estatus,detalleEstatus,estatusOYM,remedy,TDOYM,hipervisor,intConfig,CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,entregaUser,fechaSoliMOP,fechaEntregaServer,inicioPreATP,finPreATP,entregaOYM,storageSolicitado,sharedSolicitado,storageEntregado,sharedEntregado,historico from maquinas where interId='".$_POST[$numeredData[22]]."'";
            $mchIndoData=mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
            else
            {
              $infmach = mysqli_fetch_array($mchIndoData);
              $sql="";
              for($j=0;$j<(sizeof($some)-1);$j++)
              {
                if($j>11&&$j<16)
                {
                  if(!is_numeric($_POST[$numeredData[$j]]))
                    $temp=0;
                  else
                    $temp=$_POST[$numeredData[$j]];
                }
                else
                {
                  if(16<=$j)
                  {
                    $temp=$_POST[$numeredData[$j]];
                    if($temp!="")
                    {
                      //echo '<script type="text/javascript">alert("'.$temp.'");</script>';
                      $temp2=explode("/",$temp);
                      $temp3=$temp2[0];
                      $temp2[0]=$temp2[2];
                      $temp2[2]=$temp3;
                      $temp=implode("-",$temp2);
                      $temp2=explode("-",$temp);
                      for($k=0;$k<3;$k++)
                        $temp2[$k]=$temp2[$k+1];
                      unset($temp2[3]);
                      $temp=implode("-",$temp2);
                    }
                  }
                  else
                    $temp=$_POST[$numeredData[$j]];
                }
                if($temp!=$infmach[$some2[$j]])
                {
                  if($sql=="")
                  {
                    if($j>11&&$j<16)
                    {
                      $HistoryUPD="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql="update maquinas set maquinas.".$some2[$j]."=".$temp;
                    }
                    else
                    {
                      $HistoryUPD="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql="update maquinas set maquinas.".$some2[$j]."='".$temp."'";
                    } 
                  }
                  else
                  {
                    if($j>11&&$j<16)
                    {
                      $HistoryUPD .="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql .=",maquinas.".$some2[$j]."=".$temp;
                    }
                    else
                    {
                      $HistoryUPD .="\\nDato Actualizado ".$some2[$j]." anterior :".$infmach[$some2[$j]]." , actual :".$temp;
                      $sql .=",maquinas.".$some2[$j]."='".$temp."'";
                    }
                  }
                }
              }
              if($sql!="")
              {
                $sql .= " where maquinas.interId='".$_POST[$numeredData[22]]."'";
                mysqli_query($conn,$sql);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                  $noError=0;
                $sql2 ="update maquinas set maquinas.historico='".$infmach['historico'].$HistoryUPD."' where maquinas.interId='".$_POST[$numeredData[22]]."'";
                //echo '<script type="text/javascript">prompt("ff","'.$sql2.'");</script>';
                mysqli_query($conn,$sql2);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                  $noError=0;
              }
            }
          }
        }
        for($i=0;$i<$_POST['childOfCluster'];$i++)
        {
          for($j=0;$j<$_POST['machinesOnCluster'.$i];$j++)
          {
            for($h=0;$h<sizeof($some);$h++)  
              $numeredData[$h]='C'.$i.$some[$h].$j;
            /*$howManyDisk=$i.'DiskStaticClusterS'.$j;
            $totalStatic=giveMem($conn,$j,$howManyDisk,'c'.$i,$_POST[$numeredData[23]]);
            $staticInfo=explode(",",$totalStatic);
            $howManyDisk=$i.'DiskSharedSCluster'.$j;
            $totalShared=giveShMe($conn,$j,$howManyDisk,$i,$_POST[$numeredData[23]]);
            $sharedInfo=explode(",",$totalShared);
            */
            $howManyIP=$i.'IPCluster'.$j;
            giveIP($conn,$j,$howManyIP,'c'.$i,$_POST[$numeredData[22]]);
            if($noError==1)
            {
              $sql="select aplicacion,ambienteSolicitado,ambienteEntregado,nombre,infraestructuraDef,estatus,detalleEstatus,estatusOYM,remedy,TDOYM,hipervisor,intConfig,CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,entregaUser,fechaSoliMOP,fechaEntregaServer,inicioPreATP,finPreATP,entregaOYM,storageSolicitado,sharedSolicitado,storageEntregado,sharedEntregado,historico from maquinas where interId='".$_POST[$numeredData[22]]."'";
              $mchIndoData=mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $noError=0;
                echo "<p>Conexion con BD fallida</p>";
              }
              else
              {
                $infmach = mysqli_fetch_array($mchIndoData);
                $sql="";
                for($h=0;$h<(sizeof($some)-1);$h++)
                {
                  if($h>11&&$h<16)
                  {
                    if(!is_numeric($_POST[$numeredData[$h]]))
                      $temp=0;
                    else
                      $temp=$_POST[$numeredData[$h]];
                  }
                  else
                  {
                    if(16<=$h)
                    {
                      $temp=$_POST[$numeredData[$h]];
                      if($temp!="")
                      {
                        //echo '<script type="text/javascript">alert("'.$temp.'");</script>';
                        $temp2=explode("/",$temp);
                        $temp3=$temp2[0];
                        $temp2[0]=$temp2[2];
                        $temp2[2]=$temp3;
                        $temp=implode("-",$temp2);
                        $temp2=explode("-",$temp);
                        for($k=0;$k<3;$k++)
                          $temp2[$k]=$temp2[$k+1];
                        unset($temp2[3]);
                        $temp=implode("-",$temp2);
                      }
                    }
                    else
                      $temp=$_POST[$numeredData[$h]];
                  }
                  if($temp!=$infmach[$some2[$h]])
                  {
                    if($sql=="")
                    {
                      if($h>11&&$h<16)
                      {
                        $HistoryUPD="\\nDato Actualizado ".$some2[$h]." anterior :".$infmach[$some2[$h]]." , actual :".$temp;
                        $sql="update maquinas set maquinas.".$some2[$h]."=".$temp;
                      }
                      else
                      {
                        $HistoryUPD="\\nDato Actualizado ".$some2[$h]." anterior :".$infmach[$some2[$h]]." , actual :".$temp;
                        $sql="update maquinas set maquinas.".$some2[$h]."='".$temp."'";
                      } 
                    }
                    else
                    {
                      if($h>11&&$h<16)
                      {
                        $HistoryUPD .="\\nDato Actualizado ".$some2[$h]." anterior :".$infmach[$some2[$h]]." , actual :".$temp;
                        $sql .=",maquinas.".$some2[$h]."=".$temp;
                      }
                      else
                      {
                        $HistoryUPD .="\\nDato Actualizado ".$some2[$h]." anterior :".$infmach[$some2[$h]]." , actual :".$temp;
                        $sql .=",maquinas.".$some2[$h]."='".$temp."'";
                      }
                    } 
                  }
                }
                if($sql!="")
                {
                  $sql .= " where maquinas.interId='".$_POST[$numeredData[22]]."'";
                  mysqli_query($conn,$sql);
                  $r=mysqli_affected_rows($conn);
                  if($r<1)
                    $noError=0;
                  $sql2 ="update maquinas set maquinas.historico='".$infmach['historico'].$HistoryUPD."' where maquinas.interId='".$_POST[$numeredData[22]]."'";
                  //echo '<script type="text/javascript">prompt("ff","'.$sql2.'");</script>';
                  mysqli_query($conn,$sql2);
                  $r=mysqli_affected_rows($conn);
                  if($r<1)
                    $noError=0;
                }
              }
            }
          }
        }
        if($noError==0)
        {
          mysqli_rollback($conn);
          echo "<br><p>Cambios fallidos<br></p>";
        }
        else
        {
          mysqli_commit($conn);
          echo "<br><p>Maquinas modificadas <br> Recargar proyecto para visualizar cambios</p>";
        }
      }
      function giveIP($conn,$machine,$cantidadIP,$prefix,$id)
      {
        $sql="delete from interIP where interId='".$id."'";
        mysqli_query($conn,$sql);
        for($k=0;$k<$_POST[$cantidadIP];$k++)
        {
          $datosDisco[0]=$prefix.'IP'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'dip'.$machine.'e'.$k;
          $sql="insert into interIP values ('".$id."','".$_POST[$datosDisco[0]]."','".$_POST[$datosDisco[1]]."')"; 
          mysqli_query($conn,$sql);
        }
        $sql3 ="update maquinas set maquinas.netcard='".$_POST[$cantidadIP]."' where maquinas.interId='".$id."'";
        mysqli_query($conn,$sql3);
      }
      function giveMem($conn,$machine,$cantidadDiscos,$prefix,$id)
      {
        $Entregado=0;
        $Solicitado=0;
        $sql="delete from discos where interId='".$id."' and tipoDisco='Estatico'";
        mysqli_query($conn,$sql);
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'name'.$machine.'s'.$k;
          $datosDisco[1]=$prefix.'tam'.$machine.'s'.$k;
          $datosDisco[2]=$prefix.'des'.$machine.'s'.$k;
          $datosDisco[3]=$prefix.'name'.$machine.'e'.$k;
          $datosDisco[4]=$prefix.'tam'.$machine.'e'.$k;
          $datosDisco[5]=$prefix.'des'.$machine.'e'.$k;
          if($datosDisco[1]=="")
            $datosDisco[1]=0;
          if($datosDisco[4]=="")
            $datosDisco[4]=0;
          $sql="insert into discos values ('".$id."','".$_POST[$datosDisco[0]]."','".$_POST[$datosDisco[3]]."',".$_POST[$datosDisco[1]].",".$_POST[$datosDisco[4]].",'Estatico','".$_POST[$datosDisco[2]]."','".$_POST[$datosDisco[5]]."')"; 
          mysqli_query($conn,$sql);
          $Solicitado=$Solicitado+$_POST[$datosDisco[1]];
          $Entregado=$Entregado+$_POST[$datosDisco[4]];
        }
        return($Solicitado.",".$Entregado);
      }
      function giveShMe($conn,$machine,$cantidadDiscos,$prefix,$id)
      {
        $Entregado=0;
        $Solicitado=0;
        $sql="delete from discos where interId='".$id."' and tipoDisco='Compartido'";
        mysqli_query($conn,$sql);
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'namo'.$machine.'s'.$k;
          $datosDisco[1]=$prefix.'tamo'.$machine.'s'.$k;
          $datosDisco[2]=$prefix.'deso'.$machine.'s'.$k;
          $datosDisco[3]=$prefix.'namo'.$machine.'e'.$k;
          $datosDisco[4]=$prefix.'tamo'.$machine.'e'.$k;
          $datosDisco[5]=$prefix.'deso'.$machine.'e'.$k;
          if($datosDisco[1]=="")
            $datosDisco[1]=0;
          if($datosDisco[4]=="")
            $datosDisco[4]=0;
          $sql="insert into discos values ('".$id."','".$_POST[$datosDisco[0]]."','".$_POST[$datosDisco[3]]."',".$_POST[$datosDisco[1]].",".$_POST[$datosDisco[4]].",'Compartido','".$_POST[$datosDisco[2]]."','".$_POST[$datosDisco[5]]."')"; 
          mysqli_query($conn,$sql);
          $Solicitado=$Solicitado+$_POST[$datosDisco[1]];
          $Entregado=$Entregado+$_POST[$datosDisco[4]];
        }
        return($Solicitado.",".$Entregado);
      }
      mysqli_close($conn);
    ?>
    <button onclick=window.close();>Continuar</button>
    <br><br>
  </div>
  <iframe src="print('http://historico-ex379816-project-dev.apps.paas.telcelcloud.dt/someec.php'" width="0" height="0"></iframe>
</body>
</html>
<!--           //  discos
       /* 
       echo '<script type="text/javascript">prompt("'.$temp.'","'.$infmach[$some2[$j]].'");</script>';