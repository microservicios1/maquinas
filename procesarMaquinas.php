<html>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Maquinas registradas </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        $ItsFine=1;
        for($i=0;$i<$_POST['childOfStandalone'];$i++)
        {
          if($ItsFine==1)
          {
            $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."s1m".$i."'";
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r!=1)
              $ItsFine=0;
          }
        }
        if($ItsFine==1)
        {
          for($i=0;$i<$_POST['childOfApliance'];$i++)
          {
            if($ItsFine==1)
            {
              $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."a1m".$i."'";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r!=1)
                $ItsFine=0;
            }
          }
        }
        if($ItsFine==1)
        {
          for($i=0;$i<$_POST['childOfCluster'];$i++)
          {
            if($ItsFine==1)
            {
              $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."c".$i."m0"."'";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r!=1)
                $ItsFine=0;
            }
          }
        }
        if($ItsFine==1)
        {
          $cantidadMaquinas=$_POST['childOfStandalone']+$_POST['childOfApliance'];
          for($i=0;$i<$_POST['childOfCluster'];$i++)
            $cantidadMaquinas=$cantidadMaquinas+$_POST['machinesOnCluster'.$i];    
          // limpiar maquinas existentes con folio 
            mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
            $sql="delete from filtroMaquinas where folio='".$_POST['solicitud']."'";
            mysqli_query($conn,$sql);
          $expectedData =array('arreglo','tipo','aplicacion','ambienteSolicitado','CPU','RAM');
          $noError=1;
          for($i=0;$i<$_POST['childOfStandalone'];$i++)
          {
            for($j=0;$j<sizeof($expectedData);$j++)  
              $numeredData[$j]='S1'.$expectedData[$j].$i;
            if($noError==1)
            {
              $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."s1m".$i."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $noError=0;
                echo "<p>Conexion con BD fallida</p>";
              }
              else 
              {
                $howManyDisk='DiskStaticStandalone'.$i;
                giveMem($conn,$i,$howManyDisk,'s1');
                $howManyIP='IPStandalone'.$i;
                giveIP($conn,$i,$howManyIP,'s1');
              }
            }
          }
          for($i=0;$i<$_POST['childOfApliance'];$i++)
          {
            for($j=0;$j<sizeof($expectedData);$j++)  
              $numeredData[$j]='A1'.$expectedData[$j].$i;
            if($noError==1)
            {
              $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."a1m".$i."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $noError=0;
                echo "<p>Conexion con BD fallida</p>";
              }
              else 
              {
                $howManyDisk='DiskStaticApliance'.$i;
                giveMem($conn,$i,$howManyDisk,'a1');
                $howManyIP='IPApliance'.$i;
                giveIP($conn,$i,$howManyIP,'a1');
              }
            }
          }
          for($i=0;$i<$_POST['childOfCluster'];$i++)
          {
            for($j=0;$j<$_POST['machinesOnCluster'.$i];$j++)
            {
              for($h=0;$h<sizeof($expectedData);$h++)  
                $numeredData[$h]='C'.$i.$expectedData[$h].$j;
              if($noError==1)
              {
                $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."c".$i."m".$j."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
                mysqli_query($conn,$sql);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                {
                  $noError=0;
                  echo "<p>Conexion con BD fallida</p>";
                }
                else 
                {
                  $howManyDisk=$i.'DiskStaticCluster'.$j;
                  giveMem($conn,$j,$howManyDisk,'c'.$i);
                  $howManyDisk=$i.'DiskSharedCluster'.$j;
                  giveShMe($conn,$j,$howManyDisk,$i);
                  $howManyIP=$i.'IPCluster'.$j;
                  giveIP($conn,$j,$howManyIP,'c'.$i);
                }
              }
            }
          }
          if($noError==0)
          {
            mysqli_rollback($conn);
            echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br></p>";
          }
          else
          {
            mysqli_commit($conn);
            echo "<br><p>Especificaciones sobre VM <br> agregadas a su Proyecto<br></p>";
          }
        }
        else
        {
          echo '<script type="text/javascript">alert("Es necesario llenar el archivo de configuracione SO de todas las maquinas");</script>';
          $a=$_POST['action']+2;
          ?>
          <form method="post" action="inputmachine.php" id="brinca" name="brinca">
            <input type="hidden" name="action" id="action" value="<?php echo $a;?>">
            <input type="hidden" name="solicitud" id="solicitud" value="<?php echo $_POST['solicitud'];?>">
            <input type="hidden" name="totalMaquinas" id="totalMaquinas" value="<?php echo $_POST['totalMaquinas'];?>">
            <?php
              $machineData= array('arreglo','tipo','aplicacion','ambienteSolicitado','CPU','RAM','staticDisk','sharedDisk');
              $staticData= array('name','tam','des');
              $sharedData= array('namo','tamo','deso');
              for($i=0;$i<$_POST['totalMaquinas'];$i++)
              {
                for($k=0;$k<sizeof($machineData);$k++)
                {
                  $temp=$machineData[$k].$i;
                  echo "<input type=\"hidden\" name=\"".$temp."\" id=\"".$temp."\" value=\"".$_POST[$temp]."\">";
                  if($k==6)
                  {
                    for($j=0;$j<$_POST[$temp];$j++)
                      for($h=0;$h<sizeof($staticData);$h++)
                      {
                        $some=$staticData[$h].$j.'e'.$i;
                        echo "<input type=\"hidden\" name=\"".$some."\" id=\"".$some."\" value=\"".$_POST[$some]."\">";
                      }
                  }
                  if($k==7)
                    for($j=0;$j<$_POST[$temp];$j++)
                      for($h=0;$h<sizeof($sharedData);$h++)
                      {
                        $some=$sharedData[$h].$j.'e'.$i;
                        echo "<input type=\"hidden\" name=\"".$some."\" id=\"".$some."\" value=\"".$_POST[$some]."\">";
                      }
                }
              }
            ?>
            <script type="text/javascript">
              document.getElementById('brinca').submit(); // SUBMIT FORM
            </script>
          </form>
          <?php
        }
      }
      function giveMem($conn,$machine,$cantidadDiscos,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'name'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'tam'.$machine.'e'.$k;
          $datosDisco[2]=$prefix.'des'.$machine.'e'.$k;
          $sql="insert into filtroDiscos values ('".$_POST['solicitud'].$prefix."m".$machine."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "Disk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Estatico','".$_POST[$datosDisco[2]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
      function giveIP($conn,$machine,$cantidadIP,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadIP];$k++)
        {
          $datosDisco[0]=$prefix.'IP'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'dip'.$machine.'e'.$k;
          $sql="insert into filtroIP values ('".$_POST['solicitud'].$prefix."m".$machine."','".$_POST[$datosDisco[0]]."','".$_POST[$datosDisco[1]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
      function giveShMe($conn,$machine,$cantidadDiscos,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'namo'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'tamo'.$machine.'e'.$k;
          $datosDisco[2]=$prefix.'deso'.$machine.'e'.$k;
          $sql="insert into filtroDiscos values ('".$_POST['solicitud']."c".$prefix."m".$machine."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "SharedDisk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Compartido','".$_POST[$datosDisco[2]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <!--<button type="button" class="evilbtn">TD Automatizaci�n <br>Servicios Infraestructura</button>-->
    <p>  </p><br><br>
  </div>
</body>
</html>