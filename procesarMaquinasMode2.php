<html>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Maquinas registradas </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        $ItsFine=1;
        $reason="";
        for($i=0;$i<$_POST['childOfStandalone'];$i++)
        {
          if($ItsFine==1)
          {
            $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."s1m".$i."'";
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r!=1)
            {
              $ItsFine=0;
              $reason="Falta especificar SO de maquina Standalone ".($i+1);
            }
            else
            {
              $sql="select interId from filtroDisco where interId='".$_POST['solicitud']."s1m".$i."' and tipo='Estatico'";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $ItsFine=0;
                $reason="Ningun disco de storage en una maquina standalone ".($i+1);
              }
            }
          }
        }
        if($ItsFine==1)
        {
          for($i=0;$i<$_POST['childOfApliance'];$i++)
          {
            if($ItsFine==1)
            {
              $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."a1m".$i."'";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r!=1)
              {
                $ItsFine=0;
                $reason="Falta especificar SO de maquina Apliance ".($i+1);
              }
              else
              {
                $sql="select interId from filtroDisco where interId='".$_POST['solicitud']."a1m".$i."' and tipo='Estatico'";
                mysqli_query($conn,$sql);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                {
                  $ItsFine=0;
                  $reason="Ningun disco de storage en una maquina Apliance ".($i+1);
                }
              }
            }
          }
        }
        if($ItsFine==1)
        {
          for($i=0;$i<$_POST['childOfCluster'];$i++)
          {
            if($ItsFine==1)
            {
              $sql="select folio from filtroEspecificacionesSO where folioNumber='".$_POST['solicitud']."c".$i."m0"."'";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r!=1)
              {
                $ItsFine=0;
                $reason="Falta especificar SO en una maquina del cluster ".($i+1);
              }
              else
              {
                for($j=0;$j<$_POST['machinesOnCluster'.$i];$j++)
                {
                  $sql="select interId from filtroDisco where interId='".$_POST['solicitud']."c".$i."m".$j."' and tipo='Estatico'";
                  mysqli_query($conn,$sql);
                  $r=mysqli_affected_rows($conn);
                  if($r<1)
                  {
                    $ItsFine=0;
                    $reason="Ningun disco de storage en una maquina ".($j+1)." del cluster ".($i+1);
                  }
                }
              }
            }
          }
        }
        if($ItsFine==1)
        {
          $cantidadMaquinas=$_POST['childOfStandalone']+$_POST['childOfApliance'];
          for($i=0;$i<$_POST['childOfCluster'];$i++)
            $cantidadMaquinas=$cantidadMaquinas+$_POST['machinesOnCluster'.$i];    
          // limpiar maquinas existentes con folio 
            mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
            $sql="delete from filtroMaquinas where folio='".$_POST['solicitud']."'";
            mysqli_query($conn,$sql);
          $expectedData =array('arreglo','tipo','aplicacion','ambienteSolicitado','CPU','RAM');
          $noError=1;
          for($i=0;$i<$_POST['childOfStandalone'];$i++)
          {
            for($j=0;$j<sizeof($expectedData);$j++)  
              $numeredData[$j]='S1'.$expectedData[$j].$i;
            if($noError==1)
            {
              $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."s1m".$i."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $noError=0;
                echo "<p>Conexion con BD fallida</p>";
              }
              else 
              {
                /*$howManyDisk='DiskStaticStandalone'.$i;
                giveMem($conn,$i,$howManyDisk,'s1');*/
                $howManyIP='IPStandalone'.$i;
                giveIP($conn,$i,$howManyIP,'s1');
              }
            }
          }
          for($i=0;$i<$_POST['childOfApliance'];$i++)
          {
            for($j=0;$j<sizeof($expectedData);$j++)  
              $numeredData[$j]='A1'.$expectedData[$j].$i;
            if($noError==1)
            {
              $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."a1m".$i."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
              mysqli_query($conn,$sql);
              $r=mysqli_affected_rows($conn);
              if($r<1)
              {
                $noError=0;
                echo "<p>Conexion con BD fallida</p>";
              }
              else 
              {
                /* $howManyDisk='DiskStaticApliance'.$i;
                giveMem($conn,$i,$howManyDisk,'a1');*/
                $howManyIP='IPApliance'.$i;
                giveIP($conn,$i,$howManyIP,'a1');
              }
            }
          }
          for($i=0;$i<$_POST['childOfCluster'];$i++)
          {
            for($j=0;$j<$_POST['machinesOnCluster'.$i];$j++)
            {
              for($h=0;$h<sizeof($expectedData);$h++)  
                $numeredData[$h]='C'.$i.$expectedData[$h].$j;
              if($noError==1)
              {
                $sql="insert into filtroMaquinas values ('".$_POST['solicitud']."','".$_POST['solicitud']."c".$i."m".$j."',".$_POST[$numeredData[0]].",'".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."','".$_POST[$numeredData[3]]."','".$_POST[$numeredData[4]]."',".$_POST[$numeredData[5]].")";
                mysqli_query($conn,$sql);
                $r=mysqli_affected_rows($conn);
                if($r<1)
                {
                  $noError=0;
                  echo "<p>Conexion con BD fallida</p>";
                }
                else 
                {
                  /*$howManyDisk=$i.'DiskStaticCluster'.$j;
                  giveMem($conn,$j,$howManyDisk,'c'.$i);
                  $howManyDisk=$i.'DiskSharedCluster'.$j;
                  giveShMe($conn,$j,$howManyDisk,$i);*/
                  $howManyIP=$i.'IPCluster'.$j;
                  giveIP($conn,$j,$howManyIP,'c'.$i);
                }
              }
            }
          }
          if($noError==0)
          {
            mysqli_rollback($conn);
            echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br></p>";
          }
          else
          {
            mysqli_commit($conn);
            echo "<br><p>Especificaciones sobre VM <br> agregadas a su Proyecto<br></p>";
          }
        }
        else
        {
          echo '<script type="text/javascript">alert("'.$reason.'");</script>';
          $a=$_POST['action']+2;
          ?>
          <form method="post" action="enterMachine.php" id="brinca" name="brinca">
            <input type="hidden" name="action" id="action" value="<?php echo $a;?>">
            <input type="hidden" name="solicitud" id="solicitud" value="<?php echo $_POST['solicitud'];?>">
            <input type="hidden" name="childOfStandalone" id="childOfStandalone" value="<?php echo $_POST['childOfStandalone'];?>">
            <input type="hidden" name="childOfApliance" id="childOfApliance" value="<?php echo $_POST['childOfApliance'];?>">
            <input type="hidden" name="childOfCluster" id="childOfCluster" value="<?php echo $_POST['childOfCluster'];?>">
            <?php
              $expectedData =array('arreglo','tipo','aplicacion','ambienteSolicitado','CPU','RAM');
              for($i=0;$i<$_POST['childOfStandalone'];$i++)
              {
                $IPData= array('s1IP','s1dip');
                for($k=0;$k<sizeof($expectedData);$k++)  
                  echo "<input type=\"hidden\" name=\"S1".$expectedData[$k].$i."\" id=\"S1".$expectedData[$k].$i."\" value=\"".$_POST['S1'.$expectedData[$k].$i]."\">";
                echo "<input type=\"hidden\" name=\"IPStandalone".$i."\" id=\"IPStandalone".$i."\" value=\"".$_POST['IPStandalone'.$i]."\">";
                for($j=0;$j<$_POST['IPStandalone'.$i];$j++)
                  for($h=0;$h<sizeof($IPData);$h++)
                  {
                    $some=$IPData[$h].$i.'e'.$j;
                    echo "<input type=\"hidden\" name=\"".$some."\" id=\"".$some."\" value=\"".$_POST[$some]."\">";
                  }
              }
              for($i=0;$i<$_POST['childOfApliance'];$i++)
              {
                $IPData= array('a1IP','a1dip');
                for($k=0;$k<sizeof($expectedData);$k++)  
                  echo "<input type=\"hidden\" name=\"A1".$expectedData[$k].$i."\" id=\"A1".$expectedData[$k].$i."\" value=\"".$_POST['A1'.$expectedData[$k].$i]."\">";
                echo "<input type=\"hidden\" name=\"IPApliance".$i."\" id=\"IPApliance".$i."\" value=\"".$_POST['IPApliance'.$i]."\">";
                for($j=0;$j<$_POST['IPApliance'.$i];$j++)
                  for($h=0;$h<sizeof($IPData);$h++)
                  {
                    $some=$IPData[$h].$i.'e'.$j;
                    echo "<input type=\"hidden\" name=\"".$some."\" id=\"".$some."\" value=\"".$_POST[$some]."\">";
                  }
              }
              for($i=0;$i<$_POST['childOfCluster'];$i++)
              {
                echo "<input type=\"hidden\" name=\"machinesOnCluster".$i."\" id=\"machinesOnCluster".$i."\" value=\"".$_POST['machinesOnCluster'.$i]."\">";
                for($r=0;$r<$_POST['machinesOnCluster'.$i];$r++)
                {
                  $IPData= array('c'.$i.'IP','c'.$i.'dip');
                  for($k=0;$k<sizeof($expectedData);$k++)  
                    echo "<input type=\"hidden\" name=\"C".$i.$expectedData[$k].$r."\" id=\"C".$i.$expectedData[$k].$r."\" value=\"".$_POST['C'.$i.$expectedData[$k].$r]."\">";
                  echo "<input type=\"hidden\" name=\"".$i."IPCluster".$r."\" id=\"".$i."IPCluster".$r."\" value=\"".$_POST[$i."IPCluster".$r]."\">";
                  for($j=0;$j<$_POST[$i."IPCluster".$r];$j++)
                    for($h=0;$h<sizeof($IPData);$h++)
                    {
                      $some=$IPData[$h].$r.'e'.$j;
                      echo "<input type=\"hidden\" name=\"".$some."\" id=\"".$some."\" value=\"".$_POST[$some]."\">";
                    }
                }
              }
            ?>
            <script type="text/javascript">
              document.getElementById('brinca').submit(); // SUBMIT FORM
            </script>
          </form>
          <?php
        }
      }
      function giveMem($conn,$machine,$cantidadDiscos,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'name'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'tam'.$machine.'e'.$k;
          $datosDisco[2]=$prefix.'des'.$machine.'e'.$k;
          $sql="insert into filtroDiscos values ('".$_POST['solicitud'].$prefix."m".$machine."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "Disk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Estatico','".$_POST[$datosDisco[2]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
      function giveIP($conn,$machine,$cantidadIP,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadIP];$k++)
        {
          $datosDisco[0]=$prefix.'IP'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'dip'.$machine.'e'.$k;
          $sql="insert into filtroIP values ('".$_POST['solicitud'].$prefix."m".$machine."','".$_POST[$datosDisco[0]]."','".$_POST[$datosDisco[1]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
      function giveShMe($conn,$machine,$cantidadDiscos,$prefix)
      {
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]=$prefix.'namo'.$machine.'e'.$k;
          $datosDisco[1]=$prefix.'tamo'.$machine.'e'.$k;
          $datosDisco[2]=$prefix.'deso'.$machine.'e'.$k;
          $sql="insert into filtroDiscos values ('".$_POST['solicitud']."c".$prefix."m".$machine."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "SharedDisk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Compartido','".$_POST[$datosDisco[2]]."')"; 
          mysqli_query($conn,$sql);
        }
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <!--<button type="button" class="evilbtn">TD Automatizaci�n <br>Servicios Infraestructura</button>-->
    <p>  </p><br><br>
  </div>
</body>
</html>