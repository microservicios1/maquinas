<html>
<script languaje='javascript' type='text/javascript'> 
  window.resizeTo(500, 370);
</script>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Especificaciones DB </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        // limpiar maquinas existentes con folio 
          mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
          $sql="delete from filtroEspecificacionesDB where folioNumber='".$_POST['machine']."'";    
          mysqli_query($conn,$sql);
        $noError=1;
        $sql="insert into filtroEspecificacionesDB values ('".$_POST['machine']."','".$_POST['folio']."',";
        if($_POST['puerto']=="")
          $sql .= "0,'";
        else
          $sql .= $_POST['puerto'].",'";
        $sql .= $_POST['DBname']."',";
        if($_POST['especificaciones']=="")
          $sql .= "0,";
        else
          $sql .= $_POST['especificaciones'].",";
        if($_POST['usuarios']=="")
          $sql .= "0,";
        else
          $sql .= $_POST['usuarios'].",";
        if($_POST['response1']=="")
          $sql .= "0,'";
        else
          $sql .= $_POST['response1'].",'";
        $sql .= $_POST['comentarios']."','".$_POST['base2']."')";
        mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
        {
          $noError=0;
          echo "<p>Conexion con BD fallida</p>";
        }
        else 
        {
          $noError=giveSpecs($conn);
          if($noError==1)
            $noError=giveUsuarios($conn);
        }
        if($noError==0)
        {
          mysqli_rollback($conn);
          echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br> O campo de respaldos si/no vacio</p>";
        }
        else
        {
          mysqli_commit($conn);
          echo "<br><p>Especificaciones sobre DB de maquina ".$_POST['machine']." <br> agregadas a su Proyecto<br></p>";
        }
      }
      function giveUsuarios($conn)
      {
        $expectedData = array('dbuName','dbuRol','dbuPrivilegios');
        for($k=0;$k<$_POST['usuarios'];$k++)
        {
          for($r=0;$r<sizeof($expectedData);$r++)
            $searchForData[$r] = $expectedData[$r].$k;
          $sql="insert into filtroDBUsuarios values ('".$_POST['machine']."','".$_POST[$searchForData[0]]."','".$_POST[$searchForData[1]]."','".$_POST[$searchForData[2]]."')";    
          mysqli_query($conn,$sql);
          if(mysqli_affected_rows($conn)!=1)
            return(0);
        } 
        return(1);
      }
      function giveSpecs($conn)
      {
        $expectedData = array('especificacion','valor','valorDef');
        for($k=0;$k<$_POST['especificaciones'];$k++)
        {
          for($r=0;$r<sizeof($expectedData);$r++)
            $searchForData[$r] = $expectedData[$r].$k;
          $sql="insert into filtroDBEspecificaciones values ('".$_POST['machine']."','".$_POST[$searchForData[0]]."','".$_POST[$searchForData[1]]."','".$_POST[$searchForData[2]]."')";    
          mysqli_query($conn,$sql);
          if(mysqli_affected_rows($conn)!=1)
            return(0);
        } 
        return(1);
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <br>
  </div>
</body>
</html>