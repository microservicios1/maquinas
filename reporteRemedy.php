<?php
  include 'dbc.php';
  $conn = mysqli_connect($host,$user,$pass,$db);
  $sql3="select maquinas.folio,maquinas.aplicacion,maquinas.ambienteSolicitado,maquinas.CPUSolicitado,maquinas.RAMSolicitado,maquinas.storageSolicitado,maquinas.netcard,maquinas.hipervisor,especificacionesSO.SOSolicitado from maquinas left join especificacionesSO on especificacionesSO.folioNumber=maquinas.interId where maquinas.interId='".$_GET['folio']."'";
  $re=mysqli_query($conn,$sql3);
  $machineData = mysqli_fetch_array($re);
  $sql="select proyectos.proyecto,proyectos.descripcion,proyectos.criticidad,proyectos.administra,persona.nombre,persona.gerencia,persona.direccionId,persona.celular,persona.extension,persona.correo,persona.cargo from proyectos left join persona on proyectos.solicita=persona.userId where folio='".$machineData['folio']."'";
  $re=mysqli_query($conn,$sql);
  $proyectoData = mysqli_fetch_array($re);
  $direccion="";
  if($proyectoData['direccionId']>0)
  {
    $sql2="select nombre from direcciones where direccionId=".$proyectoData['direccionId'];
    $re2=mysqli_query($conn,$sql2);
    $data = mysqli_fetch_array($re2);
    $direccion=$data['nombre'];
  }
  $sql2="select nombreDisco,mountpoint,sizeDiscoS,tipoDisco,proposito,performance from disco where interId='".$_GET['folio']."' and tipo='LogVol'";
  $re2=mysqli_query($conn,$sql2);
  $m1=mysqli_affected_rows($conn);
  if($m1==""||$m1==0)
    $m1=1;
  $sql4="select sizeDiscoS from disco where interId='".$_GET['folio']."' and tipo='Estatico'";
  $re4=mysqli_query($conn,$sql4);
  $m4=0;
  $m4=mysqli_affected_rows($conn);
  require('ToRtf.php');
  $f = new ToRtf();
  $f->fichero = 'the-other-images/REMEDY/REMEDY'.$m1.'.rtf';
  $f->fsalida = 'Reporte_REMEDY.doc';
  $f->dirsalida = '';
  $f->retorno = 'fichero';
  $f->prefijo = '';
  $f->valores = array(
    '#*RESUMEN*#' => $proyectoData['descripcion'],
    '#*GERENCIA*#' => $proyectoData['gerencia'],
    '#*AREA*#' => $direccion,
    '#*NOMBRE*#' => $proyectoData['nombre'],
    '#*EXTEN*#' => $proyectoData['extension'],
    '#*CELULAR*#' => $proyectoData['celular'],
    '#*CORREO*#' => $proyectoData['correo'],
    '#*CARGO*#' => $proyectoData['cargo'],
    '#*RAM*#' => $machineData['RAMSolicitado'],
    '#*CPU*#' => $machineData['CPUSolicitado'],
    '#*CUANTOSDISCOS*#' => $m4,
    '#*NETCARD*#' => $machineData['netcard'],
    '#*SO*#' => $machineData['SOSolicitado'],
    '#*HIPERVISOR*#' => $machineData['hipervisor'],
    '#*TEMPLATE*#' => '',
    '#*AMBIENTE*#' => $machineData['ambienteSolicitado'],
    '#*CRITICIDAD*#' => $proyectoData['criticidad'],
    '#*DESCRIPCION*#' => $machineData['aplicacion']
  );
  $a2=0;
  while($HDData = mysqli_fetch_array($re4))
  {
    if($a2==0)
      $f->valores['#*TAMANOS*#'] = $HDData['sizeDiscoS'];
    else
      $f->valores['#*TAMANOS*#'] .= ", ".$HDData['sizeDiscoS'];
    $a2=$a2+1;
  }
  if($a2==0)
    $f->valores['#*TAMANOS*#'] = 'NA';
  $a2=0;
  while($diskData = mysqli_fetch_array($re2))
  {
    $f->valores['#*DISCO'.$a2.'*#'] = $diskData['nombreDisco'];
    $f->valores['#*MOUNTP'.$a2.'*#'] = $diskData['mountpoint'];
    $f->valores['#*ALMMB'.$a2.'*#'] = $diskData['sizeDiscoS']*1024;
    $f->valores['#*TIPO'.$a2.'*#'] = $diskData['tipoDisco'];
    $f->valores['#*PORP'.$a2.'*#'] = $diskData['proposito'];
    $f->valores['#*IOPS'.$a2.'*#'] = $diskData['performance'];
    $a2=$a2+1;
  }
  if($a2==0)
  {
    $f->valores['#*DISCO'.$a2.'*#'] = 'NA';
    $f->valores['#*MOUNTP'.$a2.'*#'] = 'NA';
    $f->valores['#*ALMMB'.$a2.'*#'] = 'NA';
    $f->valores['#*TIPO'.$a2.'*#'] = 'NA';
    $f->valores['#*PORP'.$a2.'*#'] = 'NA';
    $f->valores['#*IOPS'.$a2.'*#'] = 'NA';
  }
  switch($proyectoData['administra'])
  {
    case "ANA LILIA ACEVEDO JURADO":
      $f->valores['#*ADMGERENCIA*#'] = 'TD IMPLEMENTACION DE INFRAESTRUCTURA';
      $f->valores['#*ADMAREA*#'] = 'TD CONFIGURACION DE INFRAESTRUCTURA DIGITAL';
      $f->valores['#*ADMNOMBRE*#'] = 'ANA LILIA ACEVEDO JURADO';
      $f->valores['#*ADMEXTEN*#'] = '2452';
      $f->valores['#*ADMCELULAR*#'] = '5510103807';
      $f->valores['#*ADMCORREO*#'] = 'ana.acevedo@telcel.com';
      $f->valores['#*ADMCARGO*#'] = 'JEFE DE TD CONFIGURACION DE INFRAESTRUCTURA DIGITAL';
    break;
    case "RODRIGO LOPEZ MARTINEZ":
      $f->valores['#*ADMGERENCIA*#'] = 'IMPLEMENTACION DE INFRAESTRUCTURA';
      $f->valores['#*ADMAREA*#'] = 'TD INFRAESTRUCTURA DIGITAL';
      $f->valores['#*ADMNOMBRE*#'] = 'RODRIGO LOPEZ MARTINEZ';
      $f->valores['#*ADMEXTEN*#'] = '4325';
      $f->valores['#*ADMCELULAR*#'] = '554000280';
      $f->valores['#*ADMCORREO*#'] = 'rlopezm@telcel.com';
      $f->valores['#*ADMCARGO*#'] = 'JEFE DE INFRAESTRUCTURA DIGITAL';
    break;
    default:
      $f->valores['#*ADMGERENCIA*#'] = '';
      $f->valores['#*ADMAREA*#'] = '';
      $f->valores['#*ADMNOMBRE*#'] = 'No Definido';
      $f->valores['#*ADMEXTEN*#'] = '';
      $f->valores['#*ADMCELULAR*#'] = '';
      $f->valores['#*ADMCORREO*#'] = '';
      $f->valores['#*ADMCARGO*#'] = '';
    break;
  }
  $f->rtf();
?>