<?php
  include 'dbc.php';
  $conn = mysqli_connect($host,$user,$pass,$db);
  $sql="select kernel,addicionalConfig,usuarios,filesystems,software from especificacionesSO  where folioNumber='".$_GET['folio']."'";
  $re=mysqli_query($conn,$sql);
  $thatData = mysqli_fetch_array($re);
  if($thatData['usuarios']!=""&&$thatData['usuarios']>1)
    $a1=$thatData['usuarios'];
  else 
    $a1=1;
  if($thatData['filesystems']!=""&&$thatData['filesystems']>1)
    $b1=$thatData['filesystems'];
  else 
    $b1=1;
  if($thatData['software']!=""&&$thatData['software']>1)
    $c1=$thatData['software'];
  else 
    $c1=1;
  require('ToRtf.php');
  $f = new ToRtf();
  $f->fichero = 'the-other-images/SO/SO'.$a1.$b1.$c1.'.rtf';
  $f->fsalida = 'Reporte_SO.doc';
  $f->dirsalida = '';
  $f->retorno = 'fichero';
  $f->prefijo = '';
  $f->valores = array(
    '#*KERNEL*#' => $thatData['kernel'],
    '#*ADDCONF1*#' => $thatData['addicionalConfig']
	);
  $sql2="select username,grupo,home,shell,perfil,specialConfig from SOUsuario where folioNumber='".$_GET['folio']."'";
  $re2=mysqli_query($conn,$sql2);
  $a2=0;
  while($usrData = mysqli_fetch_array($re2))
  {
    $f->valores['#*USER'.$a2.'*#'] = $usrData['username'];
    $f->valores['#*GRUPO'.$a2.'*#'] = $usrData['grupo'];
    $f->valores['#*HOME'.$a2.'*#'] = $usrData['home'];
    $f->valores['#*SHELL'.$a2.'*#'] = $usrData['shell'];
    $f->valores['#*PERFIL'.$a2.'*#'] = $usrData['perfil'];
    $f->valores['#*ESPCONF'.$a2.'*#'] = $usrData['specialConfig'];
    $a2=$a2+1;
  }
  if($thatData['usuarios']==""||$thatData['usuarios']==0)
  {
    $f->valores['#*USER'.$a2.'*#'] = 'NA';
    $f->valores['#*GRUPO'.$a2.'*#'] = 'NA';
    $f->valores['#*HOME'.$a2.'*#'] = 'NA';
    $f->valores['#*SHELL'.$a2.'*#'] = 'NA';
    $f->valores['#*PERFIL'.$a2.'*#'] = 'NA';
    $f->valores['#*ESPCONF'.$a2.'*#'] = 'NA';
  }
  $sql2="select mountpoint,sizeGB,propietario,grupo,permisos from SOFilesystem where folioNumber='".$_GET['folio']."'";
  $re2=mysqli_query($conn,$sql2);
  $a2=0;
  while($filesysData = mysqli_fetch_array($re2))
  {
    $f->valores['#*MOUNTPOINT'.$a2.'*#'] = $filesysData['mountpoint'];
    $f->valores['#*SIZEGB'.$a2.'*#'] = $filesysData['sizeGB'];
    $f->valores['#*DUENO'.$a2.'*#'] = $filesysData['propietario'];
    $f->valores['#*GROUP'.$a2.'*#'] = $filesysData['grupo'];
    $f->valores['#*PERMISOS'.$a2.'*#'] = $filesysData['permisos'];
    $a2=$a2+1;
  }
  if($thatData['filesystems']==""||$thatData['filesystems']==0)
  {
    $f->valores['#*MOUNTPOINT'.$a2.'*#'] = 'NA';
    $f->valores['#*SIZEGB'.$a2.'*#'] = 'NA';
    $f->valores['#*DUENO'.$a2.'*#'] = 'NA';
    $f->valores['#*GROUP'.$a2.'*#'] = 'NA';
    $f->valores['#*PERMISOS'.$a2.'*#'] = 'NA';
  }
  $sql2="select producto,versionP from SOSoftware where folioNumber='".$_GET['folio']."'";
  $re2=mysqli_query($conn,$sql2);
  $a2=0;
  while($softwareData = mysqli_fetch_array($re2))
  {
    $f->valores['#*PRODUCTO'.$a2.'*#'] = $softwareData['producto'];
    $f->valores['#*VERSION'.$a2.'*#'] = $softwareData['versionP'];
    $a2=$a2+1;
  }
  if($thatData['software']==""||$thatData['software']==0)
  {
    $f->valores['#*PRODUCTO'.$a2.'*#'] = 'NA';
    $f->valores['#*VERSION'.$a2.'*#'] = 'NA';
  }
$f->rtf();
?>