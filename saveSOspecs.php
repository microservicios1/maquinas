<html>
<script languaje='javascript' type='text/javascript'> 
  window.resizeTo(500, 370);
</script>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Especificaciones SO </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        // limpiar maquinas existentes con folio 
          mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
          $sql="delete from filtroEspecificacionesSO where folioNumber='".$_POST['machine']."'";
          mysqli_query($conn,$sql);
        $noError=1;
        $sql="insert into filtroEspecificacionesSO values ('".$_POST['machine']."','".$_POST['folio']."','".$_POST['SO']."',".$_POST['noUsers'].",".$_POST['fileSystem'].",".$_POST['noSoftware'].",'".$_POST['kernel']."','".$_POST['adicional']."')";
        mysqli_query($conn,$sql);
        $r=mysqli_affected_rows($conn);
        if($r<1)
        {
          $noError=0;
          echo "<p>Conexion con BD fallida</p>";
        }
        else 
        {
          $noError=giveUsers($conn);
          if($noError==1)
            $noError=giveFilesystem($conn);
          if($noError==1)
            $noError=giveSoftware($conn);
        }
        if($noError==0)
        {
          mysqli_rollback($conn);
          echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br></p>";
        }
        else
        {
          mysqli_commit($conn);
          echo "<br><p>Especificaciones sobre SO de maquina ".$_POST['machine']." <br> agregadas a su Proyecto<br></p>";
        }
      }
      function giveUsers($conn)
      {
        $expectedData = array('username','grupo','homedir','shell','perfil','specConf');
        for($k=0;$k<$_POST['noUsers'];$k++)
        {
          for($r=0;$r<sizeof($expectedData);$r++)
            $searchForData[$r] = $expectedData[$r].$k;
          $sql="insert into filtroSOUsuario values ('".$_POST['machine']."','".$_POST[$searchForData[0]]."','".$_POST[$searchForData[1]]."','".$_POST[$searchForData[2]]."','".$_POST[$searchForData[3]]."','".$_POST[$searchForData[4]]."','".$_POST[$searchForData[5]]."')";
          mysqli_query($conn,$sql);
          if(mysqli_affected_rows($conn)!=1)
            return(0);
        } 
        return(1);
      }
      function giveFilesystem($conn)
      {
        $expectedData = array('mountpoint','size','propietario','grupoF','permisos');
        for($k=0;$k<$_POST['fileSystem'];$k++)
        {
          for($r=0;$r<sizeof($expectedData);$r++)
            $searchForData[$r] = $expectedData[$r].$k;
          $sql="insert into filtroSOFilesystem values ('".$_POST['machine']."','".$_POST[$searchForData[0]]."',";
          if($_POST[$searchForData[1]]=="")
            $sql .= "0";
          else
            $sql .= $_POST[$searchForData[1]];
          $sql .= ",'".$_POST[$searchForData[2]]."','".$_POST[$searchForData[3]]."','".$_POST[$searchForData[4]]."')";
          mysqli_query($conn,$sql);
          if(mysqli_affected_rows($conn)!=1)
            return(0);
        } 
        return(1);
      }
      function giveSoftware($conn)
      {
       for($k=0;$k<$_POST['noSoftware'];$k++)
        {
          $searchForData[0] = "producto".$k;
          $searchForData[1] = "version".$k;
          $sql="insert into filtroSOSoftware values ('".$_POST['machine']."','".$_POST[$searchForData[0]]."','".$_POST[$searchForData[1]]."')";
          mysqli_query($conn,$sql);
          if(mysqli_affected_rows($conn)!=1)
            return(0);
        } 
        return(1);
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <br>
  </div>
</body>
</html>